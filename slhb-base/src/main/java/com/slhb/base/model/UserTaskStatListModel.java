package com.slhb.base.model;

import com.slhb.core.jedis.StoredObj;
import com.slhb.base.dao.bean.TaskUserStatModel;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter@Setter
public class UserTaskStatListModel extends StoredObj {
    private Map<String,TaskUserStatModel> userTaskStatList = new ConcurrentHashMap<>();
}
