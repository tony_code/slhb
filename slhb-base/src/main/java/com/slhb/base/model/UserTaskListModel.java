package com.slhb.base.model;

import com.slhb.core.jedis.StoredObj;
import com.slhb.base.dao.bean.TaskUserHistModel;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Setter@Getter
public class UserTaskListModel extends StoredObj{
    /**
     * <id,TaskUserHistModel>
     */
    private Map<Integer,TaskUserHistModel> taskMap = new ConcurrentHashMap<>();

}
