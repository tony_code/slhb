package com.slhb.base.model;

import com.slhb.core.jedis.StoredObj;
import com.slhb.base.dao.bean.TaskAwardConfigModel;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter@Setter
public class TaskAwardsModel extends StoredObj {
    private Map<String,TaskAwardConfigModel> taskAwards = new ConcurrentHashMap<>();
}
