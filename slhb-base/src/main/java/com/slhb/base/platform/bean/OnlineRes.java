package com.slhb.base.platform.bean;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Setter@Getter
public class OnlineRes {

    //0为成功，其它都是错误
    private int code;

    private String msg;

    //数据结果
    private List<OnlineResBean> result;

}
