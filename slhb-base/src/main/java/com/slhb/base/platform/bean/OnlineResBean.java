package com.slhb.base.platform.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Setter@Getter@ToString
public class OnlineResBean {

    private String serverType;

    private String serverID;

    private int num;

}
