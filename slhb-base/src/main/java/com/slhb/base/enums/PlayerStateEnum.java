package com.slhb.base.enums;

/**
 * 玩家状态
 */
public enum PlayerStateEnum {
    spectator(0), //旁观
    sitdown(1), //坐下
    rob(2), //抢红包
    settle(3); //已结算

    private final int value;

    PlayerStateEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
