package com.slhb.base.enums;

/**
 * 房间状态
 */
public enum TableStateEnum {
    IDEL(0),  //空闲
    SEND(1),  //发红包
    ROB(2),   //抢红包
    SETTLE(3),//结算
    ;

    private final int value;

    TableStateEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
