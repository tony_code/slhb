package com.slhb.base.enums;

public enum RedisConst {
    //总杀量池(Z)
    GAME_KILL_AMOUNT_POOL(1,"GAME_KILL_AMOUNT_POOL_KEY_1021_",""),
    //当前资金池量原值量x
    ROBOT_POOL_CURRENT_MONEY(2,"ROBOT_POOL_CURRENT_MONEY_KEY_1021_",""),
    //redis 檢查key
    CHECK_REDIS_KEY(3,"CHECK_REDIS_KEY",""),

    ;

    private int index;
    private String profix;
    private String field;

    RedisConst(int index,String profix,String field){
        this.index = index;
        this.profix = profix;
        this.field = field;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getProfix() {
        return profix;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public void setProfix(String profix) {
        this.profix = profix;
    }

}
