package com.slhb.base.enums;

public class GameConst {

    /**
     * 桌面最大玩家数
     */
    public static final int TABLE_MAX_PLAYER_NUM = 5;


    /**
     * 是否强制允许客户端断线后，还能连接到游戏服务器进行游戏续玩
     */
    public static final boolean mustReconnectContinueGame = true;


    public static final int COST_TIME = 1000;

}
