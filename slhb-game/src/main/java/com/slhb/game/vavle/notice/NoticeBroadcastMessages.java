package com.slhb.game.vavle.notice;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.core.jedis.StoredCommManager;
import com.slhb.core.utils.TimeUtil;
import com.slhb.base.enums.RedisConst;
import com.slhb.base.enums.RoleType;
import com.slhb.game.dao.bean.KillPoolConfig;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.model.RedEnv;
import com.slhb.game.model.RedEnvDetail;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.robot.RobotServer;
import com.slhb.game.service.RobotStrategyService;
import com.slhb.game.service.holder.FunctionIdHolder;
import com.slhb.game.utils.NumUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.util.*;

/**
 * 广播通知处理类
 */
@Slf4j
public class NoticeBroadcastMessages {

    private static void winLoseList(AbstractTable table, List<JoloGame.JoloGame_TablePlay_PlayerSettleInfo> winList) {
        try {
            //dealer玩家
            PlayerInfo dealer = table.getPlayer(table.getDealer());
            if (dealer == null){
                log.error("Dealer玩家 userId = {}为空", table.getDealer());
                return;
            }

            //获取服务费比例
            KillPoolConfig killCfg = table.getKillPool();
            double serviceRate = killCfg.getServiceRate();

            for (PlayerInfo player : table.getInGamePlayers().values()){
                if (player == null) continue;

                //玩家抢到的红包
                RedEnvDetail pack = player.getCurRob();
                if (pack == null){
                    log.error("玩家{}本局{}抢到的红包为空", player.getPlayerId(), table.getCurrGameOrderId());
                    continue;
                }

                JoloGame.JoloGame_TablePlay_PlayerSettleInfo.Builder settle = JoloGame.JoloGame_TablePlay_PlayerSettleInfo.newBuilder();
                settle.setUserId(player.getPlayerId()).setSeatNum(0).setPackMoney(pack.getMoney())
                        .setIsDealer(dealer.getPlayerId().equals(player.getPlayerId())?1:0);

                //判断是否中雷
                if (pack.isThrunder() && table.getDealer().equals(player.getPlayerId())){
                    //中雷了且不是庄家,要赔钱
                    //输钱了
                    double lose = table.getRedEnv().getMoney() * table.getRoomConfig().getRate() - player.getCurRob().getMoney();
                    lose = NumUtils.double2Decimal(lose);

                    //玩家扣钱
                    player.minusPlayScoreStore(lose);
                    player.addPayback(lose);

                    //庄家加钱
                    dealer.addPlayScoreStore(lose);
                    dealer.addProfit(lose);
                }else {
                    //没中雷,赢得红包
                    //服务费
                    double serviceFee = 0;

                    //庄家不收服务费
                    if (!dealer.getPlayerId().equals(player.getPlayerId())){
                        serviceFee = NumUtils.double2Decimal(serviceRate * pack.getMoney());
                    }

                    //赢得钱
                    double win = NumUtils.double2Decimal(pack.getMoney() - serviceFee);

                    //给玩家加钱
                    player.addPlayScoreStore(win);
                    player.addServiceFee(serviceFee);
                    player.addProfit(win);
                }

                if (player.getPayback() > 0){
                    settle.setWinLose(0).setWinLoseScore(player.getPayback());
                }else {
                    settle.setWinLose(1).setWinLoseScore(player.getProfit());
                }
                settle.setPlayScoreStore(player.getPlayScoreStore())
                        .setIsLucky(pack.isMax()?1:0);

                winList.add(settle.build());
            }

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    //玩家入座 51001
    public static void playerSitDown(final AbstractTable table, String userId) {
        try {
            PlayerInfo player = table.getPlayer(userId);
            table.boardcastMessage(table.getTableId(),
                    JoloGame.JoloGame_Notice2Client_SitDownReq.newBuilder()
                            .setUserId(player.getPlayerId())
                            .setRoomId(table.getRoomId())
                            .setTableId(table.getTableId())
                            .setNickName(player.getNickName())
                            .setPlayScore(player.getPlayScoreStore())
                            .setSeatNum(player.getSeatNum())
                            .setIcon(player.getIcon())
                            .build(),
                    FunctionIdHolder.Game_Notice_SiteDown);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    //通知全部客户端 发红包 51005
    public static void giveRedEnvBoardcast(AbstractTable table) {
        try {
            log.debug("通知牌桌上GameOrderId={},玩家开始抢红包",table.getCurrGameOrderId());
            //本局红包
            RedEnv redEnv = table.getRedEnv();
            if (redEnv == null){
                log.error("本局红包设置失败 gameOrderId = {}", table.getCurrGameOrderId());
                return;
            }
            log.debug("通知牌桌上GameOrderId={},玩家开始抢红包={}",table.getCurrGameOrderId(),redEnv.toString());
            //庄家
            PlayerInfo dealer = table.getPlayer(table.getRedEnv().getOfferUserId());
            if (dealer == null){
                log.error("庄家不存在本桌 gameOrderId = {}", table.getCurrGameOrderId());
                return;
            }

            for (PlayerInfo player : table.getAllPlayers().values()){
                JoloGame.JoloGame_Notice2Client_GameStartReq.Builder notice = JoloGame.JoloGame_Notice2Client_GameStartReq.newBuilder();
                notice.setRoomId(table.getRoomId())
                        .setTableId(table.getTableId())
                        .setGameOrderId(table.getCurrGameOrderId())
                        .setCountDownSec(table.getCommonConfig().getRobCD())
                        .setOfferId(redEnv.getOfferUserId())
                        .setNickName(dealer.getNickName())
                        .setIcon(player.getIcon())
                        .setAllmoney(redEnv.getMoney())
                        .setRemains(redEnv.getRemains())
                        .setThunder(redEnv.getThunderEnd());

                log.debug("functionId = {} , msg = {}",FunctionIdHolder.Game_Notice_Rob_Start, notice.toString());
                table.boardcastMessageSingle(player.getPlayerId(), notice.build(), FunctionIdHolder.Game_Notice_Rob_Start);
            }

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    // 通知全部客户端用户抢红包结果
    public static void robRedEnvBoardcast(AbstractTable table, PlayerInfo player){
        try {
            log.debug("通知牌桌上GameOrderId={},玩家userId={},抢红包了", table.getCurrGameOrderId(), player.getPlayerId());
            JoloGame.JoloGame_Notice2Client_PlayerRobed.Builder notice = JoloGame.JoloGame_Notice2Client_PlayerRobed.newBuilder();
            notice.setRoomId(table.getRoomId())
                    .setTableId(table.getTableId())
                    .setUserId(player.getPlayerId())
                    .setGameOrderId(table.getCurrGameOrderId())
                    .setIcon(player.getIcon())
                    .setNickname(player.getNickName())
                    .setRobmoney(player.getCurRob().getMoney());

            table.boardcastMessage(table.getTableId(), notice.build(), FunctionIdHolder.Game_Notice2Client_RobPlayer);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    // 通知全部客户端->结算动画 51013
    public static void settleBoardcast(AbstractTable table) {
        try {
            List<JoloGame.JoloGame_TablePlay_PlayerSettleInfo> winList = new ArrayList<>();
            List<JoloGame.JoloGame_TablePlay_PlayerSettleInfo> loseList = new ArrayList<>();
            winLoseList(table, winList);
            JoloGame.JoloGame_Notice2Client_SettleRound_SettleReq.Builder reqBuilder = JoloGame.JoloGame_Notice2Client_SettleRound_SettleReq.newBuilder();
            reqBuilder.setRoomId(table.getRoomId())
                    .setTableId(table.getTableId())
                    .setGameOrderId(table.getCurrGameOrderId())
                    .addAllSettleWinList(winList)
                    .addAllSettleLoseList(loseList);

            log.debug("红包结算结果 result = {}", reqBuilder.toString());
            table.boardcastMessage(table.getTableId(), reqBuilder.build(), FunctionIdHolder.Game_Notice_SettleRound_SettleResult);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    /**
     * 通知机器人离座、回收金币
     * @param table
     * @param player
     */
    public static void sendPlayerLeaveNotice(AbstractTable table, PlayerInfo player){
        try {
            if (player.getRoleType() != RoleType.ROBOT){
                return;
            }

            JoloGame.JoloGame_Notice2Client_leaveReq.Builder notice = JoloGame.JoloGame_Notice2Client_leaveReq.newBuilder();
            notice.setTableId(table.getTableId()).setRoomId(table.getRoomId()).setUserId(player.getPlayerId());

            RobotServer.OBJ.noticeRobot(player.getPlayerId(), FunctionIdHolder.Game_Notice2Client_leavel, notice.build());
        }catch (Exception ex){
            log.error("发送消息失败 exception={}",ex);
        }
    }

    private static final String SPLIT = "_";
    public static void oprKillPool(double winScore, AbstractTable table){


    }

}
