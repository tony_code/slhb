package com.slhb.game.vavle.notice;

import JoloProtobuf.NoticeSvr.JoloNotice;
import com.google.protobuf.MessageLite;
import com.slhb.base.dao.bean.User;
import com.slhb.game.gate.service.UserService;
import com.slhb.game.robot.RobotServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 统一封装工具类
 */
public class NoticeRPCUtil {
    private final static Logger logger = LoggerFactory.getLogger(NoticeRPCUtil.class);
    /**
     * 推单个消息
     *
     * @param userId
     * @param content
     */
    public static void sendSingMsg(int gameId,String userId, int functionId, MessageLite content) {
        int length = content.toByteString().toByteArray().length;
        JoloNotice.JoloNotice_PayLoad payLoad = JoloNotice.JoloNotice_PayLoad.newBuilder().setLength(28 + length).setFunctionId(functionId).setGameId(gameId).setGameSvrId(0).setIsAsync(1).setReqNum((int) System.currentTimeMillis() / 1000).setResver1(0).setResver2(0).setPayLopad(content.toByteString()).build();

        UserService.getInstance().sendNoticeMsg(userId, payLoad);

        RobotServer.OBJ.noticeRobot(userId, functionId, content);
    }

    /**
     * 通知多个人
     *
     * @param tableId
     * @param message
     */
    public static void senMuliMsg(int gameId, String tableId, List<String> userIds, int functionId, MessageLite message) {
        int length = message.toByteString().toByteArray().length;
        JoloNotice.JoloNotice_PayLoad payLoad = JoloNotice.JoloNotice_PayLoad.newBuilder().setLength(28 + length).setFunctionId(functionId).setGameId(gameId).setGameSvrId(0).setIsAsync(1).setReqNum((int) System.currentTimeMillis() / 1000).setResver1(0).setResver2(0).setPayLopad(message.toByteString()).build();

        for(String userId:userIds){
            UserService.getInstance().sendNoticeMsg(userId, payLoad);

            RobotServer.OBJ.noticeRobot(userId, functionId, message);
        }
    }
}
