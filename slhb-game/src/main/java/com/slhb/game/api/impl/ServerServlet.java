package com.slhb.game.api.impl;

import com.slhb.game.api.BaseServlet;
import com.slhb.game.api.model.CodeObject;
import com.slhb.game.service.GameMaintentService;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


/**
 * 服务器启停
 */
@Path(value="/api/server")
public class ServerServlet extends BaseServlet {

    @GET
    @Path("/selectGameMaintain")
    @Produces({MediaType.APPLICATION_JSON})
    public String selectGameMaintainStatus() {
        CodeObject codeObject = new CodeObject();
        try {
            codeObject.setCode(0).setMsg("").setResult(GameMaintentService.OBJ.isDefense() ? "1" : "0");
            return codeObject.toJson();
        } catch (Exception e) {
            codeObject.setCode(5000).setMsg("服务器内部错误").setResult("");
            logger.error("查询游戏维护状态配置出错：{}", e.getMessage());
            return codeObject.toJson();
        }
    }

    @GET
    @Path("/updateGameMaintain")
    @Produces({MediaType.APPLICATION_JSON})
    public String updateGameMaintainStatus() {
        CodeObject codeObject = new CodeObject();
        try {
            GameMaintentService.OBJ.shift();
            codeObject.setCode(0).setMsg("").setResult(GameMaintentService.OBJ.isDefense() ? "1" : "0");
            return codeObject.toJson();
        } catch (Exception e) {
            codeObject.setCode(5000).setMsg("服务器内部错误").setResult("");
            logger.error("修改房间配置出错：{}", e.getMessage());
            return codeObject.toJson();
        }
    }

}
