package com.slhb.game.api.model;

import com.slhb.core.utils.GsonUtil;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j@Getter@ToString
public class CodeObject {

    private int code;
    private String msg;
    private String result;

    public CodeObject setCode(int code) {
        this.code = code;
        return this;
    }

    public CodeObject setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public CodeObject setResult(String result) {
        this.result = result;
        return this;
    }

    public String toJson(){
        return GsonUtil.getGson().toJson(this);
    }
}
