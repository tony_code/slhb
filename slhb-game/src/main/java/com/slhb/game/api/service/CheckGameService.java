package com.slhb.game.api.service;


import com.slhb.base.enums.RedisConst;
import com.slhb.core.jedis.StoredCommManager;
import com.slhb.game.gw.netty.GwcMsgSerivce;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 游戏检查服务
 */
@Slf4j
public class CheckGameService {
    //singleton
    public static final CheckGameService OBJ = new CheckGameService();

    //服务器正常
    private String isOk = "{\"code\": 0, \"msg\": \"success\"}";

    /**
     * 游戏状态
     * @return
     */
    public String check(){
        return isOk;
    }

    /**
     * 初始化一个线程去定时每分钟检测
     */
    public void init(){
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(()->this.selfCheck(), 0,30, TimeUnit.SECONDS);
    }

    /**
     * 自查
     */
    private void selfCheck(){
        //redis連接池检验
        boolean redisCheck = true;
        try {
            StoredCommManager.set(RedisConst.CHECK_REDIS_KEY.getProfix(),"1");
            String str = StoredCommManager.get(RedisConst.CHECK_REDIS_KEY.getProfix());
            if (StringUtils.isEmpty(str)){
                redisCheck = false;
            }
        }catch (Exception ex){
            log.error("redis連接池检验check error");
        }


        if (redisCheck == false){
            isOk = "{\"code\": 3, \"msg\": \"游戏redis存取异常,redis error\"}";
            return;
        }

        Map<Long, ChannelHandlerContext> map = GwcMsgSerivce.OBJ.get_ioHandler();
        if (map.size() <= 0){
            isOk = "{\"code\": 6, \"msg\": \"网关连接异常, game to gwc connect break\"}";
            return;
        }

        isOk = "{\"code\": 0, \"msg\": \"success\"}";
    }
}
