package com.slhb.game.api.impl;

import com.slhb.game.api.BaseServlet;
import com.slhb.game.api.service.CheckGameService;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path(value = "/api/gm")
public class GmServlet extends BaseServlet {

    @GET
    @Path("/statue")
    @Produces({MediaType.APPLICATION_JSON})
    public String statue(){
        return CheckGameService.OBJ.check();
    }

}
