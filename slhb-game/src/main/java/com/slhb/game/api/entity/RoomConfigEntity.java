package com.slhb.game.api.entity;

import com.slhb.game.dao.bean.RoomConfigModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 房间配置信息
 */
@Getter
@Setter
@ToString
public class RoomConfigEntity {
    private int id;//序列,用以数据库修改数据
    private String roomId; //房间ID
    private long minEnv; //红包最小
    private long maxEnv; //红包最大
    private double rate; //底注

    public RoomConfigEntity(RoomConfigModel e) {
        this.id = e.getId();
        this.roomId = e.getRoomId();
        this.minEnv = e.getMinEnv();
        this.maxEnv = e.getMaxEnv();
        this.rate = e.getRate();
    }
}
