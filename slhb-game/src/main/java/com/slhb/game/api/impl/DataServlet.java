package com.slhb.game.api.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.reflect.TypeToken;
import com.slhb.core.utils.GsonUtil;
import com.slhb.game.api.BaseServlet;
import com.slhb.game.api.entity.RoomConfigEntity;
import com.slhb.game.api.model.CodeObject;
import com.slhb.game.config.JsonConfig;
import com.slhb.game.dao.DBUtil;
import com.slhb.game.dao.bean.KillPoolConfig;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.room.service.RoomConfigService;
import com.slhb.game.service.RobotStrategyService;
import com.slhb.game.utils.NumUtils;
import org.apache.commons.lang3.StringUtils;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.*;
import java.util.stream.Collectors;

@Path(value = "/api/data")
public class DataServlet extends BaseServlet {

    @GET
    @Path("/selectAllRoom")
    @Produces({MediaType.APPLICATION_JSON})
    public String selectAllRoomConfig() {
        CodeObject codeObject = new CodeObject();
        try {
            List<RoomConfigModel> list = JsonConfig.ME.getRoomFromRedis();
            if (list == null || list.isEmpty()){
                list = RoomConfigService.OBJ.getConfigs();
            }

            List<RoomConfigEntity> array = new ArrayList<>();
            list.forEach(e ->
                    array.add(new RoomConfigEntity(e))
            );
            codeObject.setCode(0);
            codeObject.setMsg("");
            codeObject.setResult(JSONArray.toJSON(array).toString());
            Object obj = JSONObject.toJSON(codeObject);
            return obj.toString();
        } catch (Exception e) {
            codeObject.setCode(5000);
            codeObject.setMsg("服务器内部错误");
            codeObject.setResult("");
            Object obj = JSONObject.toJSON(codeObject);
            logger.error("查询房间配置出错：{}", e.getMessage());
            return obj.toString();
        }
    }

    @GET
    @Path("/updateRoom")
    @Produces({MediaType.APPLICATION_JSON})
    public String updateRoomConfig(@QueryParam("id") int id,
                                   @QueryParam("roomId") String roomId,
                                   @QueryParam("minJoinTable") Long minJoinTable,
                                   @QueryParam("ante") Long ante) {
        CodeObject codeObject = new CodeObject();
        try {
            if (minJoinTable <= 0 || ante <= 0  ) {
                codeObject.setCode(5001);
                codeObject.setMsg("非法参数错误");
                codeObject.setResult("");
                Object obj = JSONObject.toJSON(codeObject);
                return obj.toString();
            }
            logger.info("id:{},roomId:{},minJoinTable;{},ante,{}", id,
                    roomId,
                    minJoinTable,
                    ante);
            RoomConfigModel roomConfigModel = RoomConfigService.OBJ.getRoomConfig(roomId);

            int in = DBUtil.updateRoom(roomConfigModel);
            if (in == 1) {
                RoomConfigEntity roomConfigEntity = new RoomConfigEntity(RoomConfigService.OBJ.getRoomConfig(roomId));
                codeObject.setCode(0);
                codeObject.setMsg("");
                codeObject.setResult(JSONObject.toJSON(roomConfigEntity).toString());
            } else {
                codeObject.setCode(5001);
                codeObject.setMsg("参数错误");
                codeObject.setResult("");
            }
            Object obj = JSONObject.toJSON(codeObject);
            return obj.toString();
        } catch (Exception e) {
            codeObject.setCode(5000);
            codeObject.setMsg("服务器内部错误");
            codeObject.setResult("");
            Object obj = JSONObject.toJSON(codeObject);
            logger.error("修改房间配置出错：{}", e.getMessage());
            return obj.toString();
        }
    }


    /**
     * 游戏大厅在线参数配置
     *
     * @return
     */
    @GET
    @Path("/selectOnliner")
    @Produces({MediaType.APPLICATION_JSON})
    public String selectOnliner() {
        CodeObject codeObject = new CodeObject();
        codeObject.setCode(0);
        codeObject.setMsg("success");
        codeObject.setResult("");

        try {
            //更新数据库
            List<RoomConfigModel> list = JsonConfig.ME.getRoomFromRedis();
            if (list == null || list.isEmpty()){
                list = RoomConfigService.OBJ.getConfigs();
            }

            if (list == null) {
                return GsonUtil.getGson().toJson(codeObject);
            }

            Optional<RoomConfigModel> obj = list.stream().findFirst();
            if (obj == null) {
                return GsonUtil.getGson().toJson(codeObject);
            }

            RoomConfigModel model = obj.get();
            if (model == null) {
                return GsonUtil.getGson().toJson(codeObject);
            }

            String roles = model.getOnlinerRoles();
            List<String> arrays = GsonUtil.getGson().fromJson(roles, new TypeToken<List<String>>() {
            }.getType());
            if (arrays == null || arrays.size() != 5) {
                codeObject.setMsg("服务器内部错误,默认值异常" + roles);
                codeObject.setResult("");
                return GsonUtil.getGson().toJson(codeObject);
            }

            Map<String, String> map = new HashMap<>();
            map.put("x", arrays.get(0));
            map.put("y", arrays.get(1));
            map.put("a", arrays.get(2));
            map.put("b", arrays.get(3));
            map.put("c", arrays.get(4));
            codeObject.setResult(GsonUtil.getGson().toJson(map));
            return GsonUtil.getGson().toJson(codeObject);
        } catch (Exception e) {
            codeObject.setCode(5000);
            codeObject.setMsg("服务器内部错误");
            codeObject.setResult("");
            logger.error("修改通用配置出错：{}", e.getMessage());
            return GsonUtil.getGson().toJson(codeObject);
        }
    }

    @GET
    @Path("/updateOnliner")
    @Produces({MediaType.APPLICATION_JSON})
    public String updateOnliner(@QueryParam("x") String x,
                                @QueryParam("y") String y,
                                @QueryParam("a") String a,
                                @QueryParam("b") String b,
                                @QueryParam("c") String c) {
        CodeObject codeObject = new CodeObject();
        try {
            if (StringUtils.isEmpty(x) || StringUtils.isEmpty(y) || StringUtils.isEmpty(a) || StringUtils.isEmpty(b) || StringUtils.isEmpty(c)) {
                codeObject.setCode(5001);
                codeObject.setMsg("非法参数错误");
                codeObject.setResult("");
                Object obj = JSONObject.toJSON(codeObject);
                return obj.toString();
            }

            //构建参数
            List<String> list = Arrays.asList(x, y, a, b, c);
            String roles = GsonUtil.getGson().toJson(list);

            //更新数据库
            int count = DBUtil.updateOnlineRoles(roles);
            if (count >= 1) {
                codeObject.setCode(0);
                codeObject.setMsg("success");
                codeObject.setResult("");
            } else {
                codeObject.setCode(5001);
                codeObject.setMsg("参数错误");
                codeObject.setResult("");
            }
            Object obj = JSONObject.toJSON(codeObject);
            return obj.toString();
        } catch (Exception e) {
            codeObject.setCode(5000);
            codeObject.setMsg("服务器内部错误");
            codeObject.setResult("");
            Object obj = JSONObject.toJSON(codeObject);
            logger.error("修改通用配置出错：{}", e.getMessage());
            return obj.toString();
        }
    }

    /**
     * 杀量池配置API
     *
     * @return
     */
    @GET
    @Path("/selectAllKillPoolConfig")
    @Produces({MediaType.APPLICATION_JSON})
    public String selectAllKillPoolConfig(@QueryParam("channelId") String channelId) {
        CodeObject codeObject = new CodeObject();
        try {
            List<KillPoolConfig> list = JsonConfig.ME.getKillFromRedis();
            if (list == null || list.isEmpty()){
                list = RobotStrategyService.OBJ.get_list();
            }

            codeObject.setMsg("查询成功");
            codeObject.setCode(0);
            if (list == null) {
                codeObject.setResult("");
            } else {
                if (StringUtils.isEmpty(channelId)) {
                    codeObject.setResult(GsonUtil.getGson().toJson(list));
                } else {
                    List<KillPoolConfig> listTmp = list.stream().filter(e -> channelId.equals(e.getChannelId())).collect(Collectors.toList());
                    if (listTmp == null) {
                        codeObject.setResult("");
                    } else {
                        codeObject.setResult(GsonUtil.getGson().toJson(listTmp));
                    }
                }
            }
            return GsonUtil.getGson().toJson(codeObject);
        } catch (Exception e) {
            codeObject.setCode(5000);
            codeObject.setMsg("服务器内部错误");
            codeObject.setResult("");
            logger.error("查询杀量池配置出错：{}", e.getMessage());
            return GsonUtil.getGson().toJson(codeObject);
        }
    }

    @GET
    @Path("/addOrUpdateKillPoolConfig")
    @Produces({MediaType.APPLICATION_JSON})
    public String addOrUpdateKillPoolConfig(
            @QueryParam("id") int id,
            @QueryParam("channelId") String channelId,
            @QueryParam("roomId") String roomId,
            @QueryParam("initRobotPool") String initRobotPool,
            @QueryParam("initKillPool") String initKillPool,
            @QueryParam("flowRate") String flowRate,
            @QueryParam("maxKillRate") String maxKillRate,
            @QueryParam("minKillRate") String minKillRate,
            @QueryParam("serviceFee") int serviceFee
    ) {
        logger.debug("id={},channelId={},roomId={},initRobotPool={},initKillPool={},flowRate={},maxKillRate={},minKillRate={},serviceFee"
                , id, channelId, roomId, initRobotPool, initKillPool, flowRate, maxKillRate, minKillRate, serviceFee);
        //是否是更新操作
        boolean update = false;
        if (id > 0) {
            update = true;
        }

        CodeObject codeObject = new CodeObject();
        try {
            //参数校验
            if (id < 0 || serviceFee <= 0 || StringUtils.isEmpty(channelId) || StringUtils.isEmpty(roomId)
                    || StringUtils.isEmpty(initKillPool) || StringUtils.isEmpty(flowRate) || StringUtils.isEmpty(maxKillRate) || StringUtils.isEmpty(minKillRate)) {
                codeObject.setCode(5001);
                codeObject.setMsg("非法参数错误,有空字符混入");
                codeObject.setResult("");
                return GsonUtil.getGson().toJson(codeObject);
            }

            if (!NumUtils.isNumeric(initKillPool) || !NumUtils.isNumeric(flowRate)
                    || !NumUtils.isNumeric(maxKillRate) || !NumUtils.isNumeric(minKillRate)) {
                codeObject.setCode(5001);
                codeObject.setMsg("非法参数错误,有非数值参数混入");
                codeObject.setResult("");
                return GsonUtil.getGson().toJson(codeObject);
            }

            int room = Integer.valueOf(roomId);
            if (room > 4 || room < 1) {
                codeObject.setCode(5001);
                codeObject.setMsg("非法参数错误,房间ID在1~4之间");
                codeObject.setResult("");
                return GsonUtil.getGson().toJson(codeObject);
            }

            double killPool = Double.valueOf(initKillPool).doubleValue();
            double flowpre = Double.valueOf(flowRate).doubleValue();
            double killRateMax = Double.valueOf(maxKillRate).doubleValue();
            double killRateMin = Double.valueOf(minKillRate).doubleValue();

            if (flowpre > 8 || flowpre < 0) {
                codeObject.setCode(5001);
                codeObject.setMsg("非法参数错误,每局流水百分比在0~8之间");
                logger.error(codeObject.getMsg());
                return GsonUtil.getGson().toJson(codeObject);
            }

            if (killRateMax > 150 || killRateMax < 110) {
                codeObject.setCode(5001);
                codeObject.setMsg("非法参数错误,最大杀量比例在110~150之间");
                logger.error(codeObject.getMsg());
                return GsonUtil.getGson().toJson(codeObject);
            }

            if (killRateMin > 100 || killRateMin < 50) {
                codeObject.setCode(5001);
                codeObject.setMsg("非法参数错误,最大杀量比例在100~50之间");
                logger.error(codeObject.getMsg());
                return GsonUtil.getGson().toJson(codeObject);
            }

            if (serviceFee > 10 || serviceFee < 1) {
                codeObject.setCode(5001);
                codeObject.setMsg("非法参数错误,渠道服务费1%~10%之间");
                logger.error(codeObject.getMsg());
                return GsonUtil.getGson().toJson(codeObject);
            }

            if (killPool < 0) {
                codeObject.setCode(5001);
                codeObject.setMsg("非法参数错误,初始总杀量池不能小于0");
                logger.error(codeObject.getMsg());
                return GsonUtil.getGson().toJson(codeObject);
            }

            //构建数据库对象
            KillPoolConfig config = new KillPoolConfig();
            config.setId(id);
            config.setChannelId(channelId);
            config.setRoomId(roomId);
            config.setInitKillPool(killPool);
            config.setInitRobotPool(killPool);
            config.setFlowRate(flowpre);
            config.setMaxKillRate(killRateMax * 0.01d);
            config.setMinKillRate(killRateMin * 0.01d);
            config.setServiceFee(serviceFee);

            //数据库操作
            int count = 0;
            if (update) {
                count = DBUtil.updateKillPool(config);
            } else {
                List<KillPoolConfig> list = RobotStrategyService.OBJ.get_list();
                List<KillPoolConfig> listTmp = list.stream().filter(e -> channelId.equals(e.getChannelId()) && roomId.equals(e.getRoomId())).collect(Collectors.toList());
                if (listTmp == null || listTmp.size() == 0) {
                    count = DBUtil.insertKillPool(config);
                } else {
                    codeObject.setCode(5001);
                    codeObject.setMsg("操作失败,该渠道房间配置已存在");
                    codeObject.setResult("");
                    return GsonUtil.getGson().toJson(codeObject);
                }
            }

            //返回构建
            if (count == 0) {
                codeObject.setCode(5001);
                codeObject.setMsg("操作失败");
                codeObject.setResult("");
            } else {
                codeObject.setCode(0);
                codeObject.setMsg("操作成功");
                codeObject.setResult("");
            }
            return GsonUtil.getGson().toJson(codeObject);
        } catch (Exception e) {
            codeObject.setCode(5000);
            codeObject.setMsg("服务器内部错误");
            codeObject.setResult("");
            logger.error("修改杀量池配置出错：{}", e.getMessage());
            return GsonUtil.getGson().toJson(codeObject);
        }
    }



}



