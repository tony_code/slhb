package com.slhb.game.gameUtil;

import com.slhb.game.model.PlayerInfo;
import com.slhb.base.enums.PlayerStateEnum;
import com.slhb.base.enums.TableStateEnum;
import com.slhb.game.network.protocol.TableInnerReq;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.play.TableUtil;
import com.slhb.game.service.TableService;
import com.slhb.game.service.TimerService;
import com.slhb.game.utils.CardComparator;
import com.slhb.game.vavle.notice.NoticeBroadcastMessages;
import lombok.extern.slf4j.Slf4j;
import java.util.Iterator;
import java.util.List;

@Slf4j
public class GameLogic {

    //游戏开始
    //发红包
    public static void gameReady(final AbstractTable table) {
        table.setTableStateEnum(TableStateEnum.IDEL);
        table.setTableStatus();

        //停止牌桌内正在进行的倒计时
        TimerService.getInstance().delTimerTask(table.getRoomTableRelation());
        TimerService.getInstance().addTimerTask(table.getCommonConfig().getRobCD(), table.getRoomTableRelation(),
                new TableInnerReq(table.getPlayTypeStr(), table.getRoomId(), table.getTableId()) {
                    @Override
                    public void processImpl() throws Exception {

                        table.setTableStateEnum(TableStateEnum.SEND);
                        table.setTableStatus();
                        //准备红包
                        table.pressCard();

                        table.setTableStateEnum(TableStateEnum.ROB);
                        table.setTableStatus();
                        //通知开抢 51005
                        NoticeBroadcastMessages.giveRedEnvBoardcast(table);

                        //抢红包
                        robPackage(table);
                    }
                });
    }

    //抢红包
    public static void robPackage(final AbstractTable table){
        TimerService.getInstance().delTimerTask(table.getRoomTableRelation());
        TimerService.getInstance().addTimerTask(table.getCommonConfig().getSettleCD(), table.getRoomTableRelation(),
                new TableInnerReq(table.getPlayTypeStr(), table.getRoomId(), table.getTableId()) {
                    @Override
                    public void processImpl() throws Exception {
                        //结算
                        settle(table);
                    }
                });
    }

    //结算
    public static void settle(final AbstractTable table) {
        table.setTableStateEnum(TableStateEnum.SETTLE);
        table.setTableStatus();

        //停止牌桌内正在进行的倒计时
        TimerService.getInstance().delTimerTask(table.getRoomTableRelation());

        //广播结算51013
        NoticeBroadcastMessages.settleBoardcast(table);

        for (PlayerInfo player : table.getAllPlayers().values()) {
            //玩家数据重置
            player.resetGameingInfo();
            /*//强制玩家离桌，清除玩家数据
            NoticeBroadcastMessages.sendPlayerLeaveNotice(table, player);
            table.returnLobby(player.getPlayerId(), false);*/
        }

        //修改桌子的状态v
        table.setTableStateEnum(TableStateEnum.IDEL);
        table.setTableStatus();
        table.initTableStateAttribute();
        table.clearAllSeats();

        if (table.exsitRealPlayer()){
            //下一轮发红包
            gameReady(table);
        }else {
            for (PlayerInfo player : table.getAllPlayers().values()) {
                NoticeBroadcastMessages.sendPlayerLeaveNotice(table, player);
            }
            //销毁牌桌
            TableService.getInstance().destroyTable(table.getPlayTypeStr(), table.getRoomId(), table.getTableId());
        }
    }


}