package com.slhb.game.gameUtil;


import com.slhb.game.config.Config;
import com.slhb.game.service.NoticePlatformSerivce;
import com.slhb.game.utils.IdWorker;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

/**
 * 生成唯一游戏局号
 */
@Slf4j
public class GameOrderIdGenerator {

    private static final String split = "_";

    public static String generate() {
        try {
            return String.valueOf(IdWorker.getInstance().nextId());
        }catch (Exception e){
            log.error("IdWorker生产id失败,启用备用方案 , exception = {}" ,e.getMessage());
            return UUID.randomUUID().toString();
        }
    }

    public static String generate(String roomId,String tableId) {
        try {
            String gameOrderId = Config.GAME_SERID + split
                    + NoticePlatformSerivce.OBJ.getRoomLevel(roomId) + split
                    + tableId + split
                    + String.valueOf(IdWorker.getInstance().nextId());
            log.debug("IdWorker生产id={}", gameOrderId);
            return gameOrderId;
        }catch (Exception e){
            log.error("IdWorker生产id失败,启用备用方案 , exception = {}" ,e.getMessage());
            return UUID.randomUUID().toString();
        }
    }
}
