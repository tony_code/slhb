package com.slhb.game.network.protocol.acks;

import com.slhb.game.gate.network.protocol.Ack;

/**
 * 心跳消息网关->游戏服务器回复
 *
 * 申请下注
 */
public class JoloGame_ApplyLeaveAck_50003 extends Ack {

    /**
     * @param messageLite
     */
    public JoloGame_ApplyLeaveAck_50003(int functionId) {
        super(functionId);
    }
}
