package com.slhb.game.network.protocol.acks;

import com.slhb.game.gate.network.protocol.Ack;

public class JoloGame_OtherPlayerInfoAck_50014 extends Ack {
    public JoloGame_OtherPlayerInfoAck_50014(int functionId) {
        super(functionId);
    }
}
