package com.slhb.game.network.protocol.acks;

import com.slhb.game.gate.network.protocol.Ack;

public class JoloGame_ReconnectAck_50016 extends Ack {
    /**
     * @param messageLite
     * @param header
     */
    public JoloGame_ReconnectAck_50016(int functionId) {
        super(functionId);
    }
}
