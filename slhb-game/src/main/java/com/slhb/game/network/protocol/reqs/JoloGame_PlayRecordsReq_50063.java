package com.slhb.game.network.protocol.reqs;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.base.enums.ErrorCodeEnum;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.service.PlayerRecordSerivce;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

/**
 * 牌局历史记录
 *
 * @author
 *
 * @since 2018/9/11 18:55
 */
@Slf4j
public class JoloGame_PlayRecordsReq_50063 extends Req {

    private JoloGame.JoloGame_PlayRecordsReq req;

    public JoloGame_PlayRecordsReq_50063(int functionId) {
        super(functionId);
    }

    @Override
    public void readPayLoadImpl(ByteBuf byteBuf) throws Exception {
        byte[] blob = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(blob);
        req = JoloGame.JoloGame_PlayRecordsReq.parseFrom(blob);
    }

    @Override
    public void processImpl() throws Exception {

        log.info("收到消息functionId = {}, 消息体body={}",functionId,req.toString());
        String userId = this.userId;

        //返回消息体
        JoloGame.JoloGame_PlayRecordsAck.Builder ack = JoloGame.JoloGame_PlayRecordsAck.newBuilder();

        try {
            //构建消息
            ack.setUserId(userId)
                    .setResult(1)
                    .setResultMsg("")
                    .addAllRecords(PlayerRecordSerivce.OBJ.getPlayRecords(userId));

            log.debug("send - > {}", ack.toString());
            sendResponse(GateFunctionFactory.__function__id_50063 | 0x08000000, ack.build().toByteArray());
        }catch (Exception e){
            log.error("functionId = {},消息处理失败 exception = {}", functionId, e.getMessage());
            //构建失败消息
            ack.setUserId(userId)
                    .setResult(-1)
                    .setResultMsg(ErrorCodeEnum.GAME_50063_1.getCode());
            sendResponse(GateFunctionFactory.__function__id_50063 | 0x08000000, ack.build().toByteArray());
        }

    }
}
