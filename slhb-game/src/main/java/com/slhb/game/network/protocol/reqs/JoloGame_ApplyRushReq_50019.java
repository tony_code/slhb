package com.slhb.game.network.protocol.reqs;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.base.enums.ErrorCodeEnum;
import com.slhb.base.enums.PlayerStateEnum;
import com.slhb.base.enums.TableStateEnum;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.model.RedEnvDetail;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.play.TableUtil;
import com.slhb.game.room.service.RoomConfigService;
import com.slhb.game.service.TableService;
import com.slhb.game.vavle.notice.NoticeBroadcastMessages;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

/**
 * 抢庄
 */
@Slf4j
public class JoloGame_ApplyRushReq_50019 extends Req {
    private long time;

    public JoloGame_ApplyRushReq_50019( int functionId) {
        super(functionId);
    }

    private JoloGame.JoloGame_RobPackReq req;

    @Override
    public void readPayLoadImpl(ByteBuf buf) throws Exception {
        time = System.currentTimeMillis();
        byte[] blob = new byte[buf.readableBytes()];
        buf.readBytes(blob);
        req = JoloGame.JoloGame_RobPackReq.parseFrom(blob);
        this.setTable(TableService.getInstance().getTable(reqHeader.gameId + "", req.getRoomId(), req.getTableId()));
    }

    @Override
    public void processImpl() throws Exception {
        log.debug("收到消息-> " + functionId + ", reqNum-> " + reqHeader.reqNum + ", req->" + req.toString());
        String userId = this.userId;
        String roomId = req.getRoomId();
        String tableId = req.getTableId();
        String gameOrderId = req.getGameOrderId();


        AbstractTable table = getTable();
        JoloGame.JoloGame_RobPackAck.Builder ack = JoloGame.JoloGame_RobPackAck.newBuilder();
        ack.setUserId(userId);
        ack.setRoomId(roomId);
        ack.setTableId(tableId);
        ack.setResult(1);

        try {
            if (table == null) {
                log.error("未找到table");
                ack.setResult(-1).setResultMsg("找不到当前桌子");
                sendResponse(GateFunctionFactory.__function__id_50019 | 0x08000000, ack.build().toByteArray());
                return;
            }
            if (table.getTableStateEnum().getValue() != TableStateEnum.ROB.getValue()) {
                log.error("当前牌桌状态={}",table.getTableStateEnum().name());
                ack.setResult(-1).setResultMsg("桌内状态不符，不能操作");
                sendResponse(GateFunctionFactory.__function__id_50019 | 0x08000000, ack.build().toByteArray());
                return;
            }

            PlayerInfo player = table.getPlayer(userId);
            if (null == player) {
                log.error("can't found player info, playerId->" + userId + ",tableInfo:" + TableUtil.toStringNormal(table));
                ack.setResult(-1).setResultMsg(ErrorCodeEnum.GAME_50050_2.getCode());
                sendResponse(GateFunctionFactory.__function__id_50019 | 0x08000000, ack.build().toByteArray());
                return;
            }

            String currGameOrderId = table.getCurrGameOrderId();
            //如果gameOrderId不一致，那么返回错误
            if (!gameOrderId.equals(currGameOrderId)) {
                ack.setResult(-13);
                log.error("GameOrderId不一致, req gameOrderId->" + gameOrderId + ", currGameOrderId->" + currGameOrderId + ",tableInfo:" + TableUtil.toStringNormal(table));
                ack.setResultMsg("gameOrderId错误");
                sendResponse(GateFunctionFactory.__function__id_50019 | 0x08000000, ack.build().toByteArray());
                return;
            }

            if (player.getState().getValue() >= PlayerStateEnum.rob.getValue()){
                log.error("当前玩家状态={}",player.getState().name());
                ack.setResult(-1).setResultMsg("桌内状态不符，不能操作");
                sendResponse(GateFunctionFactory.__function__id_50019 | 0x08000000, ack.build().toByteArray());
                return;
            }

            RoomConfigModel cfg = RoomConfigService.OBJ.getRoomConfig(roomId);
            if (player.getPlayScoreStore() < table.getRedEnv().getMoney() * cfg.getRate()){
                ack.setResult(-1).setResultMsg(ErrorCodeEnum.GAME_50013_3.getDesc());
                sendResponse(GateFunctionFactory.__function__id_50019 | 0x08000000, ack.build().toByteArray());
                return;
            }

            RedEnvDetail pack = table.robPackage(player.getPlayerId());
            if (pack == null){
                log.error("玩家{}本局{}抢了个空红包", player.getPlayerId(), table.getCurrGameOrderId());
                return;
            }
            player.setCurRob(pack);
            player.setState(PlayerStateEnum.rob);
            table.getAllPlayers().put(userId, player);

            //返回消息
            ack.setRobmoney(pack.getMoney());
            sendResponse(GateFunctionFactory.__function__id_50019 | 0x08000000, ack.build().toByteArray());

            //广播玩家抢红包
            NoticeBroadcastMessages.robRedEnvBoardcast(table, player);
        } catch (Exception ex) {
            ack.setResult(-10).setResultMsg(ErrorCodeEnum.GAME_50002_2.getCode());
            sendResponse(GateFunctionFactory.__function__id_50019 | 0x08000000, ack.build().toByteArray());
            log.error(ex.getMessage(), ex);
        } finally {
            log.debug("响应消息-> " + functionId + ", ack->" + ack.toString());
        }
    }
}
