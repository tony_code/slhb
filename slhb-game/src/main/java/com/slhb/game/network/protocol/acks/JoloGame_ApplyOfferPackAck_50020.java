package com.slhb.game.network.protocol.acks;

import com.slhb.game.gate.network.protocol.Ack;

/**
 * 心跳消息网关->游戏服务器回复
 *
 * 申请抢庄
 */
public class JoloGame_ApplyOfferPackAck_50020 extends Ack {

    /**
     * @param messageLite
     */
    public JoloGame_ApplyOfferPackAck_50020(int functionId) {
        super(functionId);
    }
}
