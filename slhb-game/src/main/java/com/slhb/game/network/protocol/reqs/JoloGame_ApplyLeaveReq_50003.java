package com.slhb.game.network.protocol.reqs;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.core.common.log.LoggerUtils;
import com.slhb.base.enums.ErrorCodeEnum;
import com.slhb.base.enums.GameConst;
import com.slhb.base.enums.TableStateEnum;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.network.protocol.logic.LeaveTableLogic;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.play.TableUtil;
import com.slhb.game.service.PlayerService;
import com.slhb.game.service.TableService;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

/**
 * 申请离桌
 */
@Slf4j
public class JoloGame_ApplyLeaveReq_50003 extends Req {

    private long time;

    public JoloGame_ApplyLeaveReq_50003(int functionId) {
        super(functionId);
    }

    private JoloGame.JoloGame_ApplyLeaveReq req;

    @Override
    public void readPayLoadImpl(ByteBuf buf) throws Exception {
        time = System.currentTimeMillis();
        byte[] blob = new byte[buf.readableBytes()];
        buf.readBytes(blob);
        req = JoloGame.JoloGame_ApplyLeaveReq.parseFrom(blob);
        this.setTable(TableService.getInstance().getTable(reqHeader.gameId + "", req.getRoomId(), req.getTableId()));
    }

    @Override
    public void processImpl() throws Exception {
        log.debug("收到消息-> " + functionId + " reqNum-> " + reqHeader.reqNum + "req->" + req.toString());
        String userId = this.userId;
        PlayerService.getInstance().onPlayerLoutOut("" + userId);
        AbstractTable table = getTable();
        if (table == null) {
            log.error("table is null ");
            return;
        }
        PlayerInfo player = table.getPlayer(userId);
        JoloGame.JoloGame_ApplyLeaveAck.Builder ack = JoloGame.JoloGame_ApplyLeaveAck.newBuilder();
        try {

            ack.setUserId(userId);
            ack.setRoomId(req.getRoomId());
            ack.setTableId(req.getTableId());
            ack.setResult(1);

            if (table.getTableStateEnum().equals(TableStateEnum.IDEL) ||
                    table.getTableStateEnum().equals(TableStateEnum.SETTLE)) {

            } else {
                log.error("请在本局结束后操作 state:" + table.getTableStateEnum());
                ack.setResult(-1).setResultMsg("请在本局结束后操作");
                sendResponse(GateFunctionFactory.__function__id_50003 | 0x08000000, ack.build().toByteArray());
                return;
            }
            if (null == player) {
                log.error("can't found player info, playerId->" + userId);
                ack.setResult(1).setResultMsg(ErrorCodeEnum.GAME_50050_2.getCode());
                sendResponse(GateFunctionFactory.__function__id_50003 | 0x08000000, ack.build().toByteArray());
                return;
            }

            LeaveTableLogic.getInstance().logic(player, table, ack);

            //输出结果给客户端
            sendResponse(GateFunctionFactory.__function__id_50003 | 0x08000000, ack.build().toByteArray());

        } catch (Exception ex) {
            //TODO 是否必须字段都加上了
            ack.setResult(-10).setResultMsg(ErrorCodeEnum.GAME_50002_2.getCode());
            ack.setCurrStoreScore(0);
            sendResponse(GateFunctionFactory.__function__id_50003 | 0x08000000, ack.build().toByteArray());
            log.error("", ex);
        } finally {
            log.debug("Leave ACK info: " + ack.toString());
            log.debug("Leave ACK bytes length: " + ack.build().toByteArray().length);
            if (null != table) {
                log.debug("All Player info: " + System.getProperty("line.separator") + TableUtil.toStringAllPlayers(table));

                log.debug("InGame Player info: " + System.getProperty("line.separator") + TableUtil.toStringInGamePlayers(table));
            }
            log.debug("Leave over. Table state: " + table.getTableStateEnum());
        }
    }
}
