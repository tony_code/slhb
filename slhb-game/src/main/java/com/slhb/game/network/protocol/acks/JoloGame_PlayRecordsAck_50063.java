package com.slhb.game.network.protocol.acks;

import com.slhb.game.gate.network.protocol.Ack;

/**
 * @author
 * @since 2018/9/11 18:53
 */
public class JoloGame_PlayRecordsAck_50063 extends Ack {
    /**
     * @param messageLite
     * @param header
     */
    public JoloGame_PlayRecordsAck_50063(int functionId) {
        super(functionId);
    }
}
