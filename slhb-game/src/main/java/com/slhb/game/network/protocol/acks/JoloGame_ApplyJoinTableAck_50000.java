package com.slhb.game.network.protocol.acks;

import com.slhb.game.gate.network.protocol.Ack;

/**
 * 心跳消息网关->游戏服务器回复
 *
 * 申请入桌
 */
public class JoloGame_ApplyJoinTableAck_50000 extends Ack {

    /**
     * @param messageLite
     */
    public JoloGame_ApplyJoinTableAck_50000(int functionId) {
        super(functionId);
    }
}
