package com.slhb.game.network.protocol.acks;

import com.slhb.game.gate.network.protocol.Ack;

/**
 * 心跳消息网关->游戏服务器回复
 *
 * 申请入座
 */
public class JoloGame_ApplySitDownAck_50001 extends Ack {

    /**
     * @param messageLite
     */
    public JoloGame_ApplySitDownAck_50001(int functionId) {
        super(functionId);
    }
}
