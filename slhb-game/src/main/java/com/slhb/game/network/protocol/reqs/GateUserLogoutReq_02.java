package com.slhb.game.network.protocol.reqs;

import com.slhb.game.network.protocol.ClientReq;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

/**
 * 玩家下线请求
 */
@Slf4j
public class GateUserLogoutReq_02 extends ClientReq {

    public GateUserLogoutReq_02(int functionId) {
        super(functionId);
    }

    @Override
    public void readPayLoadImpl(ByteBuf byteBuf) throws Exception {

    }

    @Override
    public void processImpl() {
        log.error("收到无用消息 founctionId = {}", functionId);
    }

}
