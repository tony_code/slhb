package com.slhb.game.network.protocol.reqs;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.core.common.log.LoggerUtils;
import com.slhb.base.enums.*;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.play.TableUtil;
import com.slhb.game.service.TableService;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

/**
 * 申请站起
 */
@Slf4j
public class JoloGame_ApplyStandUpReq_50002 extends Req {
    private long time;
    public JoloGame_ApplyStandUpReq_50002(int functionId) {
        super(functionId);
    }

    private JoloGame.JoloGame_ApplyStandUpReq req;

    @Override
    public void readPayLoadImpl(ByteBuf buf) throws Exception {
        time = System.currentTimeMillis();
        byte[] blob = new byte[buf.readableBytes()];
        buf.readBytes(blob);
        req = JoloGame.JoloGame_ApplyStandUpReq.parseFrom(blob);
        this.setTable(TableService.getInstance().getTable(reqHeader.gameId + "", req.getRoomId(), req.getTableId()));
    }

    @Override
    public void processImpl() throws Exception {
        log.debug("收到消息-> " + functionId + ", reqNum-> " + reqHeader.reqNum + ", req->" + req.toString());
        String userId = this.userId;
        String roomId = req.getRoomId();
        String tableId = req.getTableId();
        AbstractTable table = getTable();
        PlayerInfo player = table.getPlayer(userId);
        JoloGame.JoloGame_ApplyStandUpAck.Builder ack = JoloGame.JoloGame_ApplyStandUpAck.newBuilder();

        try {
            ack.setUserId(userId);
            ack.setRoomId(roomId);
            ack.setTableId(tableId);
            ack.setResult(1);

            if (table.getTableStateEnum().equals(TableStateEnum.IDEL) ||
                    table.getTableStateEnum().equals(TableStateEnum.SETTLE)) {

            } else {
                log.error("请在本局结束后操作 state:" + table.getTableStateEnum());
                ack.setResult(-1).setResultMsg("请在本局结束后操作");
                sendResponse(GateFunctionFactory.__function__id_50002 | 0x08000000, ack.build().toByteArray());
                return;
            }
            if (null == player) {
                log.error("can't found player info, playerId->" + userId);
                ack.setResult(-1).setResultMsg(ErrorCodeEnum.GAME_50050_2.getCode());
                sendResponse(GateFunctionFactory.__function__id_50002 | 0x08000000, ack.build().toByteArray());
                return;
            }

            if (player.getState().getValue() == PlayerStateEnum.spectator.getValue()) {
                log.error("player is a spectator, playerId->" + userId);
                ack.setResult(-2).setResultMsg(ErrorCodeEnum.GAME_50014_1.getCode());
                sendResponse(GateFunctionFactory.__function__id_50002 | 0x08000000, ack.build().toByteArray());
                return;
            }

            if (player != null && player.getState().getValue() >= PlayerStateEnum.sitdown.getValue()) {
                if (table.standUp(player.getSeatNum(), player.getPlayerId(), "standUpReq")) {
                    if (player.getRoleType() != null && player.getRoleType().equals(RoleType.ROBOT)) {
                        LoggerUtils.robot.info("Robot standUp reason apply_stand_up id:" + player.getPlayerId() + ",gameId:" + table.getPlayType()
                                + ",roomId:" + table.getRoomId() + ",tableId:" + table.getTableId());
                    }
                    double currentMoney = table.playerDataSettlement(player);

                    ack.setCurrStoreScore(currentMoney);
                    sendResponse(GateFunctionFactory.__function__id_50002 | 0x08000000, ack.build().toByteArray());
                } else {//最好不要走到这里来
                    ack.setResult(-2).setResultMsg(ErrorCodeEnum.GAME_50002_1.getCode());
                    sendResponse(GateFunctionFactory.__function__id_50002 | 0x08000000, ack.build().toByteArray());
                    return;
                }
            }

        } catch (Exception ex) {
            log.error("系统异常");
            ack.setResult(-10).setResultMsg(ErrorCodeEnum.GAME_50002_1.getCode());
            sendResponse(GateFunctionFactory.__function__id_50002 | 0x08000000, ack.build().toByteArray());
            log.error("", ex);
        } finally {
            log.debug("StandUp ACK info: " + ack.toString());
            log.debug("StandUp ACK bytes length: " + ack.build().toByteArray().length);
            if (null != table) {
                log.debug("All Player info: " + System.getProperty("line.separator") + TableUtil.toStringAllPlayers(table));

                log.debug("InGame Player info: " + System.getProperty("line.separator") + TableUtil.toStringInGamePlayers(table));
            }
            log.debug("StandUp over. Table state: " + table.getTableStateEnum());
        }
    }
}
