package com.slhb.game.network.protocol.reqs;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.base.enums.ErrorCodeEnum;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.model.RedEnv;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.room.service.RoomConfigService;
import com.slhb.game.service.RedEnvService;
import com.slhb.game.service.TableService;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

/**
 * 抢庄
 */
@Slf4j
public class JoloGame_ApplyOfferPackReq_50020 extends Req {

    public JoloGame_ApplyOfferPackReq_50020(int functionId) {
        super(functionId);
    }

    private JoloGame.JoloGame_OfferPackReq req;

    @Override
    public void readPayLoadImpl(ByteBuf buf) throws Exception {
        byte[] blob = new byte[buf.readableBytes()];
        buf.readBytes(blob);
        req = JoloGame.JoloGame_OfferPackReq.parseFrom(blob);
        this.setTable(TableService.getInstance().getTable(reqHeader.gameId + "", req.getRoomId(), req.getTableId()));
    }

    @Override
    public void processImpl() throws Exception {
        log.debug("收到消息-> functionId={}, req={}" , functionId , req.toString());
        String userId = this.userId;
        String roomId = req.getRoomId();
        String tableId = req.getTableId();
        String gameOrderId = req.getGameOrderId();
        int money = req.getMoney();
        int thunder = req.getThunder();
        int times = req.getTimes();

        AbstractTable table = getTable();
        JoloGame.JoloGame_OfferPackAck.Builder ack = JoloGame.JoloGame_OfferPackAck.newBuilder();
        ack.setUserId(userId);
        ack.setRoomId(roomId);
        ack.setTableId(tableId);
        ack.setResult(1);

        try {
            if (null == table) {
                log.error("未找到table");
                ack.setResult(-1).setResultMsg("找不到当前桌子");
                sendResponse(GateFunctionFactory.__function__id_50020_SC, ack.build().toByteArray());
                return;
            }

            RoomConfigModel cfg = RoomConfigService.OBJ.getRoomConfig(roomId);
            if (null == cfg){
                log.error("房间配置 roomId = {}不存在",roomId);
                ack.setResult(-1).setResultMsg("配置不存在");
                sendResponse(GateFunctionFactory.__function__id_50020_SC, ack.build().toByteArray());
                return;
            }

            if (cfg.getMaxEnv() < money || cfg.getMinEnv() > money){
                log.error("筹码超限 min = {}, max = {},cur = {}", cfg.getMinEnv(), cfg.getMaxEnv(), money);
                ack.setResult(-1).setResultMsg("红包金额超出房间限制");
                sendResponse(GateFunctionFactory.__function__id_50020_SC, ack.build().toByteArray());
                return;
            }

            if (thunder < 0 || thunder > 9){
                log.error("雷号设置错误 thunder = {}", thunder);
                ack.setResult(-1).setResultMsg("红包雷号设置错误");
                sendResponse(GateFunctionFactory.__function__id_50020_SC, ack.build().toByteArray());
                return;
            }

            if (times <= 0){
                log.error("红包数量 times = {}",times);
                ack.setResult(-1).setResultMsg("红包数量不能小于1");
                sendResponse(GateFunctionFactory.__function__id_50020_SC, ack.build().toByteArray());
                return;
            }

            PlayerInfo sender = table.getPlayer(userId);
            if (null == sender){
                log.error("玩家userId = {}不存在",userId);
                ack.setResult(-1).setResultMsg("玩家不在桌内");
                sendResponse(GateFunctionFactory.__function__id_50020_SC, ack.build().toByteArray());
                return;
            }

            double totalMoney = money * times;
            if (sender.getPlayScoreStore() < totalMoney){
                log.error("玩家userId = {}筹码不足", userId);
                ack.setResult(-1).setResultMsg(ErrorCodeEnum.GAME_50013_3.getDesc());
                sendResponse(GateFunctionFactory.__function__id_50020_SC, ack.build().toByteArray());
                return;
            }

            //开始扣钱了
            sender.minusPlayScoreStore(totalMoney);

            //构建红包
            for (int index = 0; index < times; index ++){
                RedEnv redEnv = RedEnvService.OBJ.createPlayerRedenv(userId, roomId, thunder, money);
                if (null == redEnv){
                    continue;
                }

                //塞红包到桌内
                table.getRedEnvList().add(redEnv);
            }

            sendResponse(GateFunctionFactory.__function__id_50020_SC, ack.build().toByteArray());
        } catch (Exception ex) {
            ack.setResult(-10).setResultMsg(ErrorCodeEnum.GAME_50002_2.getCode());
            sendResponse(GateFunctionFactory.__function__id_50020_SC, ack.build().toByteArray());
            log.error(ex.getMessage(), ex);
        } finally {
            log.debug("响应消息-> " + functionId + ", ack->" + ack.toString());
        }
    }
}
