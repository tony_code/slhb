package com.slhb.game.network.protocol.reqs;

import JoloProtobuf.GameSvr.JoloGame;
import com.google.common.base.Strings;
import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.ErrorCodeEnum;
import com.slhb.base.enums.TableStateEnum;
import com.slhb.base.model.GameRoomTableSeatRelationModel;
import com.slhb.base.model.RoomTableRelationModel;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.play.TableUtil;
import com.slhb.game.room.cache.TableUserSet;
import com.slhb.game.room.cache.UserTableSet;
import com.slhb.game.room.service.RoomConfigService;
import com.slhb.game.service.PlayerService;
import com.slhb.game.service.RoomStateService;
import com.slhb.game.service.TableService;
import com.slhb.game.service.TimerService;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

@Slf4j
public class JoloGame_ReconnectReq_50016 extends Req {
    private long time;
    private JoloGame.JoloGame_ReconnectReq req;

    public JoloGame_ReconnectReq_50016(int functionId) {
        super(functionId);
    }

    @Override
    public void readPayLoadImpl(ByteBuf buf) throws Exception {
        time = System.currentTimeMillis();
        log.debug("收到消息");
        byte[] blob = new byte[buf.readableBytes()];
        buf.readBytes(blob);
        req = JoloGame.JoloGame_ReconnectReq.parseFrom(blob);
    }

    @Override
    public void processImpl() throws Exception {
        //1很正常坐下
        //2已被站起 随机坐下or没座位被弹出到大厅
        log.debug("收到消息, functionId->" + functionId + ", reqNum->" + reqHeader.reqNum + ", req->" + req.toString());
        JoloGame.JoloGame_ReconnectAck.Builder ack = JoloGame.JoloGame_ReconnectAck.newBuilder();
        AbstractTable table = null;

        String userId = this.userId;
        int seatNum = req.getSeatNum();
        double buyInScore = -1;
        ack.setUserId(userId);
        ack.setSeatNum(seatNum);
        ack.setGameOrderId("0");
        ack.setRoomId("");
        ack.setTableId("");
        try {
            GameRoomTableSeatRelationModel roomTableSeat = UserTableSet.OBJ.get(userId);
            if (roomTableSeat == null) {
                log.error("user={},not in table ", userId);
                ack.setSeatNum(0);
                ack.setResult(-1).setResultMsg(ErrorCodeEnum.GAME_50001_1.getCode());
                sendResponse(GateFunctionFactory.__function__id_50016 | 0x08000000, ack.build().toByteArray());
                return;
            }
            String roomId = roomTableSeat.getRoomId();
            String tableId = roomTableSeat.getTableId();

            RoomTableRelationModel roomTable = RoomStateService.getInstance()
                    .getExistTable(roomTableSeat.getGameId(), roomTableSeat.getRoomId(), roomTableSeat.getTableId());
            if (roomTable == null) {
                log.error("can't found table, gameId:{},roomId:{},tableId:{}", reqHeader.gameId, roomId, tableId);
                ack.setSeatNum(0);
                ack.setResult(-1).setResultMsg(ErrorCodeEnum.GAME_50001_1.getCode());
                sendResponse(GateFunctionFactory.__function__id_50016 | 0x08000000, ack.build().toByteArray());
                return;
            }

            table = TableService.getInstance().getTable(reqHeader.gameId + "", roomId, tableId);
            if (table == null) {
                log.debug("  -12  can't found user, userId->" + userId);
                ack.setSeatNum(0);
                ack.setResult(-12).setResultMsg(ErrorCodeEnum.GAME_50001_2.getCode());
                sendResponse(GateFunctionFactory.__function__id_50016 | 0x08000000, ack.build().toByteArray());
                return;
            }
            //用户信息：从缓存获取
            User user = PlayerService.getInstance().getUser(userId);
            if (null == user) {
                table.returnLobby(userId, false);
                log.debug("  -2  can't found user, userId->" + userId);
                ack.setSeatNum(0);
                ack.setResult(-2).setResultMsg(ErrorCodeEnum.GAME_50001_2.getCode());
                sendResponse(GateFunctionFactory.__function__id_50016 | 0x08000000, ack.build().toByteArray());
                return;
            }
            log.debug("玩家当前的积分数量：==============" + user.getMoney());

            PlayerInfo player = new PlayerInfo(roomTable ,user);

            String res = TableUserSet.OBJ.get(player.getRoomId()+player.getTableId(), player.getPlayerId());
            if (Strings.isNullOrEmpty(res)) {
                table.returnLobby(userId, false);
                log.error("  -10  桌内查无此人, userId->" + userId + ", roomId->" + roomId + ",tableId->" + tableId);
                ack.setSeatNum(0);
                ack.setResult(-10).setResultMsg(ErrorCodeEnum.GAME_50050_2.getCode());
                sendResponse(GateFunctionFactory.__function__id_50016 | 0x08000000, ack.build().toByteArray());
                return;
            }

            if (user.getMoney() <= 0) {
                log.debug("  -3  用户余额不足, userId->" + userId + ", money->" + user.getMoney());
                ack.setSeatNum(0);
                ack.setResult(-3).setResultMsg(ErrorCodeEnum.GAME_50013_3.getCode());
                sendResponse(GateFunctionFactory.__function__id_50016 | 0x08000000, ack.build().toByteArray());
                return;
            }

            if (req.hasBuyInScore()) {
                buyInScore = req.getBuyInScore();
            } else {
                buyInScore = user.getMoney();
            }

            //创建player对象
            player = table.getPlayer(userId);
            log.debug("玩家信息={}",player);
            if (null == player) {
                table.returnLobby(userId, false);
                log.debug("  -5  can't found suitable player. player->" + player);
                ack.setSeatNum(0);
                ack.setResult(-5).setResultMsg(ErrorCodeEnum.GAME_50050_2.getCode());
                sendResponse(GateFunctionFactory.__function__id_50016 | 0x08000000, ack.build().toByteArray());
                return;
            }
            player.setOffLine(false);

            RoomConfigModel roomConfig = RoomConfigService.OBJ.getRoomConfig(roomId);
            log.debug("获取房间信息reconnect 1");
            ack.setResult(1);
            ack.setTableState(table.getTableStateEnum().getValue());
            ack.setCurrPlayScore(buyInScore);
            log.debug("获取房间信息reconnect 2");
            //初始化ack信息
            ack.setRoomId(roomId);
            ack.setTableId(tableId);
            ack.setSeatNum(player.getSeatNum());
            log.debug("获取房间信息reconnect 3");
            //本局癞子牌
            //本局换出去的牌
            ack.setGameOrderId(table.getCurrGameOrderId());
            log.debug("获取房间信息reconnect 4");
            //判断玩家在不在游戏中
            if (player.getState().getValue() > 1) {
                ack.setNotInGame(0);
            } else {
                ack.setNotInGame(1);
            }
            log.debug("获取房间信息reconnect 5");
            if (table.getTableStateEnum().getValue() < TableStateEnum.SETTLE.getValue() ) {
                ack.setCurrActionSurplusTime(TimerService.getInstance().getLeftCountDown(table.getRoomTableRelation()));
            }
            log.debug("获取牌局玩家信息");
            ack.addAllPlayerInfoList(TableUtil.getPlayers(table, player));
            log.debug("发送消息");
            sendResponse(GateFunctionFactory.__function__id_50016 | 0x08000000, ack.build().toByteArray());
            log.debug("玩家入座成功：" + player.toSitDownString());

            //如果在座玩家超过两人并且桌子状态为空闲状态
            //开始游戏
            TableService.getInstance().playGame(table);
            PlayerService.getInstance().onPlayerLogin(userId);

        } catch (Exception e) {
            log.error("reconnect error", e);
            ack.setSeatNum(0).setResult(-11).setResultMsg(ErrorCodeEnum.GAME_50002_2.getCode());
            sendResponse(GateFunctionFactory.__function__id_50016 | 0x08000000, ack.build().toByteArray());
        } finally {
            log.debug("ACK info: " + ack.toString());
            if (null != table) {
                log.debug("All Player info: " + System.getProperty("line.separator") + TableUtil.toStringAllPlayers(table));
                log.debug("InGame Player info: " + System.getProperty("line.separator") + TableUtil.toStringInGamePlayers(table));
            }
        }

    }
}
