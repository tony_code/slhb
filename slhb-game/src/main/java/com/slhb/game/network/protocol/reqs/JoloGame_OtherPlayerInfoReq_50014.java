package com.slhb.game.network.protocol.reqs;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.core.common.log.LoggerUtils;
import com.slhb.base.enums.ErrorCodeEnum;
import com.slhb.base.enums.GameConst;
import com.slhb.base.enums.PlayerStateEnum;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.service.TableService;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

/**
 * 记录本桌内信息 入桌清空
 */
@Slf4j
public class JoloGame_OtherPlayerInfoReq_50014 extends Req {
    private long time;

    public JoloGame_OtherPlayerInfoReq_50014(int functionId) {
        super(functionId);
    }

    private JoloGame.JoloGame_OtherPlayerInfoReq req;

    @Override
    public void readPayLoadImpl(ByteBuf buf) throws Exception {
        time = System.currentTimeMillis();
        byte[] blob = new byte[buf.readableBytes()];
        buf.readBytes(blob);
        req = JoloGame.JoloGame_OtherPlayerInfoReq.parseFrom(blob);
        this.setTable(TableService.getInstance().getTable(reqHeader.gameId + "", req.getRoomId(), req.getTableId()));
    }

    @Override
    public void processImpl() throws Exception {
        log.debug("收到消息-> " + functionId + ", reqNum-> " + reqHeader.reqNum + ", req->" + req.toString());
        String userId = this.userId;
        int seatNum = req.getSeatNum();
        AbstractTable table = getTable();
        PlayerInfo player = table.getPlayer(userId);
        JoloGame.JoloGame_OtherPlayerInfoAck.Builder ack = JoloGame.JoloGame_OtherPlayerInfoAck.newBuilder();
        try {
            ack.setResult(1);

            if (null == player) {
                ack.setResult(-1).setResultMsg(ErrorCodeEnum.GAME_50050_2.getCode());
                sendResponse(GateFunctionFactory.__function__id_50014 | 0x08000000, ack.build().toByteArray());
                return;
            }

            if (player.getState().getValue() == PlayerStateEnum.spectator.getValue()) {
                ack.setResult(-2).setResultMsg(ErrorCodeEnum.GAME_50014_1.getCode());
                sendResponse(GateFunctionFactory.__function__id_50014 | 0x08000000, ack.build().toByteArray());
                return;
            }

            if (player != null && player.getState().getValue() >= PlayerStateEnum.sitdown.getValue()) {
                PlayerInfo pi = table.getAllPlayers().get(player.getPlayerId());
                if (pi == null) {
                    ack.setResult(-3).setResultMsg(ErrorCodeEnum.GAME_50014_2.getCode());
                    sendResponse(GateFunctionFactory.__function__id_50014 | 0x08000000, ack.build().toByteArray());
                    return;
                }

                JoloGame.JoloGame_TablePlay_OtherPlayerInfo.Builder playerInfo = JoloGame.JoloGame_TablePlay_OtherPlayerInfo.newBuilder();
                playerInfo.setUserId(pi.getPlayerId());
                playerInfo.setNickName(pi.getNickName());
                playerInfo.setPlayScoreStore(pi.getPlayScoreStore());
                playerInfo.setIcon(pi.getIcon());
                sendResponse(GateFunctionFactory.__function__id_50014 | 0x08000000, ack.build().toByteArray());
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        } finally {
        }
    }
}
