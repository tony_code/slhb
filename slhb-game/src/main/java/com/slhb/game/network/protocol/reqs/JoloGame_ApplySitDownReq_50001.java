package com.slhb.game.network.protocol.reqs;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.*;
import com.slhb.base.model.GameRoomTableSeatRelationModel;
import com.slhb.base.model.RoomTableRelationModel;
import com.slhb.game.config.Config;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.room.cache.UserTableSet;
import com.slhb.game.room.service.PlayerQueueService;
import com.slhb.game.room.service.RoomConfigService;
import com.slhb.game.service.PlayerService;
import com.slhb.game.service.TableService;
import com.slhb.game.service.holder.RoomConfigHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import java.util.ArrayList;

/**
 * 申请入座(无指定座位随机)
 * return：
 */
@Slf4j
public class JoloGame_ApplySitDownReq_50001 extends Req {
    private long time;

    public JoloGame_ApplySitDownReq_50001(int functionId) {
        super(functionId);
    }

    private JoloGame.JoloGame_ApplySitDownReq req;

    @Override
    public void readPayLoadImpl(ByteBuf buf) throws Exception {
        time = System.currentTimeMillis();
        byte[] blob = new byte[buf.readableBytes()];
        buf.readBytes(blob);
        req = JoloGame.JoloGame_ApplySitDownReq.parseFrom(blob);
    }

    @Override
    public void processImpl() throws Exception {
        log.debug("收到消息, functionId->" + functionId + ", reqNum->" + reqHeader.reqNum + ", req->" + req.toString());
        JoloGame.JoloGame_ApplySitDownAck.Builder ack = JoloGame.JoloGame_ApplySitDownAck.newBuilder();

        String userId = this.userId;
        String roomId = req.getRoomId();
        String tableId = req.getTableId();
        String gameId = String.valueOf(Config.GAME_ID);

        int seatNum = req.getSeatNum();
        double buyInScore = -1;

        ack.setUserId(userId);
        ack.setRoomId(roomId);
        ack.setTableId("");
        ack.setSeatNum(0);
        ack.setResult(0);
        ack.setResult(1);
        ack.setTableState(TableStateEnum.IDEL.getValue());
        ack.setCurrPlayScore(buyInScore);
        ack.setTotalAlreadyBet(0);
        ack.setGameOrderId("");
        //判断玩家在不在游戏中
        ack.setNotInGame(1);
        ack.addAllPlayerInfoList(new ArrayList<>());

        AbstractTable table = null;
        try {
            //用户信息：从缓存获取
            User user = PlayerService.getInstance().getUser(userId);
            if (null == user) {
                log.debug("  -2  can't found user, userId->" + userId);
                ack.setSeatNum(0);
                ack.setResult(-2).setResultMsg(ErrorCodeEnum.GAME_50001_2.getCode());
                sendResponse(GateFunctionFactory.__function__id_50001 | 0x08000000, ack.build().toByteArray());
                return;
            }

            //首先查看自己有没有在游戏内
            GameRoomTableSeatRelationModel gameRoomTable = UserTableSet.OBJ.get(userId);
            if (gameRoomTable != null) {
                table = TableService.getInstance().getTable(gameRoomTable.getGameId(), gameRoomTable.getRoomId(), gameRoomTable.getTableId());
            }

            if (gameRoomTable != null && RoleType.ROBOT != RoleType.getRoleType(user.getChannel_id())){
                log.error("玩家已在座内");
                ack.setResult(-15);
                sendResponse(GateFunctionFactory.__function__id_50001 | 0x08000000, ack.build().toByteArray());
                return;
            }

            RoomTableRelationModel model = new RoomTableRelationModel(gameId, roomId, tableId,TableStateEnum.IDEL.getValue());
            PlayerInfo player = new PlayerInfo(model, user);
            if (user.getMoney() <= 0) {
                ack.setSeatNum(0);
                ack.setResult(-3).setResultMsg(ErrorCodeEnum.GAME_50013_3.getCode());
                sendResponse(GateFunctionFactory.__function__id_50001 | 0x08000000, ack.build().toByteArray());
                return;
            }

            buyInScore = user.getMoney();
            double currScoreStore = user.getMoney(); //玩家当前积分库存
            RoomConfigModel roomconfig = RoomConfigHolder.getInstance().getRoomConfig(roomId);
            /*if (currScoreStore < roomconfig.getMinScore4JoinTable()) {
                ack.setSeatNum(0);
                ack.setResult(-12).setResultMsg(ErrorCodeEnum.GAME_50013_3.getCode());
                sendResponse(GateFunctionFactory.__function__id_50001 | 0x08000000, ack.build().toByteArray());
                return;
            }*/

            if (!req.hasRoomId()) {
                ack.setSeatNum(0);
                ack.setResult(-4).setResultMsg(ErrorCodeEnum.GAME_50001_3.getCode());
                sendResponse(GateFunctionFactory.__function__id_50001 | 0x08000000, ack.build().toByteArray());
                return;
            }

            if (null == player) {
                log.debug("  -5  can't found suitable player. player->" + player);
                ack.setSeatNum(0);
                ack.setResult(-5).setResultMsg(ErrorCodeEnum.GAME_50050_2.getCode());
                sendResponse(GateFunctionFactory.__function__id_50001 | 0x08000000, ack.build().toByteArray());
                return;
            }

            ack.setResult(1);
            ack.setCurrPlayScore(user.getMoney());
            ack.setTotalAlreadyBet(0);

            RoomConfigModel config = RoomConfigService.OBJ.getRoomConfig(roomId);
            if (config == null) {
                log.debug(player.playerToString() + "玩家积分低于当前房间最低下注额 ante->" + config.getMinEnv());
                ack.setSeatNum(0);
                ack.setResult(-11).setResultMsg(ErrorCodeEnum.GAME_50013_3.getCode());
                sendResponse(GateFunctionFactory.__function__id_50001 | 0x08000000, ack.build().toByteArray());
                return;
            }

            ack.setNotInGame(1).addAllPlayerInfoList(new ArrayList<>());
            sendResponse(GateFunctionFactory.__function__id_50001 | 0x08000000, ack.build().toByteArray());

            //排队入座
            PlayerQueueService.OBJ.addPlayer(user.getChannel_id(),roomId,player);

            if (!user.getChannel_id().equals(RoleType.ROBOT.getTypeName())) {
                PlayerService.getInstance().onPlayerLogin(userId);
            }

        } catch (Exception e) {
            ack.setSeatNum(0).setResult(-15).setResultMsg(ErrorCodeEnum.GAME_50002_2.getCode());//座位号是必须的字段
            sendResponse(GateFunctionFactory.__function__id_50001 | 0x08000000, ack.build().toByteArray());
            log.error("", e);
        } finally {
        }
    }
}
