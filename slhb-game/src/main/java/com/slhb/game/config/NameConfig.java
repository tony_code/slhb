package com.slhb.game.config;

import com.slhb.game.robot.utils.RandomTools;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 虚拟角色名池
 */
@Slf4j
public class NameConfig {

    public static final NameConfig ME = new NameConfig();

    //机器人昵称池
    private List<String> _namePool = new ArrayList<>();

    public void init(){
        //每小时加载一次名昵称库
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(()->loadNameFile(), 0, 1, TimeUnit.HOURS);
    }

    /**
     * 来个名字
     * @return
     */
    public String nextName(){
        if (_namePool.size() <= 0){
            return "";
        }

        int idx = RandomTools.getRandomInt(_namePool.size()-1);
        return _namePool.get(idx);
    }

    private void loadNameFile(){
        List<String> listTmp = readFile("./config/names.properties");
        if (listTmp == null || listTmp.size() <= 0){
            log.info("读到的name库为空");
            return;
        }

        //替换旧有昵称池
        _namePool = listTmp;
    }

    private List<String> readFile(String path){
        List<String> list = new ArrayList<>();
        try {
            InputStreamReader isr = new InputStreamReader(new FileInputStream(path));
            BufferedReader buffer = new BufferedReader(isr);

            String line = "";
            while ((line = buffer.readLine()) != null){
                //此行为空
                if (StringUtils.isEmpty(line)){
                    continue;
                }

                //此行包含注释行
                if (line.contains("#")){
                    continue;
                }

                list.add(line);
            }

            buffer.close();
            isr.close();
        }catch (Exception ex){
            log.debug("读取文件失败", ex);
        }
        return list;
    }

}
