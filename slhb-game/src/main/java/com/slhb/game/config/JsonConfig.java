package com.slhb.game.config;

import com.google.common.reflect.TypeToken;
import com.slhb.core.jedis.StoredCommManager;
import com.slhb.game.dao.bean.CommonConfigModel;
import com.slhb.game.dao.bean.KillPoolConfig;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.utils.JsonWorker;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


@Slf4j@Getter
public class JsonConfig {

    public static final JsonConfig ME = new JsonConfig();

    private static final String charset = "utf-8";

    //房间配置key
    private static final String key_room = "roomconfig_syn_"+Config.GAME_ID;
    //杀率配置key
    private static final String key_kill = "killconfig_syn_"+Config.GAME_ID;

    private List<RoomConfigModel> roomConfigs = new ArrayList<>();

    private List<CommonConfigModel> commonConfigs = new ArrayList<>();

    private List<KillPoolConfig>  killPoolConfigs = new ArrayList<>();

    /**
     * 初始化配置
     */
    public void init (){
        //如果配置有变化，最新配置写入redis
        //定时查询redis,看有没有需要拉取得配置
        //如果有配置变化，拉到本地文本
        //全服都使用的是最新配置文件
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(()->synFromRedis(), 0,5, TimeUnit.MINUTES);
    }

    private void loadConfigs(){
        //读取房间配置
        String roomFile = "./config/json/RoomConfig.json";
        String roomJson = readFile(roomFile);
        log.info("读取房间配置roomJson={}",roomJson);
        roomConfigs = JsonWorker.OBJ.getGson().fromJson(roomJson, new TypeToken<List<RoomConfigModel>>() {}.getType());


        //读取通用配置
        String commonFile = "./config/json/CommonConfig.json";
        String commonJson = readFile(commonFile);
        log.info("读取通用配置commonJson={}",commonJson);
        commonConfigs = JsonWorker.OBJ.getGson().fromJson(commonJson, new TypeToken<List<CommonConfigModel>>() {}.getType());

        //读取杀率配置
        String killPoolFile = "./config/json/KillPoolConfig.json";
        String killPoolJson = readFile(killPoolFile);
        log.info("读取杀率配置killPoolJson={}",killPoolJson);
        killPoolConfigs = JsonWorker.OBJ.getGson().fromJson(killPoolJson, new TypeToken<List<KillPoolConfig>>() {}.getType());

    }

    //写房间配置
    public void writeRoom(List<RoomConfigModel> list){
        String roomFile = "./config/json/RoomConfig.json";
        writeFile(JsonWorker.OBJ.getGson().toJson(list), roomFile);
    }

    //写杀率配置
    public void writeKillPoll(List<KillPoolConfig> list){
        String killPoolFile = "./config/json/KillPoolConfig.json";
        writeFile(JsonWorker.OBJ.getGson().toJson(list), killPoolFile);
    }

    private String readFile (String path){
        String configJson = null;
        FileInputStream in = null;
        try {
            in = new FileInputStream(path);
            byte[] data = new byte[in.available()];
            in.read(data);
            configJson = new String(data, charset);
            in.close();
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return configJson;
    }

    /**
     * 写文本操作
     * @return
     */
    private boolean writeFile(String json , String path){
        try {
            FileOutputStream fos = new FileOutputStream(path);
            fos.write(json.getBytes(charset));
            fos.close();
        }catch (Exception e){
            log.error(e.getMessage(),e);
            return false;
        }
        return true;
    }

    /**
     * 从redis 同步配置到本地
     */
    public void synFromRedis(){
        synRoomFromRedis();
        synKillFromRedis();

        loadConfigs();
    }

    private void synRoomFromRedis(){
        try {
            //读取redis中的配置
            String roomConfigJson = StoredCommManager.get(key_room);
            log.info("同步房间配置key={},val={}",key_room, roomConfigJson);
            if (StringUtils.isEmpty(roomConfigJson)){
                return;
            }

            //反序列化
            List<RoomConfigModel> list = JsonWorker.OBJ.getGson().fromJson(roomConfigJson, new TypeToken<List<RoomConfigModel>>() {}.getType());
            if (list == null || list.isEmpty()){
                return;
            }

            //写入本地文本
            writeRoom(list);
        }catch (Exception ex){
            log.error("同步失败", ex);
        }

    }

    private void synKillFromRedis(){
        try {
            //读取redis中的配置
            String killConfigJson = StoredCommManager.get(key_kill);
            log.info("同步杀率配置key={},val={}",key_kill, killConfigJson);
            if (StringUtils.isEmpty(killConfigJson)){
                return;
            }

            //反序列化
            List<KillPoolConfig> list = JsonWorker.OBJ.getGson().fromJson(killConfigJson, new TypeToken<List<KillPoolConfig>>() {}.getType());
            if (list == null || list.isEmpty() ){
                return;
            }

            //写入本地文本
            writeKillPoll(list);
        }catch (Exception ex){
            log.error("同步失败", ex);
        }
    }

    public void synRoomToRedis(List<RoomConfigModel> list){
        if (list == null || list.isEmpty()) {
            return;
        }

        String roomConfigJson = JsonWorker.OBJ.getGson().toJson(list);
        StoredCommManager.set(key_room, roomConfigJson);
    }

    public void synKillToRedis(List<KillPoolConfig> list){
        if (list == null || list.isEmpty()) {
            return;
        }

        String killPoolJson = JsonWorker.OBJ.getGson().toJson(list);
        StoredCommManager.set(key_kill, killPoolJson);
    }

    public List<RoomConfigModel> getRoomFromRedis(){
        List<RoomConfigModel> list = new ArrayList<>();
        try {
            //读取redis中的配置
            String roomConfigJson = StoredCommManager.get(key_room);
            log.info("同步房间配置key={},val={}",key_room, roomConfigJson);
            if (StringUtils.isEmpty(roomConfigJson)){
                return list;
            }

            //反序列化
            list = JsonWorker.OBJ.getGson().fromJson(roomConfigJson, new TypeToken<List<RoomConfigModel>>() {}.getType());
            if (list == null || list.isEmpty()){
                return list;
            }

            return list;
        }catch (Exception ex){
            log.error("同步失败", ex);
        }
        return list;
    }

    public List<KillPoolConfig> getKillFromRedis(){
        try {
            //读取redis中的配置
            String killConfigJson = StoredCommManager.get(key_kill);
            log.info("同步杀率配置key={},val={}",key_kill, killConfigJson);
            if (StringUtils.isEmpty(killConfigJson)){
                return null;
            }

            //反序列化
            List<KillPoolConfig> list = JsonWorker.OBJ.getGson().fromJson(killConfigJson, new TypeToken<List<KillPoolConfig>>() {}.getType());
            if (list == null || list.isEmpty() ){
                return null;
            }

            return list;
        }catch (Exception ex){
            log.error("同步失败", ex);
        }
        return null;
    }
}
