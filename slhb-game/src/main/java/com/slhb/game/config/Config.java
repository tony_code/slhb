package com.slhb.game.config;

import com.slhb.core.configuration.ConfigurableProcessor;
import com.slhb.core.configuration.Property;
import com.slhb.core.configuration.ThreadConfig;
import com.slhb.core.jedis.JedisCommonConfig;
import com.slhb.core.utils.PropertiesUtils;
import lombok.extern.slf4j.Slf4j;
import java.util.Properties;

/**
 * @author
 */
@Slf4j
public class Config {

    @Property(key = "game.bind.ip", defaultValue = "127.0.0.1")
    public static String BIND_IP;

    @Property(key = "restful.ip", defaultValue = "127.0.0.1")
    public static  String REST_IP ;

    @Property(key = "restful.port", defaultValue = "8082")
    public static  int REST_PORT ;

    @Property(key = "load.db.config.hour", defaultValue = "5")
    public static int loadDBConfigHour;

    /**********************大厅接入参数**************************/
    @Property(key = "game.id", defaultValue = "1001")
    public static int GAME_ID;

    @Property(key = "game.serverId", defaultValue = "1")
    public static String GAME_SERID;

    @Property(key = "game.maxLoad", defaultValue = "500")
    public static int GAME_MAXLOAD;

    @Property(key = "game.gateurl", defaultValue = "ws://%s:%s/gate")
    public static String GAME_GATEURL;

    @Property(key = "game.gwcurl", defaultValue = "54001")
    public static int GAME_GWCURL;

    @Property(key = "game.reporturl", defaultValue = "")
    public static String GAME_REPORTURL;

    @Property(key = "game.accounturl", defaultValue = "")
    public static String GAME_ACCOUNTURL;

    @Property(key = "game.encryption.key", defaultValue = "")
    public static String GAME_ENCRYPE_KEY;

    @Property(key = "game.encryption.isOpen", defaultValue = "false")
    public static boolean GAME_ENCRYPE_ISOPEN;

    @Property(key = "game.gcurl", defaultValue = "http://172.31.11.30:7771/")
    public static String GAME_GCURL;

    /**********************gate参数**************************/

    @Property(key = "gateserver.bind.port", defaultValue = "8080")
    public static int GATE_BIND_PORT;

    @Property(key = "noticeserver.bind.port", defaultValue = "8090")
    public static int NOTICESERVER_BIND_PORT;

    @Property(key = "enable.ssl", defaultValue = "false")
    public static boolean ENABLE_SSL;

    @Property(key = "gateserver.islocal", defaultValue = "true")
    public static boolean GATESERVER_ISLOCAL;

    @Property(key = "gateserver.msgreversal", defaultValue = "false")
    public static boolean GATE_MSG_REVERSAL;


    /**********************room参数**************************/
    @Property(key = "room.init.desk.num", defaultValue = "2")
    public static int ROOM_INIT_DESK_NUM;

    @Property(key = "room.assigned.together", defaultValue = "true")
    public static boolean ROOM_ASSIGNED_TOGETHER;





    /**
     * Load configs from files.
     */
    public static void load(String dir) {
        try {
            Properties myProps = null;
            try {
                log.info("Loading: mycs.properties");
                myProps = PropertiesUtils.load("./config/mycs.properties");
            } catch (Exception e) {
                log.info("No override properties found");
            }

            Properties[] props = PropertiesUtils.loadAllFromDirectory("./config/network");
            PropertiesUtils.overrideProperties(props, myProps);

            log.info("Loading: game.properties");
            ConfigurableProcessor.process(Config.class, PropertiesUtils.loadAllFromDirectory("./config/network/" + dir + "/"));
            log.info("Loading: redis.properties");
            //ConfigurableProcessor.process(JedisConfig.class, props);
            log.info("Loading: redis_common.properties");
            ConfigurableProcessor.process(JedisCommonConfig.class, props);
            log.info("Loading: database.properties");
            //ConfigurableProcessor.process(DatabaseConfig.class, props);
            log.info("Loading: threadpool.properties");
            ConfigurableProcessor.process(ThreadConfig.class, props);
        } catch (Exception e) {
            log.error("Can't load Game configuration", e);
            throw new Error("Can't load Game configuration", e);
        }
    }
}
