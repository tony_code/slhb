package com.slhb.game.dao.bean;

import lombok.Data;

/**
 * 房间配置信息
 */
@Data
public class RoomConfigModel implements Comparable{
    private int id;//序列,用以数据库修改数据
    private String roomId; //房间ID
    private int minEnv; //红包最小
    private int maxEnv; //红包最大
    private double rate; //倍数
    private int num;
    private String onlinerRoles;//在线人数规则

    @Override
    public int compareTo(Object o){
        RoomConfigModel oModel = (RoomConfigModel)o;
        return (int)(this.id - oModel.id);
    }

}
