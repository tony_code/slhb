package com.slhb.game.dao.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * 杀量池配置
 */

@Getter@Setter@ToString
public class KillPoolConfig implements Comparable{

    private int id;

    //渠道ID
    private String channelId;

    //房间ID  1初级 2中级 3高级 4至尊场
    private String roomId;

    //初始资金池变化
    private double initRobotPool;

    //初始总杀量池
    private double initKillPool;

    //每局流水百分比
    private double flowRate;

    //最大杀量比例
    private double  maxKillRate;

    //最小杀量比例
    private double  minKillRate;

    //渠道服务费
    private int  serviceFee;

    @Override
    public int compareTo(Object o){
        return 0;
    }

    public double getServiceRate(){
        return serviceFee * 0.01d;
    }

}
