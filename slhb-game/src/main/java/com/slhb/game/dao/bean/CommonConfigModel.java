package com.slhb.game.dao.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * 房间通用的配置信息
 */
@Getter@Setter@ToString
public class CommonConfigModel {
    private int id;//序列,用以数据库修改数据

    /**抢红包倒计时*/
    private int robCD;

    /**结算倒计时*/
    private int settleCD;

}
