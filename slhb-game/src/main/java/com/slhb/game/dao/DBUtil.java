package com.slhb.game.dao;

import com.slhb.game.config.JsonConfig;
import com.slhb.game.dao.bean.*;
import com.slhb.game.room.service.RoomConfigService;
import com.slhb.game.service.RobotStrategyService;
import lombok.extern.slf4j.Slf4j;
import java.util.Iterator;
import java.util.List;


@Slf4j
public class DBUtil {

    public static int updateRoom(RoomConfigModel record) {
        List<RoomConfigModel> list = JsonConfig.ME.getRoomFromRedis();
        if (list == null || list.isEmpty()){
            list = RoomConfigService.OBJ.getConfigs();
        }

        //索引
        int idx = 0;

        Iterator<RoomConfigModel> iter = list.iterator();
        while (iter.hasNext()) {
            RoomConfigModel config = iter.next();

            //移除旧的
            if (config.getId() == record.getId()){
                idx = list.indexOf(config);
                iter.remove();
            }
        }

        //替换掉
        list.add(idx, record);

        //写入json文本
        JsonConfig.ME.writeRoom(list);
        JsonConfig.ME.synRoomToRedis(list);
        return 1;
    }


    public static int updateOnlineRoles(String roles) {
        List<RoomConfigModel> list = JsonConfig.ME.getRoomFromRedis();
        if (list == null || list.isEmpty()){
            list = RoomConfigService.OBJ.getConfigs();
        }
        for (RoomConfigModel config : list){
            config.setOnlinerRoles(roles);
        }

        //写入json文本
        JsonConfig.ME.writeRoom(list);
        JsonConfig.ME.synRoomToRedis(list);
        return 1;
    }

    public static int updateKillPool(KillPoolConfig pool) {
        List<KillPoolConfig> list = JsonConfig.ME.getKillFromRedis();
        if (list == null || list.isEmpty()){
            list = RobotStrategyService.OBJ.get_list();
        }

        //索引
        int idx = 0;

        Iterator<KillPoolConfig> iter = list.iterator();
        while (iter.hasNext()) {
            KillPoolConfig config = iter.next();

            //移除旧的
            if (config.getId() == pool.getId()){
                idx = list.indexOf(config);
                iter.remove();
            }
        }

        //替换掉
        list.add(idx, pool);

        //写入json文本
        JsonConfig.ME.writeKillPoll(list);
        JsonConfig.ME.synKillToRedis(list);
        return 1;
    }

    public static int insertKillPool(KillPoolConfig pool) {
        List<KillPoolConfig> list = JsonConfig.ME.getKillFromRedis();
        if (list == null || list.isEmpty()){
            list = RobotStrategyService.OBJ.get_list();
        }

        int maxId = list.stream().mapToInt(KillPoolConfig :: getId).max().getAsInt();
        int nextId = maxId + 1;

        //设置主键
        pool.setId(nextId);
        list.add(pool);

        //写入json
        JsonConfig.ME.writeKillPoll(list);
        JsonConfig.ME.synKillToRedis(list);
        return 1;
    }

}
