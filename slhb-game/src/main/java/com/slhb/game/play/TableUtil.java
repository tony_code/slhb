package com.slhb.game.play;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.game.model.PlayerInfo;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class TableUtil {



    public static String toStringNormal(AbstractTable table) {
        return "Table{" +
                "gameId=" + table.getPlayType() +
                ", roomId=" + table.getRoomId() +
                ", tableId='" + table.getTableId() + '\'' +
                ", tableStateEnum=" + table.getTableStateEnum() +
                ", allPlayersCnt=" + table.getAllPlayers().size() +
                '}';
    }

    public static String toStringAllPlayers(AbstractTable table) {
        StringBuilder sb = new StringBuilder();

        for (PlayerInfo player : table.getAllPlayers().values()) {
            sb.append("userId=" + player.getPlayerId()
                    + ", seatNum=" + player.getSeatNum()
                    + ", state=" + player.getState()
                    + ", playScore=" + player.getPlayScoreStore()
                    + ", isOffline = " + player.isOffLine()
                    + System.getProperty("line.separator"));
        }

        return sb.toString();
    }

    public static String toStringInGamePlayers(AbstractTable table) {
        StringBuilder sb = new StringBuilder();
        return sb.toString();
    }

    public static String inGamePlayersBySeatNumToString(AbstractTable table) {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder.toString();
    }



    /**
     * 获取桌子上的所有玩家状态
     */
    public static List<JoloGame.JoloGame_TablePlay_PlayerInfo> getPlayers(AbstractTable table, PlayerInfo p) {
        List<JoloGame.JoloGame_TablePlay_PlayerInfo> list = new ArrayList<>();

        return list;
    }

    public static List<JoloGame.JoloGame_TablePlay_PlayerInfo> getPlayersInTable(AbstractTable table, PlayerInfo p) {
        List<JoloGame.JoloGame_TablePlay_PlayerInfo> list = new ArrayList<>();

        return list;
    }

    public static List<PlayerInfo> getSameRobDealerPlayers(AbstractTable table) {
        List<PlayerInfo> list = new ArrayList<>(table.getInGamePlayers().size());

        return list;
    }

    /**
     * 计算下注倍数
     * @param table
     */
    public static void calculateBetMultiple(AbstractTable table) {

    }
}
