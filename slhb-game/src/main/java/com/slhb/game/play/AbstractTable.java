package com.slhb.game.play;

import JoloProtobuf.GameSvr.JoloGame;
import com.google.protobuf.MessageLite;
import com.slhb.core.utils.fifo.FIFORunnableQueue;
import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.*;
import com.slhb.base.model.GameRoomTableSeatRelationModel;
import com.slhb.base.model.RoomTableRelationModel;
import com.slhb.game.dao.bean.CommonConfigModel;
import com.slhb.game.dao.bean.KillPoolConfig;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.model.CardConstent;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.model.RedEnv;
import com.slhb.game.model.RedEnvDetail;
import com.slhb.game.network.protocol.ClientReq;
import com.slhb.game.network.protocol.logic.LeaveTableLogic;
import com.slhb.game.network.protocol.protoutil.JoloGame_tablePlay_OtherPlayerInfoBuilder;
import com.slhb.game.room.cache.RoomTableSet;
import com.slhb.game.room.cache.TableSeatSet;
import com.slhb.game.room.cache.TableUserSet;
import com.slhb.game.room.cache.UserTableSet;
import com.slhb.game.room.service.RoomOprService;
import com.slhb.game.service.*;
import com.slhb.game.service.holder.CommonConfigHolder;
import com.slhb.game.service.holder.FunctionIdHolder;
import com.slhb.game.service.holder.RoomConfigHolder;
import com.slhb.game.utils.log.TableLogUtil;
import com.slhb.game.vavle.notice.NoticeRPCUtil;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.relation.Role;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Setter
@Getter
public class AbstractTable implements ITable {
    protected final static Logger log = LoggerFactory.getLogger(AbstractTable.class);

    private final FIFORunnableQueue fifoRunnableQueue = new FIFORunnableQueue<ClientReq>() {
    };
    /**
     * 玩法类型
     */
    private int playType;
    //渠道ID,有玩家入座后标记牌桌所属渠道并锁定渠道,牌局结束后释放
    private String channelId = "";
    private String tableId;
    private String roomId;
    private RoomTableRelationModel roomTableRelation;
    private boolean propertyChange = false;
    /**
     * 每次开局前重新加载
     */
    private CommonConfigModel commonConfig = null;
    private RoomConfigModel roomConfig = null;
    private KillPoolConfig killPool = null;
    /**
     * 当前庄家座位号
     */
    private int currDealerSeatNum;
    //房间所有人 key：userId
    private final Map<String, PlayerInfo> allPlayers = new ConcurrentHashMap<>();
    //游戏中的人 key：userId
    private final Map<String, PlayerInfo> inGamePlayers = new ConcurrentHashMap<>();
    //当前牌局单号
    private String currGameOrderId = "";
    //当前基础下注值
    private long currBaseBetScore;
    //死机玩家的id 用于强制提出
    private int offlineSeatNum;
    private String offlinePlayerId;
    private TableStateEnum tableStateEnum; //牌桌状态

    //牌局开始时间
    private long gameStartTime = 0l;

    //红包队列
    private List<RedEnv> redEnvList = new ArrayList<>();

    //本局红包
    private RedEnv redEnv;

    //庄家ID
    private String dealer;

    //是否把奖池分离出来
    private boolean splitPool = false;
    private String splitPoolChannel = "";

    public AbstractTable(String gameId, String roomId, String tableId) {
        setPlayType(Integer.parseInt(gameId));
        setRoomId(roomId);
        setTableId(tableId);
        roomTableRelation = new RoomTableRelationModel(gameId, roomId, tableId,TableStateEnum.IDEL.getValue());

        setCommonConfig(CommonConfigHolder.getInstance().getCommonConfig());
        setRoomConfig(RoomConfigHolder.getInstance().getRoomConfig(roomId));
        initTableStateAttribute();

        tableStateEnum = TableStateEnum.IDEL;
        setTableStatus();
    }

    @Override
    public synchronized boolean joinTable(PlayerInfo player) {
        if (!allPlayers.containsKey(player.getPlayerId())) {
            UserTableService.getInstance().onPlayerInTable(getPlayType() + "", player, this);
            player.setState(PlayerStateEnum.spectator);
            allPlayers.put(player.getPlayerId(), player);
        }

        log.debug("获取渠道杀量配置 channel={},room={},role={}",channelId,roomId,player.getRoleType().getTypeName());
        return true;
    }

    public synchronized boolean joinRoom(PlayerInfo player){
        String gameId = String.valueOf(this.getPlayType());
        UserTableService.getInstance().onPlayerInTable(gameId, player, this);
        RoomStateService.getInstance().onPlayerJoinTable(this);
        player.setState(PlayerStateEnum.spectator);

        TableUserSet.OBJ.add(getRoomId()+getTableId(), player.getPlayerId(), PlayerStateEnum.spectator.name());

                //设置玩家所在位置
        UserTableSet.OBJ.add(player.getPlayerId(),new GameRoomTableSeatRelationModel(gameId, player.getRoomId(), player.getTableId(), 0, ""));

        log.debug("获取渠道杀量配置 channel={},room={},role={}",channelId,roomId,player.getRoleType().getTypeName());
        allPlayers.put(player.getPlayerId(), player);
        return true;
    }

    //离开桌子
    public void leaveRoom(PlayerInfo player){
        if (!allPlayers.containsKey(player.getPlayerId())){
            return;
        }

        UserTableService.getInstance().onPlayerOutTable(player);
        allPlayers.remove(player.getPlayerId());

        TableUserSet.OBJ.remove(getRoomId()+getTableId(), player.getPlayerId());
        //设置玩家所在位置
        UserTableSet.OBJ.remove(player.getPlayerId());
    }

    @Override
    public boolean sitDown(int seatNum, String userId) {
        boolean flag = false;
        PlayerInfo player = allPlayers.get(userId);
        if (player != null) {
            removeNullSeat(seatNum);
            //玩家坐下重置状态
            player.sitDownAfterInit();
            player.setSeatNum(seatNum);
            allPlayers.forEach((k, v) -> {
                v.setStandUpTime(0L);
                log.debug("重置玩家standUpTime为0, userId->{}, tableId->{}, roomId->{}, gameId->{}, siteDownUserId->{}, playerState->{}",
                        v.getPlayerId(), tableId, roomId, playType, userId, v.getState());
            });
            flag = true;

            TableSeatSet.OBJ.add(player.getRoomId(), player.getTableId(), seatNum, userId);
            //设置座位
            GameRoomTableSeatRelationModel gameRoomTableSeatRelationModel = UserTableSet.OBJ.get(player.getPlayerId());

            log.debug("gameRoomTableSeatRelationModel != null ---> " + (gameRoomTableSeatRelationModel != null));
            if (gameRoomTableSeatRelationModel != null) {
                gameRoomTableSeatRelationModel.setTableId(tableId);
                gameRoomTableSeatRelationModel.setSeat(seatNum);

                UserTableSet.OBJ.add(player.getPlayerId(), gameRoomTableSeatRelationModel);
            }
        }
        return flag;
    }

    /**
     * 如果执行就执行正确
     * 检查没有此玩家也为站起成功
     *
     * @param seatNum
     * @param userId
     * @param standUpType
     * @return
     */
    @Override
    public boolean standUp(Integer seatNum, String userId, String standUpType) {
        String guid = UUID.randomUUID().toString();
        boolean flag = false;
        //当牌局不是空闲或结算状态时，从游戏用户列表中移除用户，否则先不移除用户，等游戏结算进行结算时再移除
        PlayerInfo player = allPlayers.get(seatNum);
        log.debug("standUp() seatNum:{},userId:{},standUpType:{}", seatNum, userId, standUpType);
        if (player == null) {
            log.debug("inGamePlayersBySeatNum:" + TableUtil.inGamePlayersBySeatNumToString(this));
        }
        StringBuilder sb = new StringBuilder();
        log.debug("tableInfo->" + TableUtil.toStringNormal(this));
        for (PlayerInfo playerInfo : allPlayers.values()) {
            sb.append("playerInfo:" + playerInfo.toString() + "," + System.getProperty("line.separator"));
        }
        log.debug(sb.toString());

        if (null != player) {
            //判断当前座位上的人，是否是需要站起的玩家。
            if (!player.getPlayerId().equals(userId)) {
                return flag;
            }
        }


        if (player != null) {
            inGamePlayers.remove(seatNum);
            addNullSeat(seatNum);
            PlayerInfo p = allPlayers.get(userId);
            p.setState(PlayerStateEnum.spectator);
            flag = true;

            TableSeatSet.OBJ.remove(player.getRoomId(), player.getTableId(), player.getPlayerId());

            log.debug("发给room的站起协议 table->" + TableUtil.toStringNormal(this));
            RoomOprService.OBJ.standupHandler(String.valueOf(getPlayType()),player.getRoomId(),player.getTableId());

            //设置座位
            UserTableSet.OBJ.remove(userId);
        }

        String res = TableUserSet.OBJ.get(getRoomId()+getTableId(), userId);

        if (StringUtils.isNotEmpty(res)) {
            TableUserSet.OBJ.add(getRoomId()+getTableId(),userId, PlayerStateEnum.spectator.name());
        }

        if (player != null && player.isOffLine() && flag) {
            LeaveTableLogic.getInstance().logic(player, this, null);
        }
        return flag;
    }

    /**
     * 离桌
     *
     * @param userId
     */
    public void returnLobby(String userId) {
        returnLobby(userId, true);
    }

    @Override
    public void returnLobby(String userId, boolean force) {
        log.debug("删除用户列表中的用户，userId->" + userId + "，删除前列表人数->" + allPlayers.size());
        PlayerInfo playerInfo = allPlayers.remove(userId);
        if (playerInfo != null && playerInfo.getSeatNum() > 0) {
            if (null != playerInfo) {
                if (playerInfo.getPlayerId().equals(userId)) {

                    inGamePlayers.remove(playerInfo.getSeatNum());
                    addNullSeat(playerInfo.getSeatNum());

                    //上桌内的玩家信息删除
                    TableSeatSet.OBJ.remove(roomId, tableId, userId);
                }
            }
        }

        TableUserSet.OBJ.remove( roomId + tableId, userId);
        //设置座位
        UserTableSet.OBJ.remove(userId);

        if (force) {//强制广播
            JoloGame.JoloGame_TablePlay_OtherPlayerInfo.Builder otherPlayerInfo =
                    JoloGame_tablePlay_OtherPlayerInfoBuilder.getOtherPlayerInfo(playerInfo);
            User user = PlayerService.getInstance().getUser(userId);
            //广播离桌
            boardcastMessageSingle(userId, JoloGame.JoloGame_Notice2Client_leaveReq.newBuilder()
                            .setRoomId(getRoomId())
                            .setTableId(getTableId())
                            .setUserId(userId)
                            .setCurrStoreScore(user == null ? 0 : user.getMoney())
                            .setOtherPlayerInfo(otherPlayerInfo)
                            .setWinLoseScore(playerInfo == null ? 0 : playerInfo.getTotalWinLoseScore())
                            .build(),
                    FunctionIdHolder.Game_Notice2Client_leavel);
        }
    }

    @Override
    public PlayerInfo getPlayer(String playerId) {
        return allPlayers.get(playerId);
    }

    @Override
    public int giveCardCounts() {
        return CardConstent.HAND_CARDS;
    }

    //region 空座位管理
    public void addNullSeat(int seatNum) {
    }

    private boolean removeNullSeat(int seatNum) {
        return false;
    }

    public synchronized int getNulSeatNum() {
        int seatNum = 0;
        return seatNum;
    }

    @Override
    public void setGameId(String gameId) {
        setPlayType(Integer.parseInt(gameId));
    }

    @Override
    public List<Integer> getChangeCards() {
        return new ArrayList<>();
    }

    @Override
    public PlayerInfo getWinnerByCompareCards(PlayerInfo player1, PlayerInfo player2) {

        return null;
    }

    public double playerDataSettlement(PlayerInfo player) {
        return this.playerDataSettlement(player, true);
    }
    /**
     * 结算玩家数据
     *
     * @param player
     * @param isSendNotice 是否在玩家站起后，发送Notice广播 add by gx 20181009
     *                     原因：牌局结算后，解散牌桌，站起每个玩家。
     *                     为了方面客户端不处理站起Notice，因此控制可不发送站起Notice消息
     * @return
     */
    public double playerDataSettlement(PlayerInfo player,boolean isSendNotice) {

        return 0;
    }

    /**
     * 推送消息(消息体相同时)
     */
    public void boardcastMessage(String tableId, MessageLite messageLite, int functionId) {
        List<String> list = new ArrayList<>();
        for (String playerId : allPlayers.keySet()) {
            PlayerInfo player = allPlayers.get(playerId);
            if (player != null) {
                if (!player.isOffLine()) {
                    list.add(player.getPlayerId());
                }
            }
        }

        log.debug("message functionId:" + functionId + "push player:" + list + ",getPlayType:" + getPlayType());
        NoticeRPCUtil.senMuliMsg(getPlayType(), tableId, list, functionId, messageLite);
    }

    /**
     * 推送消息(消息体相同时)
     */
    public void boardcastMessageSingle(String playerId, MessageLite messageLite, int functionId) {
        NoticeRPCUtil.sendSingMsg(getPlayType(), playerId, functionId, messageLite);
    }

    @Getter
    @Setter
    private long oldTimemillis = 0;

    private Map<String, Long> tmpTableUsers = new ConcurrentHashMap<>();


    public void initTableStateAttribute() {
        //初始化牌桌配置
        try {
            this.redEnv = null;
            this.dealer = null;
            this.inGamePlayers.clear(); //座位信息清除
        } catch (Exception e) {
        }
    }

    public void pressCard() {
        if (redEnvList == null || redEnvList.isEmpty()){
            for (PlayerInfo player : allPlayers.values()){
                if (player.getRoleType() == RoleType.ROBOT){
                    RedEnv redPackage = RedEnvService.OBJ.createRobotRedenv(player.getPlayerId(), roomId);
                    this.redEnv = redPackage;
                    this.dealer = redEnv.getOfferUserId();
                }
            }
        }else {
            //取红包列表第一个
            this.redEnv = redEnvList.get(0);
            this.redEnvList.remove(0);
            this.dealer = redEnv.getOfferUserId();
        }

        if (redEnv == null){
            log.error("本局发红包失败 GameOrderId = {}", currGameOrderId);
        }
    }

    //抢红包
    public synchronized RedEnvDetail robPackage(String userId){
        if (redEnv == null) return null;

        if (redEnv.getRemains() <= 0) return null;

        List<RedEnvDetail> details = redEnv.getEnvs();
        if (details == null){
            log.error("红包详情为空");
            return null;
        }

        //是否领取过
        long count = details.stream().filter(e->userId.equals(e.getBelong())).count();
        if (count >= 1){
            log.error("玩家{}已经领取本红包", userId);
            return null;
        }

        for (RedEnvDetail detail : details){
            if (detail == null) continue;

            if (!detail.isAvailable()) continue;

            detail.setBelong(userId);
            return detail;
        }
        return null;
    }

    @Override
    public String toString() {
        return "Table{" +
                "roomId=" + roomId +
                ", tableId='" + tableId + '\'' +
                ", tableStateEnum=" + tableStateEnum +
                ", allPlayers=" + allPlayers +
                '}';
    }

    public void setTableStatus(){
        RoomTableRelationModel ret = RoomTableSet.OBJ.get(getRoomId(),getTableId());
        if (ret == null){
            log.error("setTableStatus ERROR, ret from redis is null. {}, {}, {}", getPlayType() , getRoomId(),getTableId());
            return;
        }
        ret.setTableStatus(getTableStateEnum().getValue());
        RoomTableSet.OBJ.add(getRoomId(),getTableId(),ret);
    }

    public void clearAllSeats(){
        inGamePlayers.clear();
        tmpTableUsers.clear();
    }

    public String getPlayTypeStr(){
        return String.valueOf(this.playType);
    }

    public boolean exsitRealPlayer(){
        for (PlayerInfo player : allPlayers.values()){
            if (player == null) {
                continue;
            }

            if (player.getRoleType() == RoleType.GUEST){
                return true;
            }
        }
        return false;
    }


    public void loadKillPoolConfig(){
        if (StringUtils.isEmpty(this.channelId)){
            log.error("游戏开局异常 gameOrderId = {}, channel = null",currGameOrderId);

            for (PlayerInfo player : allPlayers.values()){
                //为游戏重载渠道
                if (player.getRoleType().getIndex() == RoleType.GUEST.getIndex()){
                    this.channelId = player.getUser().getChannel_id();
                }
            }
        }

        KillPoolConfig poolConfig = RobotStrategyService.OBJ.getPool(this.channelId, this.roomId);
        if (poolConfig == null) {
            log.error("获取渠道杀量配置异常 gameOrderId = {} channel={},room={},pool=null", this.currGameOrderId, this.channelId, this.roomId);
        } else {
            this.killPool = poolConfig;
            log.debug("获取渠道杀量配置 gameOrderId = {} channel={},room={},servicefee={}",
                    this.currGameOrderId, this.channelId, this.roomId, killPool.getServiceRate());
        }
    }
}
