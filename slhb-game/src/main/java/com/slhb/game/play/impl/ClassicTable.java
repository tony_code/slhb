package com.slhb.game.play.impl;


import com.slhb.game.model.CardConstent;
import com.slhb.game.play.AbstractTable;

public class ClassicTable extends AbstractTable {


    public ClassicTable(String gameId, String roomId, String tableId) {
        super(gameId, roomId, tableId);
    }

    @Override
    public int giveCardCounts() {
        return CardConstent.CLASSIC_HAND_CARDS;
    }


}
