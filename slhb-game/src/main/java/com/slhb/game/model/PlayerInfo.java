package com.slhb.game.model;

import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.PlayerStateEnum;
import com.slhb.base.enums.RoleType;
import com.slhb.base.model.RoomTableRelationModel;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.service.MoneyService;
import com.slhb.game.service.PlayerService;
import com.slhb.game.utils.NumUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * 玩家信息
 * 描述：存储玩家在玩牌过程中的信息，如果一个玩家同时存在于两个桌子，那么将有两个相同playerId 的PlayerInfo，用tableId 进行区分存在于哪个桌子。
 */
@ToString
@Getter
@Setter
public class PlayerInfo {

    private final static Logger logger = LoggerFactory.getLogger(PlayerInfo.class);

    private User user;
    private String playerId;//玩家id 和userI都一样
    private String roomId; //房间ID
    private String tableId;//用户桌子Id
    private String nickName;//显示的昵称
    private String icon; //玩家头像URL
    private boolean isOffLine = false;//是否已经掉线
    private int seatNum; //玩家座位号
    private double playScoreStore = 0;//玩家牌桌上积分的携带量

    private int isDealer; //是否庄家1是0否

    private PlayerStateEnum state;//玩家状态 0/旁观 1/抢红包 2/结算

    private long totalAlreadyBetScore4Hand; //当前牌局累积已下注金额，方便后续结算时判断用户输赢
    private long winLoseScore4Hand; //当前牌局输赢积分数量


    //region 此部分用于统计玩家玩牌时长，输赢积分
    private long joinTableTime; //加入牌桌时间
    private long leaveTableTime; //离开牌桌时间
    private long sitDownTime; //入座时间
    /**
     * 站起时间
     */
    private long standUpTime = 0L;
    private int totalPlayMinutes; //合计的玩牌时间（分钟数）
    /**
     * 入桌时带入积分及在桌内购入积分总合
     */
    private double totalTakeInScore = 0;
    private double totalWinLoseScore; //合计的输赢积分数

    /**
     * 旁观次数累计(连续超过三局自动离桌)
     */
    private int spectatorCount;

    private RoleType roleType;

    //本局抢到的红包数
    private RedEnvDetail curRob;

    //结算数据
    private double serviceFee;
    //玩家获利
    private double profit;
    //玩家赔付
    private double payback;

    public PlayerInfo(RoomTableRelationModel relationModel, String playerId, String nickName, String icon, RoleType roleType, User user) {
        this.roomId = relationModel.getRoomId();
        this.tableId = relationModel.getTableId();
        this.playerId = playerId;
        this.nickName = nickName;
        this.icon = icon;
        setRoleType(roleType);
        this.user = user;
    }

    public PlayerInfo(String roomId, String tableId, String playerId, String nickName, String icon, RoleType roleType,User user) {
        this.roomId = roomId;
        this.tableId = tableId;
        this.playerId = playerId;
        this.nickName = nickName;
        this.icon = icon;
        setRoleType(roleType);
        this.user = user;
    }

    public PlayerInfo(RoomTableRelationModel relationModel, User user) {
        this.roomId = relationModel.getRoomId();
        this.tableId = relationModel.getTableId();
        this.playerId = user.getId();
        this.nickName = user.getNick_name();
        this.icon = user.getIco_url();
        setRoleType(RoleType.getRoleType(user.getChannel_id()));
        this.user = user;
    }

    public PlayerInfo( AbstractTable table, User user) {
        this.roomId = table.getRoomId();
        this.tableId = table.getTableId();
        this.playerId = user.getId();
        this.nickName = user.getNick_name();
        this.icon = user.getIco_url();
        setRoleType(RoleType.getRoleType(user.getChannel_id()));
        this.user = user;
    }

    public void removePlayerInfo() {
        this.isDealer = 0;
        this.state = PlayerStateEnum.spectator;
        this.totalAlreadyBetScore4Hand = 0;
        this.winLoseScore4Hand = 0;
        this.spectatorCount = 0;
    }

    /**
     * 重置游戏中信息
     */
    public void resetGameingInfo() {
        //如果玩家不是自动弃牌则重置数据
        this.isDealer = 0;
        this.state = PlayerStateEnum.spectator;
        this.totalAlreadyBetScore4Hand = 0;
        this.winLoseScore4Hand = 0;
        this.spectatorCount = 0;
        this.curRob = null;
        this.serviceFee = 0;
        this.payback = 0;
        this.profit = 0;
    }

    public void sitDownAfterInit() {
        setTotalWinLoseScore(0);
        setState(PlayerStateEnum.spectator); //修改玩家状态值
    }


    public String toSitDownString() {
        return "Player{" +
                "playerId='" + playerId + '\'' +
                ", roomId='" + roomId + '\'' +
                ", tableId='" + tableId + '\'' +
                ", seatNum=" + seatNum +
                ", playScoreStore=" + playScoreStore +
                ", state=" + state +
                ", sitDownTime=" + sitDownTime +
                ", standUpTime=" + standUpTime +
                ", totalPlayMinutes=" + totalPlayMinutes +
                ", totalWinLoseScore=" + totalWinLoseScore +
                '}';
    }

    public void addServiceFee(double serviceFee){
        this.serviceFee += serviceFee;
    }

    public void addProfit(double profit){
        this.profit += profit;
    }

    public void addPayback(double payback){
        this.payback += payback;
    }

    /**
     * 增加筹码
     *
     * @param score     操作筹码
     * @return
     */
    public boolean addPlayScoreStore(double score) {
        //筹码是负数
        if (score <= 0) {
            return false;
        }

        //增加积分
        this.playScoreStore += score;
        if (this.playScoreStore != 0) {
            user.setMoney(this.playScoreStore);
        } else {
            user.setMoney(user.getMoney() + score);
        }

        PlayerService.getInstance().saveUser(user);

        logger.info("货币跟踪,玩家 = {}, 增加筹码 add = {}, 当前剩余 = {}", this.playerId, score, this.playScoreStore);
        return true;
    }

    /**
     * 扣除筹码
     *
     * @param score     操作筹码
     * @return
     */
    public boolean minusPlayScoreStore(double score) {
        //筹码是负数
        if (score <= 0 || this.playScoreStore < score) {
            return false;
        }

        //扣除积分
        this.playScoreStore -= score;

        user.setMoney(this.playScoreStore);
        PlayerService.getInstance().saveUser(user);

        logger.info("货币跟踪,玩家 = {}, 扣除筹码 add = {}, 当前剩余 = {}", this.playerId, score, this.playScoreStore);
        return true;
    }

    public String playerToString() {
        return "roomId->" + roomId + ", tableId->" + tableId + ", playerId->" + playerId + ", seatNum->" + seatNum + ", playScoreStore->" + playScoreStore;
    }

    public double getPlayScoreStore() {
        return NumUtils.double2Decimal(playScoreStore);
    }

}
