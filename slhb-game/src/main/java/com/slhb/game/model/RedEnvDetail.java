package com.slhb.game.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * 红包详情
 */
@Getter@Setter@ToString
public class RedEnvDetail {

    //序列
    private int index;

    //领取红包的钱
    private double money;

    //是否中雷 true中了
    private boolean isThrunder;

    //是否可用
    private boolean available = true;

    //领取的人
    private String belong;

    //是不是手气最好的
    private boolean isMax;

    public RedEnvDetail(int index, double money, boolean thrunder, boolean isMax) {
        this.index = index;
        this.money = money;
        this.isThrunder = thrunder;
        this.isMax = isMax;
    }

    public void setBelong(String belong) {
        this.belong = belong;
        this.available = false;
    }

}

