package com.slhb.game.model;

public class CardConstent
{
    /**默认手牌数*/
    public static final int HAND_CARDS = 5;
    /**Classic 手牌数*/
    public static final int CLASSIC_HAND_CARDS = 5;

    //牌型倍数
    public static final int mul_1 = 1;
    public static final int mul_2 = 2;
    public static final int mul_3 = 3;
    public static final int mul_4 = 4;

    //特殊牌型
    //0:无牛1至9:牛一到牛九10牛牛11四花12五花13四炸14五小牛
    public static final int niu_0 = 0;
    public static final int niu_1 = 1;
    public static final int niu_2 = 2;
    public static final int niu_3 = 3;
    public static final int niu_4 = 4;
    public static final int niu_5 = 5;
    public static final int niu_6 = 6;
    public static final int niu_7 = 7;
    public static final int niu_8 = 8;
    public static final int niu_9 = 9;
    public static final int niu_niu = 10;
    public static final int niu_4q = 11;
    public static final int niu_5q = 12;
    public static final int niu_boomb = 13;
    public static final int niu_5s = 14;

}
