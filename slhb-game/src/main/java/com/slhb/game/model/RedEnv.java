package com.slhb.game.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.*;

/**
 * 红包数量
 */
@Getter@Setter@ToString
public class RedEnv {

    //发红包的人
    private String offerUserId;

    //红包数
    private int envNum;

    //雷号
    private int thunderEnd;

    //包含的钱数
    private int money;

    //红包数
    private List<RedEnvDetail> envs = new ArrayList<>();

    //剩余数量
    private int remains;

    public void setEnvs(List<RedEnvDetail> envs) {
        this.envs = envs;
        this.remains = envs.size();
    }
}

