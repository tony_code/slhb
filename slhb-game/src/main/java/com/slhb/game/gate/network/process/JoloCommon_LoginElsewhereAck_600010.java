package com.slhb.game.gate.network.process;


import com.slhb.game.gate.network.protocol.Ack;

public class JoloCommon_LoginElsewhereAck_600010 extends Ack {

    /**
     * @param functionId
     */
    public JoloCommon_LoginElsewhereAck_600010(int functionId) {
        super(functionId);
    }
}
