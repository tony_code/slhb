package com.slhb.game.gate.network.process;


import com.slhb.game.gate.network.protocol.Ack;

public class JoloCommon_LoginAck_600001 extends Ack {
    /**
     * @param functionId
     */
    public JoloCommon_LoginAck_600001(int functionId) {
        super(functionId);
    }
}
