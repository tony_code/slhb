package com.slhb.game.gate.network.handler;

import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.gate.pool.net.ChannelManageCenter;
import com.slhb.game.gate.service.UserService;
import com.slhb.game.play.AbstractTable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Slf4j
public class GateFunctionHandler extends SimpleChannelInboundHandler<Req> {
    private final static ExecutorService executorService = Executors.newFixedThreadPool(8);

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ChannelManageCenter.getInstance().addChannel(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        log.info("消息通道 channelInactive");
        ChannelManageCenter.getInstance().removeTempSession(ctx);
        UserService.getInstance().onUserBreak(ctx,false);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Req msg) {
        /*
         * 避免msg对象run方法的执行时间长，而导致后面新来的消息被阻塞排队，因此将每个msg当成独立线程去执行，确保接收消息不出现延迟。
         */
        if (msg != null) {
            AbstractTable table = msg.getTable();
            if (table != null) {
                table.getFifoRunnableQueue().execute(msg);
            }else {
                executorService.submit(msg);
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.error("消息通道 exceptionCaught");
        ChannelManageCenter.getInstance().removeTempSession(ctx);
        UserService.getInstance().onUserBreak(ctx,true);
    }
}
