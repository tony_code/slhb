package com.slhb.game.gate.network.process;

import JoloProtobuf.AuthSvr.JoloAuth;
import com.slhb.base.dao.bean.User;
import com.slhb.base.platform.HallAPIService;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.service.PlayerService;
import io.netty.buffer.ByteBuf;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * 请求更换玩家头像
 *
 */
public class JoloAuth_ChangeIcoReq_600005 extends Req {

    private final static Logger logger = LoggerFactory.getLogger(JoloAuth_ChangeIcoReq_600005.class);

    private JoloAuth.JoloAuth_ChangeIcoReq req;

    public JoloAuth_ChangeIcoReq_600005(int functionId ) {
        super(functionId);
    }

    @Override
    public void readPayLoadImpl( ByteBuf buf ) throws Exception {
        byte[] blob = new byte[buf.readableBytes()];
        buf.readBytes(blob);
        req = JoloAuth.JoloAuth_ChangeIcoReq.parseFrom(blob);
    }

    @Override
    public void processImpl() throws Exception {
        JoloAuth.JoloAuth_ChangeIcoAck.Builder ack = JoloAuth.JoloAuth_ChangeIcoAck.newBuilder();
        String userId = this.userId;
        String ico = req.getIcoUrl();

        //参数是否正常
        if (StringUtils.isEmpty(userId)
                || StringUtils.isEmpty(ico)){
            logger.error("params is empty ,uid = "+userId+",ico ="+ico);
            ack.setResult(-1).setResultMsg("参数不正常");
            sendResponse(functionId | 0x08000000, ack.build().toByteArray());
            return;
        }

        //查询玩家
        User user = PlayerService.getInstance().getUser(userId);
        if (user == null){
            logger.error("user not exists ,uid = "+userId);
            ack.setResult(-2).setResultMsg("找不到该玩家");
            sendResponse(functionId | 0x08000000, ack.build().toByteArray());
            return;
        }

        //保存数据
        user.setUser_defined_head(ico);
        user.setIco_url(ico);

        HallAPIService.OBJ.updateIcon(user.getAndroid_id(),ico);


        PlayerService.getInstance().saveUser(user);
        sendResponse(functionId | 0x08000000, ack.setResult(1).build().toByteArray());
    }
}
