package com.slhb.game.gate.network.process;

import com.slhb.game.gate.network.protocol.Ack;

public class PongAck_60000 extends Ack {
    /**
     * @param functionId
     */
    public PongAck_60000(int functionId) {
        super(functionId);
    }
}
