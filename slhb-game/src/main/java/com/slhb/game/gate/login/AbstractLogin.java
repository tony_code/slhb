package com.slhb.game.gate.login;

import JoloProtobuf.AuthSvr.JoloAuth;
import com.slhb.core.network.ChannelHandler;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.gate.pool.net.ChannelManageCenter;
import com.slhb.game.gate.service.GateChannelService;
import com.slhb.game.gate.service.UserService;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


/**
 * 登录处理器
 * @author
 * @since 2018/11/26 19:35
 */
@Getter@Setter@Slf4j
public abstract class AbstractLogin {

    public String ChannelId;

    /**
     * 构造
     * @param channelId
     */
    public AbstractLogin(String channelId) {
        ChannelId = channelId;
        //注册
        LoginService.OBJ.reg(this);
    }

    /**
     * 处理登录
     * @param req
     * @param ctx
     * @param reqHeader
     */
    public abstract void process(JoloAuth.JoloCommon_LoginReq req, ChannelHandlerContext ctx, Req.ReqHeader reqHeader) throws Exception;


    protected void bindUser(ChannelHandlerContext ctx, Req.ReqHeader reqHeader, String userId, String token){
        GateChannelService.OBJ.handlerRepeatLoginChannel(userId);

        //绑定netty连接
        long sessionId = ChannelHandler.getSesseionId(ctx);
        ChannelManageCenter.getInstance().bind(sessionId,userId);

        //绑定netty连接
        UserService.getInstance().onUserLogin(userId, ctx);

    }

}
