package com.slhb.game.gate.network.process;


import com.slhb.game.gate.network.protocol.Ack;

public class JoloCommon_InTableAck_600011 extends Ack {
    /**
     * @param functionId
     */
    public JoloCommon_InTableAck_600011(int functionId) {
        super(functionId);
    }
}
