package com.slhb.game.gate.network.process;


import com.slhb.game.gate.network.protocol.Ack;

public class JoloCommon_NoticeCloseAck_600026 extends Ack {
    /**
     * @param functionId
     */
    public JoloCommon_NoticeCloseAck_600026(int functionId) {
        super(functionId);
    }
}
