package com.slhb.game.gate.network.process;


import com.slhb.game.gate.network.protocol.Ack;

public class JoloAuth_ChangeIcoAck_600005 extends Ack {

    /**
     * @param functionId
     */
    public JoloAuth_ChangeIcoAck_600005(int functionId ) {
        super(functionId);
    }

}
