package com.slhb.game.gate.login;

import JoloProtobuf.AuthSvr.JoloAuth;
import com.slhb.core.jedis.StoredObjManager;
import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.RedisConst;
import com.slhb.base.model.GameRoomTableSeatRelationModel;
import com.slhb.base.platform.HallAPIService;
import com.slhb.base.platform.bean.PlatUserBean;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.room.cache.UserTableSet;
import com.slhb.game.service.NoticePlatformSerivce;
import com.slhb.game.service.PlayerService;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Date;

/**
 * 通用登录
 * @author
 * @since 2018/11/26 19:42
 */
@Slf4j
public class Login_Guest extends AbstractLogin{

    private static final String _script = "guest";

    /**
     * 构造
     */
    public Login_Guest() {
        super(_script);
    }

    @Override
    public void process(JoloAuth.JoloCommon_LoginReq req, ChannelHandlerContext ctx, Req.ReqHeader reqHeader)  throws Exception{
        log.info("guest用戶登录操作");
        JoloAuth.JoloCommon_LoginAck.Builder builder = JoloAuth.JoloCommon_LoginAck.newBuilder();

        //大厅传入UserId
        String userId = req.getUserId();
        //大厅传入Identity
        String token = req.getToken();

        PlatUserBean userBean = HallAPIService.OBJ.getByIdentity(userId);

        if (userBean == null){
            log.error("远程用户验证失败 openId = {}",userId);
            builder.setUserId("")
                    .setMoney(0)
                    .setIcoUrl("")
                    .setNickName("")
                    .setDefaultIco("")
                    .setVerify("")
                    .setChannelId("")
                    .addAllServerinfo(new ArrayList<>())
                    .setResult(0).build();
            GateFunctionFactory.getInstance().getResponse(reqHeader.functionId  | 0x08000000, builder.build().toByteArray()).send(ctx, reqHeader);
            log.info("builder ack 失败->: {}",builder.toString());
            return;
        }

        User user = new User();
       // user.setId(String.valueOf(IdWorker.getInstance().nextId()));
        user.setId(userBean.getId());
        user.setNick_name(userBean.getNickname());
        user.setMoney(userBean.getGold() );
        user.setClient_version(req.getClientVersion());
        user.setPackage_name(req.getPackName());
        user.setLast_login(new Date());
        user.setDown_platform(req.getDownPlatform());
        user.setChannel_id(userBean.getChannel_id());
        user.setSub_channel_id(userBean.getSub_channel_id());
        user.setUser_defined_head(userBean.getIcon());
        user.setIco_url(userBean.getIcon());
        user.setPlatform(userBean.getPlatform());
        user.setAndroid_id(userBean.getId());

        //玩家线程绑定
        bindUser(ctx, reqHeader, user.getId(), token);

        log.info("login userInfo->" + user.toString());
        PlayerService.getInstance().saveUser(user);

        builder.setUserId(user.getId())
                .setMoney(user.getMoney())
                .setIcoUrl(user.getIco_url())
                .setDefaultIco(user.getUser_defined_head() == null ? "" : user.getUser_defined_head())
                .setNickName(user.getNick_name())
                .setVerify(req.getVerify())
                .setChannelId(user.getChannel_id())
                .addAllServerinfo(new ArrayList<>())
                .setResult(1);

        GateFunctionFactory.getInstance().getResponse(reqHeader.functionId  | 0x08000000, builder.build().toByteArray()).send(ctx, reqHeader);
        log.info("guest用戶登录操作,完成 user={},req={}", user.getId(), builder.toString());

        reconnect(user.getId(), ctx, reqHeader);
    }

    private void reconnect(String userId, ChannelHandlerContext ctx, Req.ReqHeader reqHeader){
        //首先查看自己有没有在游戏内
        GameRoomTableSeatRelationModel gameRoomTableSeatRelationModel = UserTableSet.OBJ.get(userId);

        JoloAuth.JoloCommon_InTableAck.Builder inTableAck = JoloAuth.JoloCommon_InTableAck.newBuilder();
        if (gameRoomTableSeatRelationModel != null) {
            String gameId = gameRoomTableSeatRelationModel.getGameId();
            String roomId = gameRoomTableSeatRelationModel.getRoomId();
            String tableId = gameRoomTableSeatRelationModel.getTableId();
            int seatNum = gameRoomTableSeatRelationModel.getSeat();

            log.debug("玩家user={}在游戏内,可以触发重连,game={},room={},table={}", userId, gameId, roomId, tableId);
            inTableAck.setResult(1);
            inTableAck.setUserId(userId);
            inTableAck.setGameId(gameId);
            inTableAck.setRoomId(roomId);
            inTableAck.setTableId(tableId);
            inTableAck.setSeatNum(seatNum);
        } else {
            inTableAck.setResult(0);
        }

        GateFunctionFactory.getInstance().getResponse(600011 | 0x08000000, inTableAck.build().toByteArray()).send(ctx, reqHeader);
    }
}
