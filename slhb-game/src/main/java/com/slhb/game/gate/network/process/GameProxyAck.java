package com.slhb.game.gate.network.process;

import com.slhb.game.gate.network.protocol.Ack;

/**
 */
public class GameProxyAck extends Ack {
    /**
     * @param functionId
     */
    public GameProxyAck(int functionId) {
        super(functionId);
    }
}
