package com.slhb.game.service;

import com.slhb.base.dao.bean.User;
import com.slhb.game.model.PlayerInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
@Slf4j
public class PlayerService {
    @Getter@Setter
    private static final Map<String, String> PLAYER_MAP = new ConcurrentHashMap();

    private static class SingletonHolder {
        protected static final PlayerService instance = new PlayerService();
    }

    public static final PlayerService getInstance() {
        return PlayerService.SingletonHolder.instance;
    }


    private Map<String ,User> _userCacheMap = new ConcurrentHashMap<>();

    /**
     * 玩家登陆
     */
    public void onPlayerLogin(String playerId) {
        if (PLAYER_MAP.put(playerId, playerId) != null) {
            log.debug("玩家 " + playerId + " 重连登陆");
        }
    }

    /**
     * 玩家登出
     */
    public void onPlayerLoutOut(String playerId) {
        PLAYER_MAP.remove(playerId);
        this.delUser(playerId);
    }

    /**
     * 获取玩家信息
     * @param userId
     * @return
     */
    public User getUser(String userId){
        return _userCacheMap.get(userId);
    }

    /**
     * 玩家信息暂存
     * @param user
     */
    public void saveUser(User user){
        _userCacheMap.put(user.getId(), user);
    }

    /**
     * 玩家信息移除
     * @param user
     */
    public void delUser(User user){
        if (user == null) return;

        if (_userCacheMap.containsKey(user.getId())){
            _userCacheMap.remove(user.getId());
        }
    }

    /**
     * 玩家信息移除
     * @param userId
     */
    public void delUser(String userId){
        if (StringUtils.isEmpty(userId)) return;

        if (_userCacheMap.containsKey(userId)){
            _userCacheMap.remove(userId);
        }
    }
}
