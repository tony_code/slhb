package com.slhb.game.service;

import com.slhb.core.jedis.StoredCommManager;
import com.slhb.game.config.Config;
import lombok.extern.slf4j.Slf4j;


/**
 * 游戏维护
 * @author
 * @since 2019/2/20 13:25
 */
@Slf4j
public class GameMaintentService {

    public static final GameMaintentService OBJ = new GameMaintentService();

    //redis 里的维护标识
    private static final String maintent_flag = "Game_Maintent_Flag_"+ Config.GAME_ID;


    /**
     * 开启维护墙
     */
    public void turnOn(){
        log.info("维护开启 add key = {}", maintent_flag);
        StoredCommManager.set(maintent_flag,"1");
    }

    /**
     * 关闭维护墙
     */
    public void turnOff(){
        log.info("维护结束 del key = {}", maintent_flag);
        StoredCommManager.del(maintent_flag);
    }

    /**
     * 维护状态
     * true 维护中  false 正常
     */
    public boolean isDefense(){
        return StoredCommManager.exists(maintent_flag);
    }

    public void shift(){
        if (isDefense()){
            turnOff();
        }else {
            turnOn();
        }
    }

}
