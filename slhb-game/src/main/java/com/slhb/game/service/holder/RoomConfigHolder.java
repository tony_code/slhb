package com.slhb.game.service.holder;

import com.slhb.game.config.JsonConfig;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.room.service.RoomConfigService;
import lombok.extern.slf4j.Slf4j;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class RoomConfigHolder {
    private static Map<String, RoomConfigModel> ROOM_CONFIG = new HashMap<>(); //所有房间配置

    private static List<RoomConfigModel> ROOM_JOIN_SCORE = new ArrayList<>(); //用此索引判断玩家携带积分量，该进入哪个房间游戏

    private static class SingletonHolder {
        protected static final RoomConfigHolder instance = new RoomConfigHolder();
    }

    public static final RoomConfigHolder getInstance() {
        return RoomConfigHolder.SingletonHolder.instance;
    }

    private RoomConfigHolder() {

    }

    public void loadData(){
        //启动定时加载
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(
                () -> init(),
                1,
                5,
                TimeUnit.MINUTES);
        init();
    }

    /**
     * @param roomId
     * @return
     */
    //获取桌子的所有信息
    public RoomConfigModel getRoomConfig(String roomId) {
        return RoomConfigService.OBJ.getRoomConfig(roomId);
    }

    /**
     * 初始化内存
     */
    public boolean init(){
        List<RoomConfigModel> configList = JsonConfig.ME.getRoomConfigs();
        if(configList == null){
            log.error(" room's config is null ");
            throw new Error("无房间配置信息");
        }
        for (int i = 0; i < configList.size(); i++) {
            RoomConfigModel model = configList.get(i);
            ROOM_CONFIG.put(model.getRoomId(), model);
            log.debug("加载桌子：roomId->" + model.getRoomId());
            //将配置信息放入list
            ROOM_JOIN_SCORE.add(model);
        }
        log.info("加载 " + ROOM_CONFIG.size() + " 桌子配置");
        return true;
    }


    /**
     * 获取所有配置
     *
     * @return
     */
    public Map<String, RoomConfigModel> getAllConfig() {
        return ROOM_CONFIG;
    }

    /**
     * 是否能找到相应的房间
     *
     * @param score
     * @return
     */
    public boolean canFindSuuitableRoom(double score) {
        return false;
    }

}
