package com.slhb.game.service;

import com.slhb.base.enums.*;
import com.slhb.game.config.Config;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.gameUtil.DealCardForTable;
import com.slhb.game.gameUtil.GameLogic;
import com.slhb.game.gameUtil.GameOrderIdGenerator;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.room.cache.RoomTableSet;
import com.slhb.game.room.service.RoomConfigService;
import com.slhb.game.service.holder.CardOfTableHolder;
import com.slhb.game.service.holder.CommonConfigHolder;
import com.slhb.game.service.holder.RoomConfigHolder;
import com.slhb.base.model.RoomTableRelationModel;
import io.netty.util.internal.ConcurrentSet;
import lombok.extern.slf4j.Slf4j;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 桌子管理类：
 * 获取桌子，创建新桌子
 */
@Slf4j
public class TableService {
    /**
     * 所有桌子缓存
     * key：
     * 第一层Map：gameId + roomId
     * 第二层Map：tableId
     */
    private static final Map<String, Map<String, AbstractTable>> ROOM_TABLE_MAP = new ConcurrentHashMap();
    /**
     * 房间中有空余座位，可以加入的桌子
     * Key：gameId + roomId
     * Value：list tableId
     */
    private static final Map<String, Set<String>> ROOM_CAN_JOIN_TABLE = new ConcurrentHashMap<>();
    /**
     * 房间中已存在的最大桌子号（创建新桌子ID时，取此值+1）
     */
    private static final Map<String, AtomicInteger> ROOM_MAX_TABLE_ID = new ConcurrentHashMap<>();

    private static class SingletonHolder {
        protected static final TableService instance = new TableService();
    }

    public static final TableService getInstance() {
        return SingletonHolder.instance;
    }

    private TableService() {
    }

    /**
     * 初始化游戏内存
     */
    public void init() {
        Map<String, RoomConfigModel> map = RoomConfigHolder.getInstance().getAllConfig();
        String gameId = String.valueOf(Config.GAME_ID);

        map.forEach((s, roomConfig) -> {
            String roomId = roomConfig.getRoomId();
            //每个room初始创建N张桌子
            for (int i = 0; i < Config.ROOM_INIT_DESK_NUM; i++) {
                try {
                    AbstractTable tableInfo = createNewTable(gameId, roomId, false);
                    if (tableInfo == null) {
                        break;
                    }
                } catch (Exception e) {
                    log.error("init room ERROR, create table ERROR。", e);
                }
            }
            log.info("初始化桌子->gameId:" + gameId + " roomId:" + s);
        });

        log.debug("初始化结束---：");
    }

    //判断当前房间条件满足开始游戏否
    public void playGame(final AbstractTable table) {

        if (table.getTableStateEnum() == TableStateEnum.IDEL) {
            //当GM修改了配置需要重新加载
            table.setCommonConfig(CommonConfigHolder.getInstance().getCommonConfig());
            table.setRoomConfig(RoomConfigService.OBJ.getRoomConfig(table.getRoomId()));
            table.initTableStateAttribute();

            table.setCurrGameOrderId(GameOrderIdGenerator.generate(table.getRoomId(), table.getTableId()));//游戏此回合唯一订单号
            log.debug("play game game order id:" + table.getCurrGameOrderId());
            table.setGameStartTime(System.currentTimeMillis());

            CardOfTableHolder.PutCardOperationObj(table.getCurrGameOrderId(),
                    new DealCardForTable(new RoomTableRelationModel(table.getPlayType() + "", table.getRoomId(), table.getTableId(), table.getTableStateEnum().getValue()),
                            table.getCurrGameOrderId()));

            //加载杀量池配置
            table.loadKillPoolConfig();

            GameLogic.gameReady(table);//启动定时器
            log.debug("tableId={},开始游戏!", table.getTableId());
        }
    }


    /**
     * 是否存在桌子
     *
     * @param tableId
     * @return
     */
    public AbstractTable getTable(String gameId, String roomId, String tableId) {
        return getTableFromAllTableMap(gameId, roomId, tableId);
    }

    /**
     * @param gameId
     * @param roomId
     * @param tableInfo
     * @return
     */
    private boolean addTableInAllTableMap(String gameId, String roomId, AbstractTable tableInfo) {
        Map<String, AbstractTable> tableMap = null;
        if (ROOM_TABLE_MAP.containsKey(gameId + roomId)) {
            tableMap = ROOM_TABLE_MAP.get(gameId + roomId);
        }
        if (null == tableMap) {
            tableMap = new ConcurrentHashMap<>();
        }
        if (tableMap.containsKey(tableInfo.getTableId())) {
            return false;
        }
        tableMap.put(tableInfo.getTableId(), tableInfo);
        ROOM_TABLE_MAP.put(gameId + roomId, tableMap);
        return true;
    }

    private AbstractTable getTableFromAllTableMap(String gameId, String roomId, String tableId) {
        if (ROOM_TABLE_MAP.containsKey(gameId + roomId)) {
            Map<String, AbstractTable> tableMap = ROOM_TABLE_MAP.get(gameId + roomId);
            if (null != tableMap && tableMap.containsKey(tableId)) {
                return tableMap.get(tableId);
            }
        }
        return null;
    }

    private boolean removeTableFromAllTableMap(String gameId, String roomId, String tableId) {
        return false;
    }
    //endregion

    //region 操作ROOM_CAN_JOIN_TABLE
    public void addCanJoinTable(String gameId, String roomId, String tableId) {
        Set<String> tableIdList = null;
        if (ROOM_CAN_JOIN_TABLE.containsKey(gameId + roomId)) {
            tableIdList = ROOM_CAN_JOIN_TABLE.get(gameId + roomId);
        } else {
            tableIdList = new ConcurrentSet<>();
        }

        tableIdList.add(tableId);
        ROOM_CAN_JOIN_TABLE.put(gameId + roomId, tableIdList);
    }

    public boolean removeCanJoinTable(String gameId, String roomId, String tableId) {
        Set<String> tableIdList = null;
        if (ROOM_CAN_JOIN_TABLE.containsKey(gameId + roomId)) {
            tableIdList = ROOM_CAN_JOIN_TABLE.get(gameId + roomId);
        }

        if (null != tableIdList && tableIdList.size() > 0) {
            return tableIdList.remove(tableId);
        }
        return false;
    }


    /**
     * 创建一个新桌子
     * 看缓存里有没有对应的 有则不再创建
     *
     * @return
     */
    public AbstractTable createNewTable(String gameId, String roomId, boolean force) {
        try {
            if (force == false) {
                Map<String, RoomTableRelationModel> map = RoomTableSet.OBJ.get(roomId);
                if (map != null && map.size() >= Config.ROOM_INIT_DESK_NUM) {
                    Iterator<RoomTableRelationModel> iterator = map.values().iterator();
                    while (iterator.hasNext()) {
                        RoomTableRelationModel model = iterator.next();
                        AbstractTable tableInfo = new AbstractTable(gameId, model.getRoomId(), model.getTableId());
                        addTableInAllTableMap(gameId, roomId, tableInfo);
                        addCanJoinTable(gameId, roomId, model.getTableId());
                    }
                    return null;
                }
            }

            String tableId = getNewTableId(gameId, roomId) + "";
            //存入Redis
            addRedis(gameId, roomId, tableId);
            AbstractTable tableInfo = new AbstractTable(gameId, roomId, tableId);

            addTableInAllTableMap(gameId, roomId, tableInfo);
            addCanJoinTable(gameId, roomId, tableId);


            return tableInfo;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }

    private void addRedis(String gameId, String roomId, String tableId) {
        RoomTableRelationModel rt = new RoomTableRelationModel(gameId, roomId, tableId, TableStateEnum.IDEL.getValue());
        RoomTableSet.OBJ.add(roomId,tableId,rt);
    }

    private synchronized long getNewTableId(String gameId, String roomId) {
        if (!ROOM_MAX_TABLE_ID.containsKey(roomId)){
            ROOM_MAX_TABLE_ID.put(roomId, new AtomicInteger(1));
        }
        AtomicInteger atomic = ROOM_MAX_TABLE_ID.get(roomId);
        return 10000 + atomic.getAndIncrement();
    }

    /**
     * 直接销毁一个桌子
     *
     * @param roomId
     * @param tableId
     * @return
     */
    public void directDestroyTable(String gameId, String roomId, String tableId) {

        boolean isRemoveTable = removeTableFromAllTableMap(gameId, roomId, tableId);
        boolean isRemoveCanJoin = removeCanJoinTable(gameId, roomId, tableId);

        log.info("directDestroyTable()，gameId:" + gameId + ",roomId:" + roomId
                + ",TableId:" + tableId + ",isRemoveTable:" + isRemoveTable
                + ",isRemoveCanJoin:" + isRemoveCanJoin);
    }
    /**
     * 销毁一个桌子
     *
     * @param roomId
     * @param tableId
     * @return
     */
    public boolean destroyTable(String gameId, String roomId, String tableId) {
        boolean isRemoveTable = removeTableFromAllTableMap(gameId, roomId, tableId);
        boolean isRemoveCanJoin = removeCanJoinTable(gameId, roomId, tableId);

        boolean isDel = RoomTableSet.OBJ.remove(roomId,tableId);

        if (isDel) {
            directDestroyTable(gameId, roomId, tableId);
        }
        return isRemoveTable && isRemoveCanJoin;
    }



    public void addRoomCanJoinTable(String gameId, String roomId, String tableId) {
        try {
            if (getTableFromAllTableMap(gameId, roomId, tableId) == null) {
                return;
            }
            Set<String> TableIds = ROOM_CAN_JOIN_TABLE.get(gameId + roomId);
            if (TableIds.contains(tableId)) {
                log.info("exit addRoomCanJoinTable() rooid:" + roomId + ",tableid:" + tableId);
                return;
            }
            TableIds.add(tableId);
            log.info("addRoomCanJoinTable() rooid:" + roomId + ",tableid:" + tableId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
