package com.slhb.game.service.holder;

import com.slhb.core.jedis.StoredObjManager;
import com.slhb.base.enums.RedisConst;
import com.slhb.game.config.JsonConfig;
import com.slhb.game.dao.bean.CommonConfigModel;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 通用游戏配置
 */
@Slf4j
public class CommonConfigHolder {

    /**所有玩法对应的通用配置*/
    private static CommonConfigModel commonConfigModel = null;

    private List<CommonConfigModel> commonConfigs = new ArrayList<>();

    private static class SingletonHolder {
        protected static final CommonConfigHolder instance = new CommonConfigHolder();
    }

    public static final CommonConfigHolder getInstance() {
        return CommonConfigHolder.SingletonHolder.instance;
    }

    private CommonConfigHolder() {
    }

    public void loadData(){
        //启动定时加载
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(
                () -> init(),
                0,
                5,
                TimeUnit.MINUTES);
    }


    public CommonConfigModel getCommonConfig() {
        return commonConfigModel;
    }

    public List<CommonConfigModel> getCommonConfigs() {
        return commonConfigs;
    }

    /**
     * 加载内存
     */
    public boolean init() {
        try {
            List<CommonConfigModel> configList = JsonConfig.ME.getCommonConfigs();
            if (configList == null) {
                throw new Error("game CommonConfig is null");
            }
            commonConfigs = configList;

            for (int i = 0; i < configList.size(); i++) {
                commonConfigModel = configList.get(i);
            }
            log.info("加载 game  通用配置");
            return true;
        }catch(Exception ex){
            log.error(ex.getMessage());
        }
        return false;
    }
}
