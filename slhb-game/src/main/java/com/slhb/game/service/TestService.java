package com.slhb.game.service;

import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.TableStateEnum;
import com.slhb.base.model.RoomTableRelationModel;
import com.slhb.game.config.NameConfig;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.robot.utils.RandomTools;
import com.slhb.game.room.service.PlayerQueueService;
import com.slhb.game.utils.IdWorker;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class TestService {

    public static final TestService OBJ = new TestService();

    public void test(){
        String roomId = "1";
        joinTable(vitualHuman(), roomId);
    }



    private void joinTable(User user, String roomId){
        RoomTableRelationModel model = new RoomTableRelationModel("", "", "", TableStateEnum.IDEL.getValue());
        PlayerInfo player = new PlayerInfo(model, user);

        //排队入座
        PlayerQueueService.OBJ.addPlayer(user.getChannel_id(),roomId, player);
    }

    private User vitualHuman(){
        String id = String.valueOf(IdWorker.getInstance().nextId());
        log.debug("虚拟人物创建成功userId={}", id);

        int ico = RandomTools.getRandomInt(14) + 1;

        double buyInScore =  50000000;

        User user = new User();
        user.setId(id);
        user.setNick_name(NameConfig.ME.nextName());
        user.setChannel_id("1");
        user.setIco_url(String.valueOf(ico));
        user.setUser_defined_head(String.valueOf(ico));
        user.setMoney(buyInScore);

        log.debug("虚拟人物创建成功user={}",user.toString());
        return user;
    }

}
