package com.slhb.game.service;

import com.slhb.base.enums.RoleType;
import com.slhb.game.gw.gc.GameController;
import com.slhb.game.gw.gc.model.SingleConst;
import com.slhb.game.gw.gc.model.SingleControl;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.robot.utils.RandomTools;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 单人控制杀率服务
 */
@Slf4j
public class SingleControlService {

    public static final SingleControlService OBJ = new SingleControlService();

    //玩家控制配置文件
    private Map<String, SingleControl> _configMap = new ConcurrentHashMap<>();


    /**
     * 初始化
     */
    public void init(){
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(()->loadConfigs(),0,2, TimeUnit.MINUTES);
    }

    public void loadConfigs(){
        try {
            Map<String, SingleControl> tmpCfg = new ConcurrentHashMap<>();

            //查询 game control 获取单控人员名单
            List<SingleControl> list = GameController.ME.getSingleControlConfig();
            //如果list == null 定义为查询失败
            if (list == null){
                log.error("查询 game control 失败");
                return;
            }

            list.forEach(e->tmpCfg.put(e.getUser_id(), e));
            _configMap = tmpCfg;
            log.debug("成功加载 size = {} 条玩家配置 cfg = {}", tmpCfg.size(), tmpCfg);
        }catch (Exception e){
            log.error("加载单控玩家配置异常", e);
        }
    }


    /**
     * 玩家是否需要单独控制
     * @param userId    玩家ID
     * @return          true-需要
     */
    private boolean hasUser(String userId){
        return _configMap.containsKey(userId);
    }


    /**
     * 本局是否需要控反
     * @param table     牌桌内
     * @return          0-玩家不在单控名单、1-控真人赢、2-控真人输、3-自然发牌
     */
    public int singleControl(AbstractTable table){
        String userId = "";
        for (PlayerInfo player : table.getInGamePlayers().values()){
            if (player.getRoleType() == RoleType.GUEST){
                //找出本局真人
                userId = player.getUser().getAndroid_id();
                break;
            }
        }

        if (StringUtils.isEmpty(userId)){
            return SingleConst.notControl;
        }

        if (!hasUser(userId)){
            return SingleConst.notControl;
        }

        SingleControl cfg = _configMap.get(userId);
        if (cfg == null){
            return SingleConst.notControl;
        }

        //权重
        int weight = cfg.getWin() + cfg.getLose() + cfg.getWith();

        //随机概率
        int rate = RandomTools.getRandomInt(weight);
        log.debug("单控权重weight={}, rate={}. cfg={}", weight, rate, cfg.toString());

        if (rate < cfg.getLose()){
            return SingleConst.lose;
        }

        if (rate < cfg.getLose()+cfg.getWin()){
            return SingleConst.win;
        }

        return SingleConst.natural;
    }

}
