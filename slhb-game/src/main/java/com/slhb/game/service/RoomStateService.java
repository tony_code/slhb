package com.slhb.game.service;

import com.slhb.base.model.RoomTableRelationModel;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.room.cache.RoomTableSet;
import com.slhb.game.room.model.TableStatusInfo;
import lombok.extern.slf4j.Slf4j;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 通过修改redis 状态维护房间服务器缓存，包括 入桌/退桌 坐下/站起
 */
@Slf4j
public class RoomStateService {

    private static class SingletonHolder {
        protected static final RoomStateService instance = new RoomStateService();
    }

    public static final RoomStateService getInstance() {
        return RoomStateService.SingletonHolder.instance;
    }

    private Map<String, TableStatusInfo> _tableStateMap = new ConcurrentHashMap<>();

    /**
     * @param roomId
     * @param tableId
     * @return RoomTableRelationModel
     */
    public RoomTableRelationModel getExistTable(String gameId, String roomId, String tableId) {
        RoomTableRelationModel ret = RoomTableSet.OBJ.get(roomId,tableId);

        log.info("getExistTable() gameId->"+gameId+" roomId->"+roomId+",tableId ->"+tableId+(ret!=null?" "+ret.toString():" ret is null"));
        if (ret != null) {
            return ret;
        }
        return null;
    }

    /**
     * @param tableId
     * @return
     */
    private TableStatusInfo getTableStatusInfo(String tableId) {
        if (_tableStateMap.containsKey(tableId)){
            return _tableStateMap.get(tableId);
        }
        return null;
    }

    /**
     * 需要对结果做断言
     *
     * @return
     */
    private boolean updateTableStatusInfo(String tableId, TableStatusInfo tableStatusInfo) {
        _tableStateMap.put(tableId, tableStatusInfo);
        return true;
    }

    /**
     * 玩家加入桌子
     */
    public final void onPlayerJoinTable(final AbstractTable table) {
        TableStatusInfo statusInfo = getTableStatusInfo(table.getTableId());
        if (statusInfo != null) {
            log.debug("statusInfo 更新前->" + statusInfo.toString());
            statusInfo.setCurrentWaitingCount(statusInfo.getCurrentWaitingCount() + 1);
            updateTableStatusInfo(table.getTableId(), statusInfo);
            log.debug("statusInfo 更新后->" + statusInfo.toString());
        }
    }
}
