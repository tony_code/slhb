package com.slhb.game.service;

import com.slhb.core.utils.ThreadPoolManager;
import com.slhb.game.utils.runnable.UserRunnable;
import com.slhb.base.dao.bean.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MoneyService {
    private final static Logger logger = LoggerFactory.getLogger(MoneyService.class);

    private static class SingletonHolder {
        protected static final MoneyService instance = new MoneyService();
    }

    public static final MoneyService getInstance() {
        return MoneyService.SingletonHolder.instance;
    }




    /**
     * 加回金额
     *
     * @param userId
     * @param score
     */
    public double updateMoney(String userId, double score) {
        User user = PlayerService.getInstance().getUser(userId);

        if (user != null) {
            if (score < 0) {
                double userScore = user.getMoney();
                logger.info("UserMoney operation. updateMoney(), paramScore < 0,  userId->{}, paramScore->{}, userDBMoney->{}",
                        userId, score, userScore);
                return userScore;
            }

            user.setMoney(score);//user.getMoney() + score);
            ThreadPoolManager.getInstance().executeDbTask(new UserRunnable(user));
            /**保存玩家信息到缓存*/
            logger.info("14save userInfo->" + user.toString());
            PlayerService.getInstance().saveUser(user);
            logger.info("UserMoney operation. updateMoney(), userId->{}, paramScore->{}", userId, score);
            return user.getMoney();
        }
        return 0;
    }


}
