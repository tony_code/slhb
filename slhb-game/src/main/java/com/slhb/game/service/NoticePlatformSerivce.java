package com.slhb.game.service;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.RoleType;
import com.slhb.base.platform.HallAPIService;
import com.slhb.base.platform.bean.*;
import com.slhb.game.config.Config;
import com.slhb.game.gameUtil.GameOrderIdGenerator;
import com.slhb.game.gate.service.GateChannelService;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.service.beans.NoticePlatformBean;
import com.slhb.game.utils.NumUtils;
import com.slhb.game.vavle.notice.NoticeBroadcastMessages;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 平台通知服务
 * @since 2018/11/27 11:15
 */
@Slf4j
public class NoticePlatformSerivce {
    //单例
    public static final NoticePlatformSerivce OBJ = new NoticePlatformSerivce();

    //结算
    public static final String SETTLEMENT = "settlement";

    private static final SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    public Map<String, GameRecords> getRecords(AbstractTable table, List<JoloGame.JoloGame_TablePlay_PlayerSettleInfo> winList, double pfProfit, NoticePlatformBean bean) {
        try {
            Map<String, GameRecords> resultMap = new HashMap<>();

            String count = String.valueOf(table.getInGamePlayers().size());
            String curTime = sdf.format(new Date());
            String cardvalue = makCardValue(table);

            boolean valued = false;
            Map<String,Double> betMap = new HashMap<>();

            //下注信息构建
            String betInfo = makeBetInfo(table);
            String betResult = makeBetResult(table, winList);
            log.debug("下注信息 gameOrderid = {}, betinfo = {}, betresult = {}", table.getCurrGameOrderId(), betInfo, betResult);

            for (JoloGame.JoloGame_TablePlay_PlayerSettleInfo gamer : winList) {
                PlayerInfo player = table.getInGamePlayers().get(gamer.getSeatNum());



                if (player == null || player.getRoleType() == RoleType.ROBOT) {
                    continue;
                }
                GameRecords records = new GameRecords();
                records.setUser_id(player.getUser().getAndroid_id());
                records.setGame_id(String.valueOf(Config.GAME_ID));
                records.setRoom_id(getRoomLevel(table.getRoomId()) + "_" + Integer.toHexString(Integer.valueOf(table.getRoomId())));
                records.setTable_id(table.getTableId());
                records.setSeat_id(String.valueOf(player.getSeatNum()));
                records.setUser_count(count);
                records.setRound_id(table.getCurrGameOrderId());
                records.setRoom_type(getRoomLevel(table.getRoomId()));
                records.setCard_value(cardvalue);
                records.setInit_balance(NumUtils.double2String(player.getTotalTakeInScore() ));
                records.setBalance(NumUtils.double2String(player.getPlayScoreStore() ));


                records.setStart_time(sdf.format(table.getGameStartTime()));
                records.setEnd_time(curTime);
                records.setChannel_id(player.getUser().getChannel_id());
                if (StringUtils.isNotEmpty(player.getUser().getSub_channel_id())) {
                    records.setSub_channel_id(player.getUser().getSub_channel_id());
                }

                if (!valued){
                    records.setPlatform_profit("3:" + NumUtils.double2String(pfProfit));
                    valued = true;
                }
                records.setBet_info(betInfo);
                records.setBet_result(betResult);

                resultMap.put(player.getPlayerId(), records);
            }


            log.debug("orderId = {},下注集合={}",table.getCurrGameOrderId(),betMap);
            double totalAllBet = betMap.values().stream().mapToDouble(Double::longValue).sum();

            List<ModifyAndRecord> list = new ArrayList<>();
            bean.getPlayerScoreList().forEach(e->{
                if (e.getPlayer().getRoleType() == RoleType.ROBOT){
                    return;
                }

                ModifyAndRecord record = new ModifyAndRecord();
                record.setAccount(getModifyReqBean(bean.getTable(),e.getPlayer() ,e.getAddScore(),e.isAdd(), resultMap));
                record.setGame_record(resultMap.get(e.getPlayer().getPlayerId()));

                list.add(record);
            });

            //提交账户中心
            Map<String, UserModifyBean> map = HallAPIService.OBJ.modifyUserAccountAndUpdateGameRecord(list);
            this.processCallBack(table, map);
            //修改z值
            NoticeBroadcastMessages.oprKillPool(totalAllBet,table);

            return resultMap;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }

    //处理失败，直接踢掉玩家
    private void processCallBack(AbstractTable table, Map<String, UserModifyBean> map){
        try {
            if (map == null || map.size() == 0) {
                log.error("同步到大厅失败,map = null");
                for (PlayerInfo player :  table.getInGamePlayers().values()){
                    if (player.getRoleType() == RoleType.ROBOT) {
                        continue;
                    }
                    log.error("玩家userId = {},同步游戏记录到大厅失败,立即踢出", player.getPlayerId());
                    GateChannelService.OBJ.handlerDestoryUserChannel(player.getPlayerId());
                }
                return;
            }

            for (String userId : map.keySet()) {
                PlayerInfo player = table.getInGamePlayers().get(userId);
                if (player.getRoleType() == RoleType.ROBOT) {
                    continue;
                }

                UserModifyBean userModifyBean = map.get(userId);
                if (userModifyBean == null) {
                    log.error("同步到大厅失败,userModifyBean = null");
                    log.error("玩家userId = {},同步游戏记录到大厅失败,立即踢出", player.getPlayerId());
                    GateChannelService.OBJ.handlerDestoryUserChannel(player.getPlayerId());
                    continue;
                }
                if (userModifyBean.getCode() != 0) {
                    log.error("同步到大厅失败,userModifyBean.getCode = {}", userModifyBean.getCode());
                    log.error("玩家userId = {},同步游戏记录到大厅失败,立即踢出", player.getPlayerId());
                    GateChannelService.OBJ.handlerDestoryUserChannel(player.getPlayerId());
                    continue;
                }
                Balance balance = userModifyBean.getBalance();
                if (balance == null){
                    continue;
                }
                double goldTmp = balance.getGoldDouble();
                double curMoney = NumUtils.double2Decimal(goldTmp);

                //覆盖当前玩家货币
                player.setPlayScoreStore(curMoney);
                User user = PlayerService.getInstance().getUser(player.getPlayerId());
                if (user != null) {
                    user.setMoney(curMoney);
                    PlayerService.getInstance().saveUser(user);
                }
            }
        }catch (Exception ex){
            log.error("处理返回失败 ex = {}", ex);
        }
    }

    private String makCardValue(AbstractTable table){
        StringBuffer buffer = new StringBuffer();
        try {
            for (int index = 1; index<=4; index ++){
                PlayerInfo player = table.getInGamePlayers().get(index);
                if (player == null){
                    //座位为空
                    buffer.append("0000000000");
                    continue;
                }
            }

            //加上庄家
            buffer.append(table.getCurrDealerSeatNum());
        }catch (Exception ex){
            log.error("转换牌型失败ex={}",ex);
        }
        return buffer.toString();
    }

    private String makeCard (int[] handcard){
        StringBuffer buffer = new StringBuffer();
        if (handcard == null){
            buffer.append("0000000000");
            return buffer.toString();
        }

        for (int card: handcard){
            int value = card % 13;
            int color = card <= 13 ? 4: (card <= 26 ? 3 : (card <= 39 ? 2 : 1));
            //构建
            if (value == 10){
                buffer.append("a").append(color);
            }else if (value == 11){
                buffer.append("b").append(color);
            }else if (value == 12){
                buffer.append("c").append(color);
            }else if (value == 0){
                buffer.append("d").append(color);
            }else {
                buffer.append(value).append(color);
            }
        }
        return buffer.toString();
    }

    /**
     * 根据给定规则，获取房间级别
     * @param roomId
     * @return
     */
    public String getRoomLevel(String roomId){
        return roomId;
    }

    public ModifyReqBean getModifyReqBean(AbstractTable table, PlayerInfo player,double gold, boolean isAdd,  Map<String, GameRecords> map) {
        try {
            double oprGold = gold;
            if (!isAdd) {
                oprGold = -gold;
            }

            String bet = "";
            if (map.containsKey(player.getPlayerId())){
                GameRecords record = map.get(player.getPlayerId());
                if (record != null){
                    bet = record.getAvail_bet();
                }
            }

            ModifyReqBean bean = new ModifyReqBean();
            bean.setUser_id(player.getUser().getAndroid_id());
            bean.setBehavior(SETTLEMENT);
            bean.setGold(NumUtils.double2String(oprGold));
            bean.setValid_gold(bet);
            bean.setGame_id(String.valueOf(Config.GAME_ID));
            bean.setRoom_id(table.getRoomId());
            bean.setSeat_id(String.valueOf(player.getSeatNum()));
            bean.setRound_id(table.getCurrGameOrderId());
            bean.setChannel_id(player.getUser().getChannel_id());
            if (StringUtils.isNotEmpty(player.getUser().getSub_channel_id())) {
                bean.setSub_channel_id(player.getUser().getSub_channel_id());
            }
            //格式：0_yyyyMMddHHmmssSSS_{game_id}_{user_id}_内部编号，长度48位以内
            String orderId = "0_" + df.format(new Date()) + "_" + bean.getGame_id() + "_" + player.getUser().getAndroid_id() + "_" + GameOrderIdGenerator.generate();
            bean.setOrder_id(orderId);
            return bean;
        } catch (Exception ex) {
            log.error("更新大厅货币异常ex={}", ex);
        }
        return null;
    }

    private String makeBetInfo (AbstractTable table){
        String bet_info = "";
        try {
            for (PlayerInfo player : table.getInGamePlayers().values()) {
                bet_info += player.getSeatNum();
                bet_info += ":";
                if (player.getIsDealer() == 1){
                }else {
                    bet_info += "0";
                }
                bet_info += ":";
                if (player.getIsDealer() == 1){
                    bet_info += "0";
                }else {
                }
                bet_info += ":";
                bet_info += NumUtils.double2String(player.getTotalTakeInScore());
                bet_info += ":";
                bet_info += ":";
                bet_info += ";";
            }
        }catch (Exception e){
            log.error("makeBetInfo error ,gameOrderId = {}", table.getCurrGameOrderId(), e);
            return "";
        }
        return bet_info;
    }

    private String makeBetResult(AbstractTable table, List<JoloGame.JoloGame_TablePlay_PlayerSettleInfo> winList){
        String bet_result = "";
        try {
            for (PlayerInfo player : table.getInGamePlayers().values()) {
                bet_result += player.getSeatNum();
                bet_result += ":";

                Optional<JoloGame.JoloGame_TablePlay_PlayerSettleInfo> gamerTmp = winList.stream().filter(e->player.getPlayerId().equals(e.getUserId())).findFirst();
                if (gamerTmp == null){continue;}
                JoloGame.JoloGame_TablePlay_PlayerSettleInfo gamer = gamerTmp.get();
                if (gamer.getWinLose() == 0){
                    bet_result += "-";
                }
                bet_result += NumUtils.double2String(gamer.getWinLoseScore());
                bet_result += ";";
            }
        }catch (Exception e){
            log.error("makeBetResult error ,gameOrderId = {}", table.getCurrGameOrderId(), e);
            return "";
        }
        return bet_result;
    }
}
