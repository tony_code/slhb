package com.slhb.game.service;


import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.RoleType;
import com.slhb.game.gw.gc.GameController;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.play.AbstractTable;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 分离子渠道奖池服务
 */
@Slf4j
public class SplitPoolService {

    public static final SplitPoolService OBJ = new SplitPoolService();

    //玩家控制配置文件<channel_id@sub_channel_id>
    private Set<String> _configSet = new LinkedHashSet<>();


    /**
     * 初始化
     */
    public void init(){
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(()->loadConfigs(),0,2, TimeUnit.MINUTES);
    }

    public void loadConfigs(){
        try {
            List<String> list = GameController.ME.getSplitPoolConfig();
            if (list == null){
                //加载异常，直接退出
                return;
            }

            Set<String> tmpCfg = new HashSet<>();
            list.forEach(e -> tmpCfg.add(e));

            //替换以前的配置
            _configSet = tmpCfg;
            log.info("加载子渠道独立奖池配置 size = {}, cfg = {}", _configSet.size(), _configSet);
        }catch (Exception e){
            log.error("加载单控玩家配置异常", e);
        }
    }

    /**
     * 本局是否需要独立奖池
     * @param table     牌桌内
     * @return          true-独立  false-统一
     */
    public void splitPool(AbstractTable table){
        try {
            User realUser = null;
            for (PlayerInfo player : table.getInGamePlayers().values()){
                if (player.getRoleType() == RoleType.GUEST){
                    //找出本局真人
                    realUser = player.getUser();
                    break;
                }
            }

            if (realUser == null){
                return ;
            }

            if (StringUtils.isEmpty(realUser.getChannel_id()) || StringUtils.isEmpty(realUser.getSub_channel_id())){
                //玩家渠道或者子渠道为空
                return;
            }

            String subchannel = realUser.getChannel_id()+"@"+realUser.getSub_channel_id();
            if (_configSet.contains(subchannel)){
                //设置牌桌是否需要分奖池
                table.setSplitPool(true);
                table.setSplitPoolChannel(subchannel);
            }
        }catch (Exception e){
            log.error("获取奖池分离状态失败", e);
        }
    }

}
