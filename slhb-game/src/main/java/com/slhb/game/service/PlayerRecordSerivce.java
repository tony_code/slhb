package com.slhb.game.service;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.base.dao.bean.User;
import com.slhb.base.platform.HallAPIService;
import com.slhb.base.platform.bean.PlayerRecords;
import com.slhb.game.config.Config;
import com.slhb.game.utils.NumUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.*;


/**
 * 玩家牌局记录
 * @author
 * @since 2018/9/11 20:04
 */
@Slf4j
public class PlayerRecordSerivce {


    public static final PlayerRecordSerivce OBJ = new PlayerRecordSerivce();

    //最大记录数
    private static final Integer MAX_RECORDS = 10;

    public List<JoloGame.JoloGame_GameRecordsInfo> getPlayRecords(String userId){
        List<JoloGame.JoloGame_GameRecordsInfo> beans = new ArrayList<>();

        User user = PlayerService.getInstance().getUser(userId);
        if (user == null) {
            log.error("缓存中没有user信息, userId->{}", userId);
            return beans;
        }

        List<PlayerRecords> list = HallAPIService.OBJ.getGameRecord(user.getAndroid_id(), String.valueOf(Config.GAME_ID), String.valueOf(MAX_RECORDS));
        if (list == null || list.size() == 0){
            log.error("大厅记录为空信息, list=null");
            return beans;
        }

        list.forEach(e->{
            double profit = Double.valueOf(StringUtils.isEmpty(e.getProfit())?"0":e.getProfit()).doubleValue();
            double bet = Double.valueOf(StringUtils.isEmpty(e.getAll_bet())?"0":e.getAll_bet()).doubleValue();
            double win = (profit-bet) ;
            beans.add(JoloGame.JoloGame_GameRecordsInfo.newBuilder()
                    .setGameOrderId(e.getRound_id())
                    .setRoomId(e.getRoom_type())
                    .setTableId(e.getTable_id())
                    .setWins(NumUtils.double2Decimal(Math.abs(win)))
                    .setTime(e.getEnd_time())
                    .setIsWin(win > 0?0:1)
                    .build());
        });
        return beans;
    }


}



