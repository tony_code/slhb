package com.slhb.game.service;

import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.model.RedEnv;
import com.slhb.game.model.RedEnvDetail;
import com.slhb.game.robot.utils.RandomTools;
import com.slhb.game.room.service.RoomConfigService;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * 红包服务
 */
@Slf4j
public class RedEnvService  {

    public static final RedEnvService OBJ = new RedEnvService();

    /**
     * 玩家创建一个红包
     * @param userId    玩家userId
     * @param roomId    房间Id
     * @param thundernum 雷号
     * @param money     红包总钱
     * @return
     */
    public RedEnv createPlayerRedenv(String userId, String roomId, int thundernum, int money){
        RoomConfigModel cfg = RoomConfigService.OBJ.getRoomConfig(roomId);
        if (cfg == null){
            log.error("房间配置为空 room = {}", roomId);
            return null;
        }

        //红包数
        int envNum = cfg.getNum();

        //预设每个红包的钱
        List<RedEnvDetail> list = partEnv(money, envNum, thundernum);
        if (list == null || list.isEmpty()){
            log.error("红包预设失败 userId = {}", userId);
            return null;
        }

        RedEnv red = new RedEnv();
        red.setOfferUserId(userId);
        red.setEnvNum(envNum);
        red.setThunderEnd(thundernum);
        red.setMoney(money);
        red.setEnvs(list);
        return red;
    }

    /**
     * 机器人创建一个红包
     * @param userId
     * @param roomId
     * @return
     */
    public RedEnv createRobotRedenv(String userId, String roomId){
        RoomConfigModel cfg = RoomConfigService.OBJ.getRoomConfig(roomId);
        if (cfg == null){
            log.error("房间配置为空 room = {}", roomId);
            return null;
        }

        //红包数
        int envNum = cfg.getNum();
        //红包钱数
        int money = RandomTools.getRandomInt(cfg.getMaxEnv()-cfg.getMinEnv()) + cfg.getMinEnv();
        //红包雷号
        int thundernum = RandomTools.getRandomInt(9);

        //预设每个红包的钱
        List<RedEnvDetail> list = partEnv(money, envNum, thundernum);
        if (list == null || list.isEmpty()){
            log.error("红包预设失败 userId = {}", userId);
            return null;
        }

        RedEnv red = new RedEnv();
        red.setOfferUserId(userId);
        red.setEnvNum(envNum);
        red.setThunderEnd(thundernum);
        red.setMoney(money);
        red.setEnvs(list);
        return red;
    }

    /**
     * 分红包
     * @param money
     * @param num
     * @return
     */
    private List<RedEnvDetail> partEnv(int money, int num, int thrunder){
        int allMoney = money / 100;
        List<Double> list = transfer(divideRedPackage(allMoney, num, allMoney/2));
        List<RedEnvDetail> details = new ArrayList<>();

        //由大到小排序
        Collections.sort(list);
        //取最大红包
        double luckyNum = list.get(list.size()-1);
        if (isThrunder(luckyNum, thrunder) && list.size()  > 1){
            luckyNum = list.get(list.size()-2);
        }
        //混淆
        Collections.shuffle(list);

        //构建红包
        int index = 1;
        boolean hasLucky = false;
        for (double e : list){
            boolean isThunder = this.isThrunder(e, thrunder);
            RedEnvDetail detail = null;
            if (e == luckyNum && !hasLucky){
                detail = new RedEnvDetail(index, e, isThunder, true);
                hasLucky = true;
            }else {
                detail = new RedEnvDetail(index, e, isThunder, false);
            }
            details.add(detail);
            index ++;
        }
        return details;
    }

    /**
     * 预设红包
     * @param allMoney      红包钱数
     * @param peopleCount   人数
     * @param MAX           红包最大值
     * @return
     */
    private List<Integer>  divideRedPackage(int allMoney, int peopleCount,int MAX) {
        //人数比钱数多则直接返回错误
        if(peopleCount<1||allMoney<peopleCount){
            log.error("钱数人数设置错误！");
            return null;
        }
        List<Integer> indexList = new ArrayList<>();
        List<Integer> amountList = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < peopleCount - 1; i++) {
            int index;
            do{
                index = random.nextInt(allMoney - 2) + 1;
            }while (indexList.contains(index));//解决碰撞
            indexList.add(index);
        }
        Collections.sort(indexList);
        int start = 0;
        for (Integer index:indexList) {
            //解决最大红包值
            if(index-start>MAX){
                amountList.add(MAX);
                start=start+MAX;
            }else{
                amountList.add(index-start);
                start = index;
            }
        }
        amountList.add(allMoney-start);
        return amountList;
    }

    private List<Double> transfer(List<Integer> list){
        List<Double> doubleArrays = new ArrayList<>();
        list.forEach(e -> doubleArrays.add(e * 100d));
        return doubleArrays;
    }

    private boolean isThrunder(double money, int thunder){
        return false;
    }

}
