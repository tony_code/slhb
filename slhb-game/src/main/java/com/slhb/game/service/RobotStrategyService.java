package com.slhb.game.service;

import com.slhb.game.config.JsonConfig;
import com.slhb.game.dao.bean.KillPoolConfig;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 机器人输赢策略
 *
 * @author
 *
 * @since 2018/11/1 10:24
 */
@Slf4j@Getter
public class RobotStrategyService {

    public static final RobotStrategyService OBJ = new RobotStrategyService();

    //机器人输赢
    private static final boolean common = false;
    private static final boolean win = true;

    private static final String DEFUALT = "all";

    private static final String SPLIT = "_";

    private Map<String, Map<String, KillPoolConfig>> _pool = new ConcurrentHashMap<>();

    private Map<Integer, KillPoolConfig> _idxMap = new ConcurrentHashMap<>();

    private List<KillPoolConfig> _list = new ArrayList<>();

    public void init(){
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(()-> loadFromDb(),1, 3*60, TimeUnit.SECONDS);
    }

    private void loadFromDb(){
        //加载数据库到内存
        List<KillPoolConfig> list = JsonConfig.ME.getKillPoolConfigs();
        if (list == null || list.size() == 0){
            return;
        }
        _list = list;

        _pool.clear();

        list.forEach(e->{
            _idxMap.put(e.getId(), e);

            if (_pool.containsKey(e.getChannelId())){
                Map<String, KillPoolConfig> tmpMap = _pool.get(e.getChannelId());
                tmpMap.put(e.getRoomId(), e);
                _pool.put(e.getChannelId(), tmpMap);
            }else {
                Map<String, KillPoolConfig> tmpMap = new ConcurrentHashMap<>();
                tmpMap.put(e.getRoomId(), e);
                _pool.put(e.getChannelId(), tmpMap);
            }
        });

        log.info("AI机器人,数据库配置={}", _pool.toString());
    }


    /**
     * 根据渠道、房间获取杀量配置
     * @param channel
     * @param roomId
     * @return
     */
    public KillPoolConfig getPool(String channel, String roomId){
        KillPoolConfig config = null;
        String roomLevel = transferTable(roomId);
        if (_pool.containsKey(channel)){
            Map<String,KillPoolConfig> map = _pool.get(channel);
            if (map.containsKey(roomLevel)){
                config = map.get(roomLevel);
            }
        }

        if (config == null){
            config = _pool.get(DEFUALT).get(roomLevel);
        }

        if (config == null){
            log.error("获取渠道杀量配置异常 channel={},room={},pool=null",channel,roomId);
        }else {
            log.debug("获取渠道杀量配置 channel={},room={}",channel,roomId);
        }

        return config;
    }

    public String transferTable(String roomId){
        String roomLevel = NoticePlatformSerivce.OBJ.getRoomLevel(roomId);
        return roomLevel;
    }

    public KillPoolConfig getPoolById (int idx){
        return _idxMap.get(idx);
    }
}
