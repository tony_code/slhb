package com.slhb.game.robot;

import JoloProtobuf.GameSvr.JoloGame;
import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.PlayerStateEnum;
import com.slhb.game.config.Config;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.model.RedEnvDetail;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.robot.service.RobotDictionary;
import com.slhb.game.robot.utils.RandomTools;
import com.slhb.game.service.TableService;
import com.slhb.game.vavle.notice.NoticeBroadcastMessages;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 机器人
 * @since 2019/6/11 19:46
 */
@Getter@Slf4j@ToString
public class Robot {

    private String userId;

    private User user;

    private String room;

    private String table;

    private long createtime;

    /**
     * 机器人属性构造
     * @param _user
     * @param _roomId
     * @param _tableId
     */
    public Robot(User _user, String _roomId, String _tableId){
        //构造机器人属性
        this.userId = _user.getId();
        this.user = _user;
        this.room = _roomId;
        this.table = _tableId;
        this.createtime = System.currentTimeMillis();
    }

    public void sayHello(){
        log.info("hi , I'am created, user = {}",this.user.toString());
    }

    //抢钱
    public void rushDealer(byte[] bytes){
        log.debug("机器人{}开始抢",userId);
        try {
            JoloGame.JoloGame_Notice2Client_GameStartReq ack = JoloGame.JoloGame_Notice2Client_GameStartReq.parseFrom(bytes);
            if (ack.getRoomId().equals(room) && ack.getTableId().equals(table)) {
                int delay = RandomTools.getRandomInt(3);
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    public void run() {
                        rob();
                    }
                },delay*1000);
            }
        }catch (Exception ex){
            log.error("抢庄失败",ex);
        }
    }

    //销毁
    public void distroy(){
        RobotDictionary.ME.removeRobot(user);
        RobotDictionary.ME.removeRobot(this);
        this.userId = "";
        this.room = "";
        this.table = "";
        this.createtime = 0l;
        this.user = null;
    }

    //机器人抢红包
    private void rob(){
        //牌桌
        AbstractTable t = TableService.getInstance().getTable(String.valueOf(Config.GAME_ID), room, table);
        if (t == null) return;

        PlayerInfo player = t.getPlayer(userId);
        if (player == null) return;

        //尝试抢红包
        RedEnvDetail pack = t.robPackage(userId);
        if (pack == null) {
            log.debug("抢红包失败");
            return;
        }

        player.setCurRob(pack);
        player.setState(PlayerStateEnum.rob);
        t.getAllPlayers().put(userId, player);

        log.debug("机器人robotId={},抢了个红包money = {}", userId, pack.toString());
        //通知全桌
        NoticeBroadcastMessages.robRedEnvBoardcast(t, player);
    }

}
