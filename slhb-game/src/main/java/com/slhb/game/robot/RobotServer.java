package com.slhb.game.robot;

import com.google.protobuf.MessageLite;
import com.slhb.game.robot.service.RobotDictionary;
import com.slhb.game.service.holder.FunctionIdHolder;
import lombok.extern.slf4j.Slf4j;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * @author
 * @since 2019/6/11 16:19
 */
@Slf4j
public class RobotServer {

    public static RobotServer OBJ = new RobotServer();

    //机器人操作线程
    private ExecutorService executor = Executors.newFixedThreadPool(20);

    public void noticeRobot(String userId, int msgId, MessageLite msg){
        Robot robot = RobotDictionary.ME.getRobot(userId);

        //机器人不存在
        if (robot == null) return;

        //扔线程里处理去
        executor.submit(() -> routeMsg(robot, msgId, msg));
    }

    private void routeMsg(Robot robot, int msgId, MessageLite msg){
        switch (msgId) {
            //通知抢钱
            case FunctionIdHolder.Game_Notice_Rob_Start:
                robot.rushDealer(msg.toByteArray());
                break;
            //通知离开
            case FunctionIdHolder.Game_Notice2Client_leavel:
                robot.distroy();
                break;
            default:
                //log.debug("玩家player = {} 收到消息functionId = {} 未做处理", robot.getUserId(), msgId);
                break;
        }
    }
}
