package com.slhb.game.robot.service;

import com.slhb.base.dao.bean.User;
import com.slhb.game.robot.Robot;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 机器人字典
 * @since 2019/6/12 15:36
 */
public class RobotDictionary {

    public static final RobotDictionary ME = new RobotDictionary();

    //机器人字典
    private Map<String,User> _robotUserMap = new ConcurrentHashMap<>();

    //机器人字典
    private Map<String,Robot> _robotMap = new ConcurrentHashMap<>();

    /**
     * 字典里添加
     * @param user
     */
    public void createRobot(User user){
        if (user == null) {
            return;
        }
        _robotUserMap.put(user.getId(), user);
    }

    public void createRobot(Robot robot){
        if (robot == null) return;
        _robotMap.put(robot.getUserId(), robot);
    }

    /**
     * 字典里移除
     * @param user
     */
    public void removeRobot(User user){
        if (user == null){
            return;
        }

        if (_robotUserMap.containsKey(user.getId())){
            _robotUserMap.remove(user.getId());
        }
    }

    public void removeRobot(Robot robot){
        if (robot == null) return;

        if (_robotMap.containsKey(robot.getUserId())){
            _robotMap.remove(robot.getUserId());
        }
    }

    /**
     * 查询字典
     * @param userId
     * @return
     */
    public User getRobotUser(String userId){
        if (_robotUserMap.containsKey(userId)){
            return _robotUserMap.get(userId);
        }
        return null;
    }

    public Robot getRobot(String userId){
        if (_robotMap.containsKey(userId)){
            return _robotMap.get(userId);
        }
        return null;
    }

}
