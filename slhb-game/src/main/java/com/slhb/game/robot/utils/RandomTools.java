package com.slhb.game.robot.utils;

import java.util.Random;

public class RandomTools {
    /**
     * 有边界的随机数（0 ~ max）
     * @param maxNum
     * @return
     */
    public static int getRandomNum(int maxNum){
        if(maxNum<=0){
            return 0;
        }
        Random random = new Random(System.currentTimeMillis()+System.nanoTime());
        int rd = random.nextInt(maxNum)+1;
        return rd;
    }

    /**
     * 有边界的随机数[0 ~ max）
     * @param maxNum
     * @return
     */
    public static int getRandomInt(int maxNum){
        if(maxNum<=0){
            return 0;
        }
        Random random = new Random(System.currentTimeMillis()+System.nanoTime());
        int rd = random.nextInt(maxNum);
        return rd;
    }

    public static double getRandomDouble(double maxNum){
        double minNum = 0.01;
        double boundedDouble = minNum + new Random(System.currentTimeMillis()+System.nanoTime()).nextDouble() * (maxNum - minNum);
        return boundedDouble;
    }

    public static double getRandomDouble(double minNum ,double maxNum){
        double boundedDouble = minNum + new Random(System.currentTimeMillis()+System.nanoTime()).nextDouble() * (maxNum - minNum);
        return boundedDouble;
    }

}
