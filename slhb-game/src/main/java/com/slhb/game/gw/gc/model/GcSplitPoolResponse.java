package com.slhb.game.gw.gc.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

//gc返回的数据格式
//{"code":0,"data":[]}

@ToString
@Getter@Setter
public class GcSplitPoolResponse {

    private int code;

    private List<String> data;

}
