package com.slhb.game.gw.gc.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;


//{"code":0,"data":[{"user_id":"asd","win":"20","lose":"0","with":"80","fish_hit_rate":"11"}]}

@ToString
@Getter@Setter
public class GcSingleControlResponse {

    private int code;

    private List<SingleControl> data;

}
