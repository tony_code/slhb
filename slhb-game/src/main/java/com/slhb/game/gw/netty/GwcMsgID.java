package com.slhb.game.gw.netty;

/**
 * 消息号
 * @author
 * @since 2018/11/26 14:51
 */
public class GwcMsgID {

    public static final int BroadcastByUID = 1;

    public static final int Broadcast = 2;

    public static final int HeartbeatReq = 3;

    public static final int HeartbeatRes = 4;

    public static final int NotifyClose = 5;

    public static final int TickUser = 6;

    public static final int OnlineUserNumReq = 7;

    public static final int OnlineUserNumRes = 8;

    public static final int IsOnlineReq = 9;

    public static final int IsOnlineRes = 10;

    public static final int ForbiddenIp = 11;

    public static final int CanBeModifyUserAccountReq = 23;

    public static final int CanBeModifyUserAccountRes = 24;
}
