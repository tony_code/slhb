package com.slhb.game.gw;

import com.slhb.base.platform.HallAPIService;
import com.slhb.base.platform.bean.OnlineResBean;
import com.slhb.game.config.Config;
import com.slhb.game.room.service.RoomConfigService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 消息发送
 * @author
 * @since 2019/3/15 10:49
 */
@Slf4j
public class MsgSender {
    //单例
    public static final MsgSender ME = new MsgSender();

    /**
     * 获取全服在线人数
     */
    public void getAllServerOnlineNum(){
        log.debug("请求本游戏在线人数");
        List<OnlineResBean> list = HallAPIService.OBJ.onlineNum(String.valueOf(Config.GAME_ID), "");
        if (list == null){
            return;
        }

        int count = list.stream().mapToInt(e -> e.getNum()).sum();

        log.info("本游戏在线人数 count = {}", count);
        //本游戏在线
        RoomConfigService.OBJ.setPlayers(count, 0);
    }

}
