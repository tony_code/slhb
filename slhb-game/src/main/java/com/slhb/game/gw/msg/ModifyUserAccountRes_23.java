package com.slhb.game.gw.msg;

import JoloProtobuf.GW.Gwc;
import com.slhb.base.dao.bean.User;
import com.slhb.base.model.GameRoomTableSeatRelationModel;
import com.slhb.core.jedis.StoredCommManager;
import com.slhb.game.gate.service.UserService;
import com.slhb.game.gw.netty.AbstractGwcHander;
import com.slhb.game.gw.netty.GwcMsg;
import com.slhb.game.gw.netty.GwcMsgID;
import com.slhb.game.room.cache.UserTableSet;
import com.slhb.game.service.PlayerService;
import com.slhb.game.utils.NumUtils;
import io.netty.channel.ChannelHandlerContext;


/**
 * 请求在线下分
 *      如果用户在游戏分大厅，则可以进行下分操作。如果余额足够，则在游戏服扣费后同时返回协议，如果用户在游戏中，则不可以做下分操作！
 *
 * uid
 *      用户id
 *
 * gold
 *      是要下分的金额（已经乘以了10000，我们游戏中直接加减）
 *      当gold=0时，说明该请求希望把所有可下分的金额都下分
 *      当gold < 0 时，说明希望下分的金额为 gold（不是全部余额）
 *      当gold > 0 说明是上分了，上分逻辑日后实现
 *
 * seqID
 *      请求序列ID
 *
 *
 */
public class ModifyUserAccountRes_23 extends AbstractGwcHander {

    public ModifyUserAccountRes_23() {
        super(GwcMsgID.CanBeModifyUserAccountReq, GwcMsgID.CanBeModifyUserAccountRes);
    }

    @Override
    public void process(ChannelHandlerContext ctx, GwcMsg msg) throws Exception {
        //收到消息
        Gwc.canBeModifyUserAccountReq req = Gwc.canBeModifyUserAccountReq.parseFrom(msg.getBody());
        log.debug("收到cmd = 23，消息={}", req.toString());

        //消息参数
        String userId = req.getUid();
        long gold = req.getGold();
        String seqId = req.getSeqID();

        //返回消息构建
        Gwc.canBeModifyUserAccountRes.Builder res = Gwc.canBeModifyUserAccountRes.newBuilder();
        res.setCode(0).setUid(userId).setGold(gold).setSeqID(seqId);

        try {
            //查询玩家
            User u = PlayerService.getInstance().getUser(userId);
            if (u == null){
                log.error("游戏内没有查询到该玩家 openid={}",userId);
                sendMsg(ctx, res.build().toByteArray());
                return;
            }

            //首先查看自己有没有在游戏内
            GameRoomTableSeatRelationModel inGame = UserTableSet.OBJ.get(u.getId());
            if (inGame != null){
                log.error("玩家在游戏中,不下分 openId={},userId={} ", u, u.getId());
                res.setCode(5301).setGold(0);
                sendMsg(ctx, res.build().toByteArray());
                return;
            }

            //开始下分操作
            //锁定这个玩家N秒, 防止玩家下分同时入桌,导致带入和实际不一致, 锁定效果N秒后自动释放
            StoredCommManager.acquireLock("ModifyUserAccountRes_"+userId, 5);
            double curmoney = 0;
            if (gold == 0){
                //全卸掉
                curmoney = 0;
                res.setGold(Long.parseLong(NumUtils.double2String(u.getMoney()*-1)));
            }else {
                //卸掉变化量
                curmoney = u.getMoney() + gold;
            }

            if (curmoney < 0){
                log.error("玩家 {} 余额不足，money = {}, gold = {}", userId, u.getMoney(), gold);
                res.setCode(5103).setGold(0);
                sendMsg(ctx, res.build().toByteArray());
                return;
            }

            //修改玩家货币
            u.setMoney(curmoney);
            PlayerService.getInstance().saveUser(u);

            //通知客户端货币变化
            UserService.getInstance().sendPayNoticeMsg(u.getId(), u.getMoney());

            //返回消息发送
            sendMsg(ctx, res.build().toByteArray());
        }catch (Exception e){
            log.error("消息处理失败", e);
        }finally {
            log.debug("返回cmd = 24，消息={}", res.toString());
            StoredCommManager.del("ModifyUserAccountRes_" + userId);
        }
    }

}
