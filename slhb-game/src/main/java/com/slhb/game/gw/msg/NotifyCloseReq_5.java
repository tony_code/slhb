package com.slhb.game.gw.msg;

import JoloProtobuf.GW.Gwc;
import com.slhb.core.jedis.StoredObjManager;
import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.RedisConst;
import com.slhb.base.model.GameRoomTableSeatRelationModel;
import com.slhb.game.dao.DBUtil;
import com.slhb.game.gate.service.GateChannelService;
import com.slhb.game.gw.netty.AbstractGwcHander;
import com.slhb.game.gw.netty.GwcMsg;
import com.slhb.game.gw.netty.GwcMsgID;
import com.slhb.game.room.cache.UserTableSet;
import com.slhb.game.room.service.PlayerQueueService;
import com.slhb.game.service.PlayerService;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 通知服务器玩家断开
 */
@Slf4j
public class NotifyCloseReq_5 extends AbstractGwcHander {

    private static final ExecutorService fixpool = Executors.newFixedThreadPool(5);

    public NotifyCloseReq_5() {
        super(GwcMsgID.NotifyClose);
    }

    @Override
    public void process(ChannelHandlerContext ctx, GwcMsg msg) throws Exception {
        Gwc.NotifyClose req = Gwc.NotifyClose.parseFrom(msg.getBody());
        log.debug("收到玩家断开通知msgId = {}, msg={}", msg.getCmd(), req.toString());

        //平台ID
        String openId = req.getUid();

        fixpool.submit(()->closeCtx(openId));
    }

    private void closeCtx (String openId){
        try {
            User user = PlayerService.getInstance().getUser(openId);
            if (user == null){
                log.error("游戏内没有该玩家 openid={}",openId);
                return;
            }

            //首先查看自己有没有在游戏内
            GameRoomTableSeatRelationModel inGame = UserTableSet.OBJ.get(user.getId());

            if (inGame != null){
                log.error("玩家在游戏中,不断开 openId={},userId={} ",openId,user.getId());
                return;
            }

            //再查看玩家是否在游戏队列中
            if (PlayerQueueService.OBJ.get_inQueueIds().contains(user.getId())){
                log.error("玩家已在游戏队列中，无法退出 openId={},userId={} ",openId, user.getId());
                return;
            }

            //通知gate断开
            PlayerService.getInstance().onPlayerLoutOut(openId);
            GateChannelService.OBJ.handlerDestoryUserChannel(user.getId());
        }catch (Exception ex){
            log.error("玩家下线失败openid={}",openId);
        }
    }
}
