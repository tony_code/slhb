package com.slhb.game.gw.msg;

import JoloProtobuf.GW.Gwc;
import com.slhb.game.gw.netty.AbstractGwcHander;
import com.slhb.game.gw.netty.GwcMsg;
import com.slhb.game.gw.netty.GwcMsgID;
import com.slhb.game.room.service.RoomConfigService;
import io.netty.channel.ChannelHandlerContext;

/**
 * GWC响应游戏的拉用户人数请求
 * @author
 * @since 2019/3/15 10:06
 */
public class GetOnlineUserNumRes_8 extends AbstractGwcHander {

    public GetOnlineUserNumRes_8() {
        super(GwcMsgID.OnlineUserNumRes);
    }

    @Override
    public void process(ChannelHandlerContext ctx, GwcMsg msg) throws Exception {
        try {
            Gwc.getOnlineUserNumRes res = Gwc.getOnlineUserNumRes.parseFrom(msg.getBody());
            int num = res.getNum();
            log.debug("收到GWC拉取在线用户数消息, msgId = {},num={}, msg={}", msg.getCmd(), num, res.toString());

            //本游戏在线
            RoomConfigService.OBJ.setPlayers(num, 0);
        }catch (Exception ex){
            log.error("返回消息处理异常",ex);
        }
    }
}
