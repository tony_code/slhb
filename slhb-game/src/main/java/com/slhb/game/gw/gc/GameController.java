package com.slhb.game.gw.gc;

import com.slhb.core.utils.GsonUtil;
import com.slhb.core.utils.HttpsUtil;
import com.slhb.game.config.Config;
import com.slhb.game.gw.gc.model.GcSplitPoolResponse;
import com.slhb.game.gw.gc.model.GcSingleControlResponse;
import com.slhb.game.gw.gc.model.SingleControl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

@Slf4j
public class GameController {

    public static final GameController ME = new GameController();

    //游戏单控配置请求地址
    private static String gc_url_single_control = Config.GAME_GCURL + "UserControl/gamePullConfig?game_id=%s";

    //游戏分离奖池配置请求地址
    private static String gc_url_split_pool = Config.GAME_GCURL + "GameControl/subChannelPoolConfig?game_id=%s";

    /**
     * 请求game control 玩家单控游戏配置
     * @return
     */
    public List<SingleControl> getSingleControlConfig(){
        List<SingleControl> list = new ArrayList<>();
        try {
            String url = String.format(gc_url_single_control, Config.GAME_ID);
            log.info("getSingleControlConfig请求地址 URL = {}", url);
            String ret = HttpsUtil.doGet(url, false);

            if (StringUtils.isEmpty(ret)){
                log.error("getSingleControlConfig请求返回ret = null");
                return null;
            }

            //ret = unGzip(ret);
            if (StringUtils.isEmpty(ret)){
                log.error("getSingleControlConfig请求返回结果解码失败");
                return null;
            }
            log.debug("getSingleControlConfig请求返回结果解码成功 ret = {}", ret);

            //解析请求结果
            GcSingleControlResponse response = GsonUtil.getGson().fromJson(ret, GcSingleControlResponse.class);
            if (response == null || response.getCode() != 0){
                return null;
            }

            if (response.getData() != null){
                return response.getData();
            }else {
                return list;
            }
        }catch (Exception e){
            log.error("getSingleControlConfig请求失败", e);
            return null;
        }
    }

    /**
     * 请求game control 分离奖池游戏配置
     * @return
     */
    public List<String> getSplitPoolConfig(){
        List<String> list = new ArrayList<>();
        try {
            String url = String.format(gc_url_split_pool, Config.GAME_ID);
            log.info("getSplitPoolConfig请求地址 URL = {}", url);
            String ret = HttpsUtil.doGet(url, false);

            if (StringUtils.isEmpty(ret)){
                log.error("getSplitPoolConfig请求返回ret = null");
                return null;
            }

            //ret = unGzip(ret);
            if (StringUtils.isEmpty(ret)){
                log.error("getSplitPoolConfig请求返回结果解码失败");
                return null;
            }
            log.debug("getSplitPoolConfig请求返回结果解码成功 ret = {}", ret);

            //解析请求结果
            GcSplitPoolResponse response = GsonUtil.getGson().fromJson(ret, GcSplitPoolResponse.class);
            if (response == null || response.getCode() != 0){
                return null;
            }

            if (response.getData() != null){
                return response.getData();
            }else {
                return list;
            }
        }catch (Exception e){
            log.error("getSplitPoolConfig请求失败", e);
            return null;
        }
    }

    /**
     * 使用gzip进行解压缩Base64码
     */
    private static String unGzip(String result) {
        if (result == null) {
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = null;
        GZIPInputStream ginzip = null;
        byte[] compressed = null;
        String decompressed = null;
        try {
            compressed = new BASE64Decoder().decodeBuffer(result);
            in = new ByteArrayInputStream(compressed);
            ginzip = new GZIPInputStream(in);

            byte[] buffer = new byte[1024];
            int offset = -1;
            while ((offset = ginzip.read(buffer)) != -1) {
                out.write(buffer, 0, offset);
            }
            decompressed = out.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ginzip != null) {
                try {
                    ginzip.close();
                } catch (IOException e) {
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            try {
                out.close();
            } catch (IOException e) {
            }
        }
        return decompressed;
    }
}
