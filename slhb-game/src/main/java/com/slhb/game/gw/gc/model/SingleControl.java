package com.slhb.game.gw.gc.model;

import com.slhb.core.utils.GsonUtil;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Getter@Slf4j@ToString
public class SingleControl {

    private String user_id ;
    /**
     * 控真人赢的概率
     */
    private int win = 0;
    /**
     * 控真人输的概率
     */
    private int lose = 0;
    /**
     * 自然概率
     */
    private int with = 0;

    public String toJson(){
        return GsonUtil.getGson().toJson(this);
    }
}
