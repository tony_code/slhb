package com.slhb.game.gw.gc.model;

public class SingleConst {

    //0-玩家不在单控名单
    public static final int notControl = 0;

    //1-控真人赢
    public static final int win = 1;

    //2-控真人输
    public static final int lose = 2;

    //3-自然发牌
    public static final int natural = 3;

}
