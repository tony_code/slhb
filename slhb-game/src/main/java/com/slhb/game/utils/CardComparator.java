package com.slhb.game.utils;

import com.slhb.game.model.CardConstent;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.service.beans.NoticePlatformBean;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * 比牌器
 * @author
 * @since 2018/10/18 16:16
 */
@Slf4j
public class CardComparator {

    public static final CardComparator OBJ = new CardComparator();

    /**
     * 输赢
     */
    private static final int win = 1;
    private static final int lose = -1;



    /**
     * 1、满足我减去的钱不能超过我现有的
     * 2、满足我加的钱不能超过我现有的
     *
     * @param player 我
     * @param score  要减去的钱
     * @return
     */
    public double getWinMoneyJudage(PlayerInfo player, double score) {
        double tmpScore = 0;
        if (player.getPlayScoreStore() >= score) {
            tmpScore = score;
        } else {
            tmpScore = player.getPlayScoreStore();
        }
        return tmpScore;
    }



    /**
     * 比较手牌大小
     * @param comparor 发起者
     * @param target   比牌目标
     * @return
     *                  发起者 > 目标 返回 1
     *                  发起者 = 目标 返回 0
     *                  发起者 < 目标 返回 -1
     */
    public int compare(int[] comparor, int[] target){
        //获取点数
        int c_special = getCommomCardPoint(comparor);
        int t_special = getCommomCardPoint(target);

        if (c_special > t_special){
            return win;
        }

        if (c_special < t_special){
            return lose;
        }

        //都是炸弹牛
        if (c_special == CardConstent.niu_boomb){
            int c_boomb = niu_boomb_card(comparor);
            int t_boomb = niu_boomb_card(target);
            if (c_boomb > t_boomb){
                return win;
            }
            if (c_boomb < t_boomb){
                return lose;
            }
        }

        //最大牌点数
        int c_maxcard = getMaxCard(comparor);
        int t_maxcard = getMaxCard(target);
        if (c_maxcard > t_maxcard){
            return win;
        }
        if (c_maxcard < t_maxcard){
            return lose;
        }

        //最大牌花色
        int c_maxcardcolor = getMaxCardColor(comparor);
        int t_maxcardcolor = getMaxCardColor(target);
        if (c_maxcardcolor < t_maxcardcolor){
            return win;
        }else {
            return lose;
        }
    }


    /**
     * 牌型倍数
     * @param cards
     * @return  牌型倍数
     */
    public int isSpecialCard(int[] cards){
        //获取点数
        int point = getCommomCardPoint(cards);

        if (point <= CardConstent.niu_6){
            return CardConstent.mul_1;
        }

        if (point <= CardConstent.niu_9){
            return CardConstent.mul_2;
        }

        if (point == CardConstent.niu_niu){
            return CardConstent.mul_3;
        }

        return CardConstent.mul_4;
    }

    /**
     * 取得手牌点数
     * @param cards
     * @return
     */
    public int getCommomCardPoint(int[] cards){
        //参数验证
        if (cards == null || cards.length != CardConstent.HAND_CARDS){
            return CardConstent.niu_0;
        }

        //五小
        if (isNiu_5s(cards)){
            return CardConstent.niu_5s;
        }
        //炸弹
        if (isNiu_boomb(cards)){
            return CardConstent.niu_boomb;
        }
        //五花
        if (isNiu_5q(cards)){
            return CardConstent.niu_5q;
        }
        //四花
        if (isNiu_4q(cards)){
            return CardConstent.niu_4q;
        }

        return getNomalNiu(cards);
    }

    //取手牌中的最大牌点数
    private int getMaxCard(int[] cards){
        //获取点数
        List<Integer> handcards = new ArrayList<>();
        for (int card : cards){
            int tmp = card % 13;
            tmp = (tmp == 0)? 13: tmp;
            handcards.add(tmp);
        }

        //最大点数
        return handcards.stream().mapToInt(Integer::intValue).max().getAsInt();
    }

    //取手牌中的最大牌点数
    private int getMaxCardColor(int[] cards){
        int maxCardPoint = getMaxCard(cards);
        //获取点数最大的卡牌
        List<Integer> handcards = new ArrayList<>();
        for (int card : cards){
            if (card % 13 == maxCardPoint || card % 13 == 0){
                handcards.add(card);
            }
        }

        //最大花色牌 黑红梅方
        return handcards.stream().mapToInt(Integer::intValue).min().getAsInt();
    }

    //是五小牛
    private boolean isNiu_5s(int[] cards){
        if (cards == null) return false;

        int count = 0;
        for (int card : cards){
            int cardA = card % 13;
            //点数小于五
            if (cardA >= 5 || cardA == 0) return false;
            count += cardA;
        }

        return count <= 10;
    }
    //是炸弹
    private boolean isNiu_boomb(int[] cards){
        if (cards == null) return false;
        Map<Integer, Integer> map = new HashMap<>();

        for (int card : cards) {
            int value = card % 13;
            if (map.containsKey(value)) {
                map.put(value, map.get(value) + 1);
            } else {
                map.put(value, 1);
            }
        }
        for (int value : map.keySet()) {
            int count = map.get(value);
            if (count == 4) {
                return true;
            }
        }
        return false;
    }
    //五花牛
    private boolean isNiu_5q(int[] cards){
        for (int card : cards) {
            int value = card % 13;
            value = value==0 ? 13 : value;
            if (value < 11) {
                return false; //手牌全是J Q K
            }
        }
        return true;
    }
    //四花牛
    private boolean isNiu_4q(int[] cards){
        int num = 0;
        int tmpValue = 0;
        for (int card : cards) {
            int value = card % 13;
            value = value==0 ? 13 : value;
            if (value < 11) {
                tmpValue = value;
                num++; //手牌全是J Q K 且 另一张为10
            }
        }
        if (num > 1) {
            return false;
        }
        return tmpValue == 10;
    }

    private int getNomalNiu(int[] handCard) {
        List<List<Byte>> list = find(NumUtils.ConvertInt2ByteArr(handCard));
        for (List<Byte> l : list) {
            int sum = 0;
            for (Byte b : l) {
                int value = getCardScoreValue(b);
                sum += value;
            }

            if (sum % 10 == 0) {
                //有牛
                List<Byte> temp = NumUtils.ConvertInt2ByteArr(handCard);
                for (Byte b : l) {
                    temp.remove(b);
                }
                int ct = 0;
                for (Byte b : temp) {
                    int value = getCardScoreValue(b);
                    ct += value;
                }
                int result = ct % 10;
                switch (result) {
                    case 0:
                        return CardConstent.niu_niu;
                    case 1:
                        return CardConstent.niu_1;
                    case 2:
                        return CardConstent.niu_2;
                    case 3:
                        return CardConstent.niu_3;
                    case 4:
                        return CardConstent.niu_4;
                    case 5:
                        return CardConstent.niu_5;
                    case 6:
                        return CardConstent.niu_6;
                    case 7:
                        return CardConstent.niu_7;
                    case 8:
                        return CardConstent.niu_8;
                    case 9:
                        return CardConstent.niu_9;
                    default:
                        return CardConstent.niu_0;
                }
            }
        }
        return CardConstent.niu_0;
    }

    private int getCardScoreValue(int card){
        int cardA = card % 13;
        return cardA > 10 ? 10 : cardA;
    }

    /***
     * 从一组五张牌中，寻找出所有三张牌的集合
     * @param list
     * @return
     */
    private List<List<Byte>> find(List<Byte> list) {
        if (list.size() != 5) { //5张牌寻三张
            return null;
        }
        List<List<Byte>> all = new ArrayList<>();
        for (Byte b : list) {
            FindNode node = new FindNode();
            node.setFindCard(b);
            List<Byte> remain = new ArrayList<>(list);
            remain.remove(b);
            node.setRemain(remain);
            findOnce(node, all);
        }
        //过滤算法
        List<List<Byte>> find = new ArrayList<>();
        for (List<Byte> l : all) {
            if (find.size() == 0) {
                find.add(l);
            } else {
                if (!containList(find, l)) {
                    find.add(l);
                }
            }
        }
        return find;
    }

    private void findOnce(FindNode parent, List<List<Byte>> all) {
        if (parent.getRemain().size() <= 2) { //数量小于2，不用再寻找了
            List<Byte> findList = new ArrayList<>();
            findList.add(parent.getFindCard());
            while (parent.getParent() != null) {
                parent = parent.getParent();
                findList.add(parent.getFindCard());
            }
            all.add(findList);
            return;
        }
        List<Byte> list = parent.getRemain();
        for (Byte b : list) {
            FindNode fn = new FindNode();
            fn.setFindCard(b);
            List<Byte> remain = new ArrayList<>(list);
            remain.remove(b);
            fn.setRemain(remain);
            fn.setParent(parent);
            findOnce(fn, all);
        }
    }

    private boolean containList(List<List<Byte>> list, List<Byte> check) {
        for (List<Byte> l : list) {
            if (sameList(l, check)) {
                return true;
            }
        }
        return false;
    }

    private boolean sameList(List<Byte> l1, List<Byte> l2) {
        if (l1.size() != l2.size()) {
            return false;
        }
        Collections.sort(l1);
        Collections.sort(l2);
        for (int i = 0; i < l1.size(); i++) {
            if (l1.get(i) != l2.get(i)) {
                return false;
            }
        }
        return true;
    }

    //是炸弹
    private int niu_boomb_card(int[] cards){
        Map<Integer, Integer> map = new HashMap<>();

        for (int card : cards) {
            int value = card % 13;
            if (map.containsKey(value)) {
                map.put(value, map.get(value) + 1);
            } else {
                map.put(value, 1);
            }
        }
        for (int value : map.keySet()) {
            int count = map.get(value);
            if (count == 4) {
                return value;
            }
        }
        return 0;
    }
}
