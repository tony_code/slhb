package com.slhb.game;

import com.slhb.core.jedis.JedisFactory;
import com.slhb.core.utils.xml.LogConfigUtils;
import com.slhb.base.platform.HallAPIService;
import com.slhb.game.api.RestfulServer;
import com.slhb.game.api.service.CheckGameService;
import com.slhb.game.config.JsonConfig;
import com.slhb.game.config.NameConfig;
import com.slhb.game.gate.GateServer;
import com.slhb.game.gw.GwcNettyServer;
import com.slhb.game.room.RoomServer;
import com.slhb.game.config.Config;
import com.slhb.game.service.*;
import lombok.extern.slf4j.Slf4j;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class Main {

    public static void main(String[] args) {
        try {
            //配置日志
            LogConfigUtils.initLogConfig();

            //加载游戏配置
            Config.load("all");

            //jedis 消息
            JedisFactory.loadJedis();

            //初始化json配置
            JsonConfig.ME.init();

            //初始化名称库
            NameConfig.ME.init();

            TimerService.getInstance();

            //大厅注册
            HallAPIService.OBJ.bindAccount(Config.GAME_ACCOUNTURL);

            GwcNettyServer.OBJ.bind(Config.BIND_IP, Config.GAME_GWCURL).start();

            //大厅接口注册
            RestfulServer.OBJ.start();

            /**热更新配置*/
            loadDBConfig();

            //Gate 服务启动
            GateServer.OBJ.start();

            //room 服务启动
            RoomServer.OBJ.start();
            CheckGameService.OBJ.init();

            TestService.OBJ.test();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {

        }
    }

    /**
     * 每天某个整点加载一次
     * 修正配置信息
     */
    private static void loadDBConfig() {
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(
                ()-> Config.load("all"),
                0, Config.loadDBConfigHour, TimeUnit.HOURS);
    }
}
