package com.slhb.game.room.service.thread;

import com.slhb.game.model.PlayerInfo;
import com.slhb.game.room.service.PlayerQueueService;
import lombok.extern.slf4j.Slf4j;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 至尊场线程
 * @since 2019/6/11 18:04
 */
@Slf4j
public class ChechThreadSupre extends AbstractOpers implements Runnable{

    public static final String roomId = "4";

    @Override
    public void run() {
        try {
            Map<String, ConcurrentLinkedQueue<PlayerInfo>> channelMap = PlayerQueueService.OBJ.get_channelMap_4();
            //无人加入牌桌队列
            if (channelMap == null || channelMap.size() == 0){
                return;
            }

            for (String channel : channelMap.keySet()){
                ConcurrentLinkedQueue<PlayerInfo> queue = channelMap.get(channel);

                if (queue == null || queue.isEmpty()){
                    continue;
                }

                while (!queue.isEmpty()){
                    createTableAndPlay(queue, roomId, channel);
                }
            }
        }catch (Exception ex){
            log.error("CheckChannelThread ERROR, msg = " + ex.getMessage(), ex);
        }
    }

}
