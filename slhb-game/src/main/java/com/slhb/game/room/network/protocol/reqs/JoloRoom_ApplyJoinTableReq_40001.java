package com.slhb.game.room.network.protocol.reqs;

import JoloProtobuf.RoomSvr.JoloRoom;
import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.ErrorCodeEnum;
import com.slhb.base.model.GameRoomTableSeatRelationModel;
import com.slhb.game.config.Config;
import com.slhb.game.dao.bean.CommonConfigModel;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.room.cache.UserTableSet;
import com.slhb.game.service.GameMaintentService;
import com.slhb.game.service.PlayerService;
import com.slhb.game.service.holder.CommonConfigHolder;
import com.slhb.game.service.holder.RoomConfigHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;


/**
 * 申请入桌(换桌)
 */
@Slf4j
public class JoloRoom_ApplyJoinTableReq_40001 extends Req {

    private JoloRoom.JoloRoom_ApplyJoinTableReq req;

    public JoloRoom_ApplyJoinTableReq_40001(int functionId) {
        super(functionId);
    }

    @Override
    public void readPayLoadImpl(ByteBuf byteBuf) throws Exception {
        byte[] playLoad = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(playLoad);
        req = JoloRoom.JoloRoom_ApplyJoinTableReq.parseFrom(playLoad);
    }

    @Override
    public void processImpl() {
        JoloRoom.JoloRoom_ApplyJoinTableAck.Builder ack = JoloRoom.JoloRoom_ApplyJoinTableAck.newBuilder();
        try {
            //初始化默认返回值
            this.initDefaultReturnValue(ack);

            log.debug("收到消息-> " + functionId + " reqNum-> " + reqHeader.reqNum + " " + req.toString());
            String userId = this.userId;
            String gameId = req.getGameId();
            String roomId = req.getRoomId();
            String tableId = "";

            //首先查看自己有没有在游戏内
            GameRoomTableSeatRelationModel gameRoomTableSeatRelationModel = UserTableSet.OBJ.get(userId);

            if (GameMaintentService.OBJ.isDefense()){
                log.error("游戏进入维护状态，无法入桌");
                ack.setResult(-1).setResultMsg("游戏进入维护状态,无法入桌");
                sendResponse(GateFunctionFactory.__function__id_40001 | 0x08000000, ack.build().toByteArray());
                return;
            }

            //用户信息：从缓存获取
            User user = PlayerService.getInstance().getUser(userId);
            if (user == null) {
                log.info("缓存中没有user信息");
                return;
            }
            ack.setResult(1).setUserId(userId).setGameId(gameId).setRoomId(roomId);

            //如果参数中没有指定roomId，那么由系统自动分配一个房间(分配规则以入场积分限制为准)
            double currScoreStore = user.getMoney(); //玩家当前积分库存
            if (gameRoomTableSeatRelationModel == null && !RoomConfigHolder.getInstance().canFindSuuitableRoom(currScoreStore)) {
                ack.setTableId("")
                        .setSeatId("")
                        .setJoinGameSvrId("")
                        .setBootAmount(0)
                        .setResult(-1).setResultMsg(ErrorCodeEnum.ROOM_40001_1.getCode());
                sendResponse(GateFunctionFactory.__function__id_40001 | 0x08000000, ack.build().toByteArray());
                return;
            }

            RoomConfigModel roomConfig;
            CommonConfigModel commonConfig;
            if (gameRoomTableSeatRelationModel == null) {
                roomConfig = RoomConfigHolder.getInstance().getRoomConfig(roomId);
                commonConfig = CommonConfigHolder.getInstance().getCommonConfig();
            } else {
                roomConfig = RoomConfigHolder.getInstance().getRoomConfig(gameRoomTableSeatRelationModel.getRoomId());
                commonConfig = CommonConfigHolder.getInstance().getCommonConfig();
            }
            if (null == roomConfig || commonConfig == null) {
                ack.setTableId("")
                        .setSeatId("")
                        .setJoinGameSvrId("")
                        .setBootAmount(0)
                        .setResult(-1).setResultMsg(ErrorCodeEnum.ROOM_40001_2.getCode());
                log.error("can't found suitable Room. userScoreStore->" + currScoreStore);
                sendResponse(GateFunctionFactory.__function__id_40001 | 0x08000000, ack.build().toByteArray());
                return;
            }
            roomId = roomConfig.getRoomId();
            if (gameRoomTableSeatRelationModel != null) {
                log.info("gameRoomTableSeatRelationModel:" + gameRoomTableSeatRelationModel.toString());
                if ( gameRoomTableSeatRelationModel.getSeat() > 0) {//在座位上
                    ack.setSeatId(gameRoomTableSeatRelationModel.getSeat() + "");
                    ack.setTableId(gameRoomTableSeatRelationModel.getTableId());
                    ack.setReconnection(1);
                }
            } else {
                ack.setReconnection(0);
            }

            ack.setTableId(tableId);
            if (!ack.hasSeatId()) {
                ack.setSeatId("");
            }
            if (gameRoomTableSeatRelationModel != null && !roomId.equals(gameRoomTableSeatRelationModel.getRoomId())) {
                //TODO 需前端一起配合做跳转提示
                ack.setResult(1);
                ack.setResultMsg("返回原来所在桌");
            }
            ack.setBootAmount(roomConfig.getMinEnv());
            ack.setBetCd(1);
            ack.setGameStartCd(1);
            ack.setMinJoinTableScore(1);
            ack.setJoinGameSvrId(Config.GAME_SERID);//没有时前端提示 "服务器爆满"

            //缓存玩法
            sendResponse(GateFunctionFactory.__function__id_40001 | 0x08000000, ack.build().toByteArray());
            log.info("ApplyJoinTableReq is finished, player = {}",userId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 初始化默认返回值
     * @param ack
     */
    private void initDefaultReturnValue(JoloRoom.JoloRoom_ApplyJoinTableAck.Builder ack){
        ack.setUserId(req.getUserId());
        ack.setGameId(req.getGameId());
        ack.setRoomId(req.getRoomId());
        ack.setTableId("");
        ack.setSeatId("");
        ack.setResult(1);
        ack.setJoinGameSvrId("");
        ack.setBootAmount(0);
        ack.setBetCd(0);
        ack.setGameStartCd(0);
        ack.setMinJoinTableScore(0);
        ack.setReconnection(0);
    }
}
