package com.slhb.game.room.network.protocol.acks;

import com.slhb.game.gate.network.protocol.Ack;

/**
 * 申请换桌
 */
public class JoloRoom_ApplyChangeTableAck_40002 extends Ack {
    /**
     * @param messageLite
     * @param header
     */
    public JoloRoom_ApplyChangeTableAck_40002(int functionId) {
        super(functionId);
    }
}
