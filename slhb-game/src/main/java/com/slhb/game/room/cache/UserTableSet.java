package com.slhb.game.room.cache;

import com.slhb.base.model.GameRoomTableSeatRelationModel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserTableSet {

    public static final UserTableSet OBJ = new UserTableSet();

    private Map<String ,GameRoomTableSeatRelationModel> _cache = new ConcurrentHashMap<>();

    public void add(String userId , GameRoomTableSeatRelationModel model){
        _cache.put(userId,model);
    }

    public GameRoomTableSeatRelationModel get(String userId){
        return _cache.get(userId);
    }

    public void remove(String userId){
        if (!_cache.containsKey(userId)){
            return;
        }

        _cache.remove(userId);
    }
}
