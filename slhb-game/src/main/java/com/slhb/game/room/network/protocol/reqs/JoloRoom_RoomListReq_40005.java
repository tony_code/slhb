package com.slhb.game.room.network.protocol.reqs;

import JoloProtobuf.RoomSvr.JoloRoom;
import com.slhb.core.jedis.StoredObjManager;
import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.RedisConst;
import com.slhb.base.platform.HallAPIService;
import com.slhb.base.platform.bean.PlatUserBean;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.gate.service.UserService;
import com.slhb.game.room.service.RoomConfigService;
import com.slhb.game.service.NoticePlatformSerivce;
import com.slhb.game.service.PlayerService;
import com.slhb.game.utils.NumUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

/**
 * 请求展示房间列表
 *
 * @author
 *
 * @since 2018/9/11 17:03
 */
@Slf4j
public class JoloRoom_RoomListReq_40005 extends Req {

    private JoloRoom.JoloRoom_GetRoomListReq req;

    public JoloRoom_RoomListReq_40005(int functionId) {
        super(functionId);
    }

    @Override
    public void readPayLoadImpl(ByteBuf buf) throws Exception {
        byte[] blob = new byte[buf.readableBytes()];
        buf.readBytes(blob);
        req =  JoloRoom.JoloRoom_GetRoomListReq.parseFrom(blob);
    }

    @Override
    public void processImpl() throws Exception {
        log.info("房间列表请求={}",req.toString());
        JoloRoom.JoloRoom_GetRoomListAck.Builder ack = JoloRoom.JoloRoom_GetRoomListAck.newBuilder();

        log.debug("method=updateMoney, user= {}",ack.getUserId());
        User user = PlayerService.getInstance().getUser(userId);
        if (user == null){
            log.error("user = null" );
            return;
        }

        ack.setResult(1)
                .setUserId(req.getUserId())
                .setGameId(req.getGameId())
                .setIsGetTableList(1)
                .addAllRoomList(RoomConfigService.OBJ.getRoomConfigs(user.getChannel_id()));

        sendResponse(GateFunctionFactory.__function__id_40005 | 0x08000000, ack.build().toByteArray());
        if ( user.getChannel_id().equals("robot")){
            log.debug("method=updateMoney, user= {},channel={}",this.userId,user.getChannel_id());
            return;
        }
        PlatUserBean userBean = HallAPIService.OBJ.getById(user.getAndroid_id());
        if (userBean == null){
            log.debug("method=updateMoney, user= {},userBean=null",ack.getUserId());
            return;
        }
        double curMoney = NumUtils.double2Decimal(userBean.getGold() );
        if (user != null) {
            user.setMoney(curMoney);
            PlayerService.getInstance().saveUser(user);
        }
        UserService.getInstance().sendPayNoticeMsg(user.getId() ,curMoney);
        log.debug("method=updateMoney, user= {}, money = {}",user.getId(), curMoney);
    }
}
