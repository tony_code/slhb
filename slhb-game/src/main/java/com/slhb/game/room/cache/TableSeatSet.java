package com.slhb.game.room.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TableSeatSet {

    public static final TableSeatSet OBJ = new TableSeatSet();

    //room, table, userId, seat
    private Map<String,Map<String,Map<String,Integer>>> _seatCache = new ConcurrentHashMap<>();

    public void add(String roomId, String tableId, int seatNum, String userId){
         if (!_seatCache.containsKey(roomId)){
             _seatCache.put(roomId, new ConcurrentHashMap<>());
         }

        Map<String,Map<String,Integer>> tableMap = _seatCache.get(roomId);
        if (!tableMap.containsKey(tableId)){
            tableMap.put(tableId, new ConcurrentHashMap<>());
        }

        Map<String,Integer> userMap = tableMap.get(tableId);
        userMap.put(userId, seatNum);
    }

    public void remove(String roomId, String tableId, String userId){
        if (!_seatCache.containsKey(roomId)){
            _seatCache.put(roomId, new ConcurrentHashMap<>());
        }

        Map<String,Map<String,Integer>> tableMap = _seatCache.get(roomId);
        if (!tableMap.containsKey(tableId)){
            tableMap.put(tableId, new ConcurrentHashMap<>());
        }

        Map<String,Integer> userMap = tableMap.get(tableId);
        if (userMap.containsKey(userId)){
            userMap.remove(userId);
        }
    }

}
