package com.slhb.game.room.network.protocol.reqs;

import JoloProtobuf.RoomSvr.JoloRoom;
import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.*;
import com.slhb.base.model.RoomTableRelationModel;
import com.slhb.game.config.Config;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.gate.network.GateFunctionFactory;
import com.slhb.game.gate.network.protocol.Req;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.room.cache.RoomTableSet;
import com.slhb.game.service.PlayerService;
import com.slhb.game.service.TableService;
import com.slhb.game.service.holder.RoomConfigHolder;
import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JoloRoom_ApplyJoinTableRobotReq_40003 extends Req {
    private final static Logger logger = LoggerFactory.getLogger(JoloRoom_ApplyJoinTableRobotReq_40003.class);

    private JoloRoom.JoloRoom_ApplyJoinTableRobotReq req;

    public JoloRoom_ApplyJoinTableRobotReq_40003(int functionId) {
        super(functionId);
    }

    @Override
    public void readPayLoadImpl(ByteBuf byteBuf) throws Exception {
        byte[] playLoad = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(playLoad);
        req = JoloRoom.JoloRoom_ApplyJoinTableRobotReq.parseFrom(playLoad);
    }

    @Override
    public void processImpl() throws Exception {
        logger.debug("收到消息-> " + functionId + " reqNum-> " + reqHeader.reqNum + " " + req.toString());
        JoloRoom.JoloRoom_ApplyJoinTableAck.Builder ack = JoloRoom.JoloRoom_ApplyJoinTableAck.newBuilder();

        try {
            String userId = req.getUserId();
            this.userId = userId;
            String gameId = req.getGameId();
            String roomId = req.getRoomId();
            String tableId = req.getTableId();
            ack.setUserId(userId).setGameId(gameId).setRoomId(roomId).setTableId(tableId).setSeatId("");
            RoomTableRelationModel ret = RoomTableSet.OBJ.get(roomId, tableId);
            if (ret == null) {
                logger.error("JoloRoom_ApplyJoinTableRobotReq_40003 找不到指定的桌子。room:" + roomId + ",tableId:" + tableId);
                sendResponse(GateFunctionFactory.__function__id_40001 | 0x08000000, ack.setResult(0).setResultMsg(ErrorCodeEnum.ROOM_40001_1.getCode()).build().toByteArray());
                return;
            }
            if (ret.getTableStatus() > TableStateEnum.IDEL.getValue()) {
                ack.setResult(-2);
                ack.setResultMsg(ErrorCodeEnum.ROOM_40001_1.getCode());
                sendResponse(GateFunctionFactory.__function__id_40001 | 0x08000000, ack.build().toByteArray());
                return;
            }

            //通过roomId和tableId 查找对就的桌子
            AbstractTable table = TableService.getInstance().getTable(reqHeader.gameId + "", roomId, tableId);
            //如果内存没有 去缓存看看
            if (table == null) {

                //table = TableService.getInstance().addExitTable("" + reqHeader.gameId, ret.getRoomId(), ret.getTableId());
            }
            //用户信息：现在从数据库获取，以后可优化成从缓存获取
            User user = PlayerService.getInstance().getUser(userId);
            if (user == null) {
                logger.error("can't found user uiserId->" + userId);
                return;
            }
            if (!user.getChannel_id().equals(RoleType.ROBOT.getTypeName())) {
                logger.error("this account is not robot，userId " + userId);
                return;
            }

            //断线重连如果有座位信息需要在这里设置上
            //从缓存里查找信息ID
            ack.setSeatId("");
            ack.setResult(1);
            //如果参数中没有指定roomId，那么由系统自动分配一个房间(分配规则以入场积分限制为准)
            //暂时不用做任何判断 未来根据需求改
            //创建player对象
            PlayerInfo player = new PlayerInfo(roomId, tableId, user.getId(), user.getNick_name(), user.getIco_url(), RoleType.getRoleType(user.getChannel_id()), user);
            //将玩家加入到table信息中（完成玩家的入桌状态）
            table.joinRoom(player);
            RoomConfigModel roomConfig = RoomConfigHolder.getInstance().getRoomConfig(roomId);
            if (roomConfig == null) {
                ack.setResult(0);
                ack.setResultMsg(ErrorCodeEnum.ROOM_40001_1.getCode());
                sendResponse(GateFunctionFactory.__function__id_40001 | 0x08000000, ack.build().toByteArray());
                return;
            }

            ack.setBootAmount(roomConfig.getMinEnv());
            ack.setJoinGameSvrId(Config.GAME_SERID);//没有时前端提示 "服务器爆满"

            sendResponse(GateFunctionFactory.__function__id_40001 | 0x08000000, ack.build().toByteArray());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
