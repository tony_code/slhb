package com.slhb.game.room.service;

import com.google.common.base.Strings;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.service.TableService;
import com.slhb.game.service.holder.CommonConfigHolder;
import com.slhb.game.service.holder.RoomConfigHolder;
import lombok.extern.slf4j.Slf4j;

/**
 * room操作类
 * @author
 * @since 2018/12/4 14:54
 */
@Slf4j
public class RoomOprService {

    public static final RoomOprService OBJ = new RoomOprService();

    //站起
    public void standupHandler(String gameId, String roomId, String tableId){
        if (Strings.isNullOrEmpty(gameId) || Strings.isNullOrEmpty(roomId) || Strings.isNullOrEmpty(tableId)) {
            return;
        }
        //TableService
        TableService.getInstance().addRoomCanJoinTable(gameId, roomId, tableId);
    }

    //GM刷新
    public void refreshConfigHandler(int channel){
        switch(channel){
            case 2:
                CommonConfigHolder.getInstance().init();
                break;
            case 3:
                RoomConfigHolder.getInstance().init();
                TableService.getInstance().init();
                break;
            default:
                break;
        }
    }

    //离桌
    public void leaveTableHandler(String gameId, String roomId, String tableId, String userId){
        if (Strings.isNullOrEmpty(gameId) || Strings.isNullOrEmpty(roomId) ||
                Strings.isNullOrEmpty(tableId) || Strings.isNullOrEmpty(userId)) {
            return;
        }
        AbstractTable tableInfo = TableService.getInstance().getTable(gameId, roomId, tableId);
        if (tableInfo != null) {
            tableInfo.returnLobby(userId);
        }
        TableService.getInstance().destroyTable(gameId, roomId, tableId);
    }
}
