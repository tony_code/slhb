package com.slhb.game.room;

import com.slhb.core.service.CronTaskManager;
import com.slhb.game.room.service.RoomConfigService;
import com.slhb.game.room.service.thread.*;
import com.slhb.game.service.RobotStrategyService;
import com.slhb.game.service.SingleControlService;
import com.slhb.game.service.SplitPoolService;
import com.slhb.game.service.TableService;
import com.slhb.game.service.holder.CommonConfigHolder;
import com.slhb.game.service.holder.RoomConfigHolder;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;

/**
 *  room服務
 * @author
 * @since 2018/12/4 11:47
 */
@Slf4j
public class RoomServer {

    public static final RoomServer OBJ = new RoomServer();

    public void start(){
        try {
            log.info("Room服务启动");
            RoomConfigHolder.getInstance().loadData();

            /**加载配置*/
            CommonConfigHolder.getInstance().loadData();

            RoomConfigService.OBJ.init();

            TableService.getInstance().init();

            RobotStrategyService.OBJ.init();
            //单控初始化
            SingleControlService.OBJ.init();
            //独立奖池初始化
            SplitPoolService.OBJ.init();

            /**启动心跳线程*/
            //初级场线程启动
            Consumer<Object> call1 = obj -> {
                    TableThreadPoolManager.getInstance().addTask(new ChechThreadCommon());
            };
            CronTaskManager.CountDownTask task1 = new CronTaskManager.CountDownTask(System.currentTimeMillis(), call1);
            CronTaskManager.getInstance().addCountDownJob(task1.getName(), "ChechThreadCommon", task1, "0/2 * * * * ?");

            //中级场线程启动
            Consumer<Object> call2 = obj -> {
                TableThreadPoolManager.getInstance().addTask(new ChechThreadMiddle());
            };
            CronTaskManager.CountDownTask task2 = new CronTaskManager.CountDownTask(System.currentTimeMillis(), call2);
            CronTaskManager.getInstance().addCountDownJob(task2.getName(), "ChechThreadMiddle", task2, "0/2 * * * * ?");

            //高级场线程启动
            Consumer<Object> call3 = obj -> {
                TableThreadPoolManager.getInstance().addTask(new ChechThreadHigh());
            };
            CronTaskManager.CountDownTask task3 = new CronTaskManager.CountDownTask(System.currentTimeMillis(), call3);
            CronTaskManager.getInstance().addCountDownJob(task3.getName(), "ChechThreadHigh", task3, "0/2 * * * * ?");

            //至尊场线程启动
            Consumer<Object> call4 = obj -> {
                TableThreadPoolManager.getInstance().addTask(new ChechThreadSupre());
            };
            CronTaskManager.CountDownTask task4 = new CronTaskManager.CountDownTask(System.currentTimeMillis(), call4);
            CronTaskManager.getInstance().addCountDownJob(task4.getName(), "ChechThreadSupre", task4, "0/2 * * * * ?");

        }catch (Exception ex){
            throw new Error("room 服务启动异常",ex);
        }
    }

}
