package com.slhb.game.room.cache;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class TableUserSet {

    public static final TableUserSet OBJ = new TableUserSet();

    private Map<String, Map<String,String>> _playerStateMap = new ConcurrentHashMap<>();

    public void add (String roomtable, String userId, String playerState){
        if (!_playerStateMap.containsKey(roomtable)){
            _playerStateMap.put(roomtable, new ConcurrentHashMap<>());
        }

        Map<String,String> stateMap = _playerStateMap.get(roomtable);
        stateMap.put(userId, playerState);
    }

    public void remove (String roomtable, String userId){
        if (!_playerStateMap.containsKey(roomtable)){
            _playerStateMap.put(roomtable, new ConcurrentHashMap<>());
        }

        Map<String,String> stateMap = _playerStateMap.get(roomtable);
        if (stateMap.containsKey(userId)){
            stateMap.remove(userId);
        }
    }

    public String get(String roomtable, String userId){
        if (!_playerStateMap.containsKey(roomtable)){
            _playerStateMap.put(roomtable, new ConcurrentHashMap<>());
        }

        Map<String,String> stateMap = _playerStateMap.get(roomtable);
        if (stateMap.containsKey(userId)){
            return stateMap.get(userId);
        }
        return null;
    }

    public Set<String> getUsers(String roomtable ){
        if (!_playerStateMap.containsKey(roomtable)){
            _playerStateMap.put(roomtable, new ConcurrentHashMap<>());
        }

        Map<String,String> stateMap = _playerStateMap.get(roomtable);
        Set<String> list = new HashSet<>();
        stateMap.forEach((k,v)->{
            list.add(k);
        });
        return list;
    }

}
