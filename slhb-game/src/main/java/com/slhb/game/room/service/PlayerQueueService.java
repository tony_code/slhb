package com.slhb.game.room.service;

import com.slhb.game.model.PlayerInfo;
import io.netty.util.internal.ConcurrentSet;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 玩家排队入座
 * @since 2019/6/11 16:31
 */

@Slf4j@Getter
public class PlayerQueueService {

    public static final PlayerQueueService OBJ = new PlayerQueueService();

    //分开设计
    //初级房 <渠道，队列>>
    private Map<String, ConcurrentLinkedQueue<PlayerInfo>> _channelMap_1 = new ConcurrentHashMap();
    //中级房
    private Map<String, ConcurrentLinkedQueue<PlayerInfo>> _channelMap_2 = new ConcurrentHashMap();
    //高级房
    private Map<String, ConcurrentLinkedQueue<PlayerInfo>> _channelMap_3 = new ConcurrentHashMap();
    //至尊房
    private Map<String, ConcurrentLinkedQueue<PlayerInfo>> _channelMap_4 = new ConcurrentHashMap();

    //队列中的玩家
    private Set<String> _inQueueIds = new ConcurrentSet<>();


    private Map<String, ConcurrentLinkedQueue<PlayerInfo>> getChannelMap(String room){
        switch (room){
            case "1": return _channelMap_1;
            case "2": return _channelMap_2;
            case "3": return _channelMap_3;
            case "4": return _channelMap_4;
            default:
                log.error("未知房间id={}",room);
                return null;
        }
    }

    /**
     * 入队
     * @param channel
     * @param player
     */
    public void addPlayer(String channel ,String room ,PlayerInfo player){
        if (StringUtils.isEmpty(channel)|| StringUtils.isEmpty(room) || player == null){
            log.error("channel = {}, room = {} player = {}", channel, room, player);
            return;
        }

        if (_inQueueIds.contains(player.getPlayerId())){
            log.error("玩家已在队列中，请勿重复排队");
            return;
        }

        Map<String, ConcurrentLinkedQueue<PlayerInfo>> map = getChannelMap(room);
        if (map == null){
            map = new ConcurrentHashMap<>();
        }

        ConcurrentLinkedQueue<PlayerInfo> queue = map.get(channel);
        if (queue == null){
            queue = new ConcurrentLinkedQueue<>();
        }

        //队列中插入一个元素
        queue.add(player);
        map.put(channel,queue);
    }

    /**
     * 指定数量玩家出队
     * @param channel
     * @param players
     * @return
     */
    public List<PlayerInfo> getPlayers(String channel, String room ,int players){
        List<PlayerInfo> list = new ArrayList<>();

        Map<String, ConcurrentLinkedQueue<PlayerInfo>> map = getChannelMap(room);
        if (map == null || map.size() == 0){
            return list;
        }

        ConcurrentLinkedQueue<PlayerInfo> queue = map.get(channel);
        //队列为空
        if (queue == null || queue.isEmpty()){
            return list;
        }

        //取n个玩家
        for (int i=0; i<players; i++){
            PlayerInfo p = queue.poll();
            //队列里去取玩家
            if (p == null){
                continue;
            }

            _inQueueIds.remove(p.getPlayerId());

            list.add(p);
        }
        return list;
    }

    //是否有人
    public boolean isEmpty(String channel, String room){
        Map<String, ConcurrentLinkedQueue<PlayerInfo>> map = getChannelMap(room);
        if (map == null || map.size() == 0){
            //队列为空
            return true;
        }

        ConcurrentLinkedQueue<PlayerInfo> queue = map.get(channel);
        if (queue == null || queue.isEmpty()){
            //队列为空
            return true;
        }

        return false;
    }

}
