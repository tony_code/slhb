package com.slhb.game.room.cache;


import com.slhb.base.model.RoomTableRelationModel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RoomTableSet {

    public static final RoomTableSet OBJ = new RoomTableSet();

    //room, table, model
    private Map<String,Map<String,RoomTableRelationModel>> _roomTableMap = new ConcurrentHashMap<>();

    public void add(String roomId, String tableId,RoomTableRelationModel model){
        if (!_roomTableMap.containsKey(roomId)){
            _roomTableMap.put(roomId, new ConcurrentHashMap<>());
        }

        Map<String,RoomTableRelationModel> tableMap = _roomTableMap.get(roomId);
        tableMap.put(tableId, model);
    }

    public boolean remove(String roomId, String tableId ){
        if (!_roomTableMap.containsKey(roomId)){
            _roomTableMap.put(roomId, new ConcurrentHashMap<>());
        }

        Map<String,RoomTableRelationModel> tableMap = _roomTableMap.get(roomId);
        if (tableMap.containsKey(tableId)){
            tableMap.remove(tableId);
        }

        return true;
    }

    public Map<String,RoomTableRelationModel> get(String roomId){
        if (_roomTableMap.containsKey(roomId)){
            return _roomTableMap.get(roomId);
        }
        return null;
    }

    public RoomTableRelationModel get(String roomId, String tableId){
        if (!_roomTableMap.containsKey(roomId)){
            _roomTableMap.put(roomId, new ConcurrentHashMap<>());
        }

        Map<String,RoomTableRelationModel> tableMap = _roomTableMap.get(roomId);
        if (tableMap.containsKey(tableId)){
            return tableMap.get(tableId);
        }
        return null;
    }
}
