package com.slhb.game.room.service.thread;

import com.slhb.base.dao.bean.User;
import com.slhb.base.enums.PlayerStateEnum;
import com.slhb.base.enums.RoleType;
import com.slhb.game.config.Config;
import com.slhb.game.config.NameConfig;
import com.slhb.game.dao.bean.CommonConfigModel;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.gameUtil.GameOrderIdGenerator;
import com.slhb.game.model.PlayerInfo;
import com.slhb.game.play.AbstractTable;
import com.slhb.game.robot.Robot;
import com.slhb.game.robot.service.RobotDictionary;
import com.slhb.game.robot.utils.RandomTools;
import com.slhb.game.room.cache.TableUserSet;
import com.slhb.game.room.service.PlayerQueueService;
import com.slhb.game.service.PlayerService;
import com.slhb.game.service.TableService;
import com.slhb.game.service.holder.CommonConfigHolder;
import com.slhb.game.service.holder.RoomConfigHolder;
import com.slhb.game.utils.IdWorker;
import com.slhb.game.utils.NumUtils;
import com.slhb.game.vavle.notice.NoticeBroadcastMessages;
import lombok.extern.slf4j.Slf4j;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 *
 * @since 2019/6/11 18:21
 */
@Slf4j
public class AbstractOpers {

    protected int max_players = 4;

    //2个人的概率
    private static int pec_2 = 20 ;
    private static int pec_3 = 30 ;
    private static int pec_4 = 50 ;

    public AbstractOpers(){
        loadTableUserConfig();
    }

    protected int getCurRoundPlayers(){
        int rate = RandomTools.getRandomInt(100);

        if (rate <= pec_2) return 2;

        if (rate <= pec_3) return 3;
        return max_players;
    }

    /**
     * 创建一个机器人
     * @param roomId
     * @return
     */
    protected User makUser(String roomId){

        String id = String.valueOf(IdWorker.getInstance().nextId());
        log.debug("虚拟人物创建成功userId={}", id);

        int ico = RandomTools.getRandomInt(14) + 1;

        RoomConfigModel roomConfig = RoomConfigHolder.getInstance().getRoomConfig(roomId);
        long minScore4JoinTable = roomConfig.getMaxEnv();
        double randomDouble = RandomTools.getRandomDouble(3.1,30.1);
        double buyInScore =  NumUtils.double1Decimal(randomDouble * minScore4JoinTable/100)*100;

        User user = new User();
        user.setId(id);
        user.setNick_name(NameConfig.ME.nextName());
        user.setChannel_id("robot");
        user.setIco_url(String.valueOf(ico));
        user.setUser_defined_head(String.valueOf(ico));
        user.setMoney(buyInScore);

        log.debug("虚拟人物创建成功user={}",user.toString());
        return user;
    }

    /**
     * 创桌开局
     * @param queue
     * @param roomId
     */
    protected void createTableAndPlay(ConcurrentLinkedQueue<PlayerInfo> queue, String roomId, String channelId){
        //确定本桌人数
        int curRoundPlayers = getCurRoundPlayers();
        List<PlayerInfo> lists = new ArrayList<>();

        //取固定真人
        int realplayer = curRoundPlayers - 1;
        if (!Config.ROOM_ASSIGNED_TOGETHER){
            realplayer = 1;
        }
        for (int i=0; i<realplayer ;i++){
            PlayerInfo p = queue.poll();
            if (p == null){
                continue;
            }

            User user = PlayerService.getInstance().getUser(p.getPlayerId());
            if (user == null) {
                log.error("玩家user = {},内存信息不存在，不开局", p.getPlayerId());
                continue;
            }

            PlayerQueueService.OBJ.get_inQueueIds().remove(p.getPlayerId());

            lists.add(p);
        }

        //没有真人，那么退出
        if (lists.isEmpty()){
            return;
        }

        //创建新桌
        AbstractTable table = TableService.getInstance().createNewTable(String.valueOf(Config.GAME_ID), roomId, true);

        //取固定机器人
        int robotPlayer = 10;
        log.debug("虚拟人物创建成功users={}", robotPlayer);
        for (int i=0; i<robotPlayer ;i++){
            User user = makUser(table.getRoomId());
            RobotDictionary.ME.createRobot(user);

            PlayerInfo robots = new PlayerInfo(table, user);
            if (robots == null){
                continue;
            }

            lists.add(robots);

            Robot robot = new Robot(user ,table.getRoomId() ,table.getTableId());
            robot.sayHello();
            RobotDictionary.ME.createRobot(robot);
        }

        //人数不足以开局
        if (lists.isEmpty() || lists.size() < 2){
            return;
        }

        //全部依次坐下
        lists.forEach(e->{
            table.setChannelId(channelId);
            table.joinRoom(e);
            table.joinTable(e);
            table.sitDown(table.getNulSeatNum(), e.getPlayerId());

            //增加一个已坐下判断
            TableUserSet.OBJ.add(table.getRoomId()+table.getTableId() , e.getPlayerId(), PlayerStateEnum.sitdown.name());

            //修改玩家状态值
            e.setState(PlayerStateEnum.sitdown);

            //代入积分
            e.setTotalWinLoseScore(0);
            e.setPlayScoreStore(e.getUser().getMoney());
            e.setTotalTakeInScore(e.getUser().getMoney());

            if (!e.getUser().getChannel_id().equals(RoleType.ROBOT.getTypeName())) {
                PlayerService.getInstance().onPlayerLogin(e.getPlayerId());
            }

            //通知玩家坐下成功
            NoticeBroadcastMessages.playerSitDown(table, e.getPlayerId());
        });

        //游戏开局
        TableService.getInstance().playGame(table);
    }


    private void loadTableUserConfig() {

    }

}
