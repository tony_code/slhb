package com.slhb.game.room.network.protocol.acks;


import com.slhb.game.gate.network.protocol.Ack;

/**
 * @author
 * @since 2018/9/11 17:03
 */
public class JoloRoom_RoomListAck_40005 extends Ack {

    public JoloRoom_RoomListAck_40005(int functionId) {
        super(functionId);
    }
}
