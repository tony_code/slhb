package com.slhb.game.room.network.protocol.acks;

import com.slhb.game.gate.network.protocol.Ack;

/**
 * 申请入桌
 */
public class JoloRoom_ApplyJoinTableAck_40001 extends Ack {

    /**
     * @param messageLite
     */
    public JoloRoom_ApplyJoinTableAck_40001(int functionId) {
        super(functionId);
    }
}
