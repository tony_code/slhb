package com.slhb.game.room.service;

import JoloProtobuf.RoomSvr.JoloRoom;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.slhb.game.config.JsonConfig;
import com.slhb.game.dao.bean.KillPoolConfig;
import com.slhb.game.dao.bean.RoomConfigModel;
import com.slhb.game.service.RobotStrategyService;
import com.slhb.game.utils.NumUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 房间配置
 *
 * @author
 *
 * @since 2018/9/11 16:27
 *
 */
@Slf4j
public class RoomConfigService {

    //单例
    public static final RoomConfigService OBJ = new RoomConfigService();

    private List<RoomConfigModel> _configs = new ArrayList<>();

    private static final Gson gson = new GsonBuilder().serializeNulls().create();

    //全服人数
    private int allServerPlayers = 100;

    //当前服务器人数
    private int curServerPlayers = 10;

    public RoomConfigService() {
        //启动定时加载
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(
                ()->loadData(),
                1,
                5,
                TimeUnit.MINUTES);
    }

    /**
     * 初始化
     */
    public void init(){
        loadData();
    }

    /**
     * 加载数据
     */
    public void loadData(){
        log.info("载入房间配置到内存");
        List<RoomConfigModel> configs = JsonConfig.ME.getRoomConfigs();
        if (configs == null){
            log.error("无房间配置");
            return;
        }
        _configs = configs;
    }

    public List<JoloRoom.JoloRoom_RoomInfo> getRoomConfigs(String channel) {
        List<JoloRoom.JoloRoom_RoomInfo> _rooms = new ArrayList<>();
        _configs.forEach(e->{
            _rooms.add(JoloRoom.JoloRoom_RoomInfo.newBuilder()
                    .setRoomId(e.getRoomId())
                    .setRoomName("").setRoomDesc("")
                    .setMinJoin(e.getMinEnv())
                    .setAnte(e.getMinEnv())
                    //.addAllDoubleRoles(makeIntegerArray(e.getDoubleRoles()))
                    .setOnLiners(getOnliners(e))
                    .setServiceCharge(getServiceFee(channel,e.getRoomId()))
                    .build());
        });
        return _rooms;
    }

    public int getServiceFee(String channel, String roomId){
        KillPoolConfig pool = RobotStrategyService.OBJ.getPool(channel, roomId);
        if (pool != null){
            return pool.getServiceFee();
        }

        log.error("pool = null, channel={},room={}",channel,roomId);
        return 5;
    }

    public RoomConfigModel getRoomConfig(String roomId){
        Optional<RoomConfigModel> obj = _configs.stream().filter(e->e.getRoomId().equals(roomId)).findFirst();
        if (obj == null){
            log.error("不存在房间配置 roomId = {}", roomId);
            return null;
        }
        return obj.get();
    }

    public RoomConfigModel getRoomConfigByIdx(int idx){
        Optional<RoomConfigModel> obj = _configs.stream().filter(e->e.getId() == idx).findFirst();
        if (obj == null){
            return null;
        }
        return obj.get();
    }

    public List<RoomConfigModel> getConfigs(){
        return this._configs;
    }

    private long getOnliners(RoomConfigModel config){
        long realPlayer = this.allServerPlayers;
        log.debug("在线人数真实值={}",realPlayer);
        try {
            //计算规则
            String onlineRole = config.getOnlinerRoles();

            List<String> roles = makeArray(onlineRole);
            //规则参数异常
            if (roles == null || roles.size() != 5){
                return realPlayer;
            }

            log.debug("在线人数规则onlineRole={}, roles={}", onlineRole, roles.toString());
            int x = Integer.valueOf(roles.get(0));
            float y = Float.valueOf(roles.get(1));
            int A = Integer.valueOf(roles.get(2));
            int B = Integer.valueOf(roles.get(3));
            int C = Integer.valueOf(roles.get(4));

            //房间占比
            double roompre = 0;
            double A1 = getRandomDouble(A * 0.8, A * 1.2);
            double B1 = getRandomDouble(B * 0.8, B * 1.2);
            double C1 = getRandomDouble(C * 0.8, C * 1.2);
            if (config.getId() == 2){
                roompre = A1;
            }else if (config.getId() == 3){
                roompre = B1;
            }else if (config.getId() == 4){
                roompre = C1;
            }else {
                roompre = (100 - A1 - B1 - C1) >=0 ? (100 - A1 - B1 - C1) : 0;
            }

            double playersTmp = (x + realPlayer * y) * roompre * 0.01d;
            log.info("在线人数 x={},realplayer={},y={}, roompre={},playersTmp={}",x,realPlayer,y,roompre,playersTmp);

            long players = Long.parseLong(NumUtils.double2String(Math.ceil(playersTmp)));

            return players;
        }catch (Exception e){
            log.error("转换异常",e);
        }

        return realPlayer;
    }

    private double getRandomDouble(double minNum, double maxNum){
        double boundedDouble = minNum + new Random(System.currentTimeMillis()+System.nanoTime()).nextDouble() * (maxNum - minNum);
        return boundedDouble;
    }

    private List<String> makeArray(String str){
        List<String> list = new ArrayList<>();
        try {
            if (StringUtils.isEmpty(str)){
                return list;
            }
            list = gson.fromJson(str , new TypeToken<List<String>>(){}.getType());
            return list;
        }catch (Exception e){
            log.error("转换规则字符串失败string str = {}, exception = {}", str, e.getMessage());
            return list;
        }
    }

    private List<Integer> makeIntegerArray(String str){
        List<Integer> list = new ArrayList<>();
        try {
            if (StringUtils.isEmpty(str)){
                return list;
            }
            list = gson.fromJson(str , new TypeToken<List<Integer>>(){}.getType());
            return list;
        }catch (Exception e){
            log.error("转换规则字符串失败 int str = {}, exception = {}", str, e.getMessage());
            return list;
        }
    }

    public void setPlayers(int allServerPlayers, int curServerPlayers){
        if (allServerPlayers > 0){
            this.allServerPlayers = allServerPlayers;
        }

        if (curServerPlayers > 0){
            this.curServerPlayers = curServerPlayers;
        }
    }

}

