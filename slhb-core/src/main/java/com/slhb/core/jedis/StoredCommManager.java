package com.slhb.core.jedis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.BinaryJedis;
import redis.clients.jedis.Jedis;
import java.util.HashSet;
import java.util.Set;

import static com.slhb.core.jedis.BinaryJedis.BinaryJedisGeneral.OK;

/**
 * StoredObj 对象的管理器，负责创建或获取StoredObj 的Ref 存储key 采用表名-id 存储value 采用json 格式
 */
public class StoredCommManager extends StoredObjCodec {
    private static final Logger log = LoggerFactory.getLogger(StoredCommManager.class);

    public static double incrByFloat(String key, double incrment){
        BinaryJedis redis = JedisFactory.getJedisCommon();
        try {
            redis.select(JedisCommonConfig.DB_ID);
            return redis.incrByFloat(encode(key), incrment);//对存储在指定key的数值执行原子的加1操作。
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return 0;
        } finally {
            if (redis != null) {
                redis.close();
            }
        }
    }

    public static String get(String key){
        BinaryJedis redis = JedisFactory.getJedisCommon();
        try {
            redis.select(JedisCommonConfig.DB_ID);
            byte[] d = redis.get(encode(key));
            return toStr(d);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        } finally {
            if (redis != null) {
                redis.close();
            }
        }
    }

    /**
     * 保存单条String
     *
     * @param key   保存key
     * @param value 保存的String对象
     */
    public static boolean set(String key, String value) {
        BinaryJedis redis = JedisFactory.getJedisCommon();
        try {
            long st = System.currentTimeMillis();
            redis.select(JedisCommonConfig.DB_ID);
            String back = redis.set(encode(key), encode(value));
            if (System.currentTimeMillis() - st > 1000) {
                log.warn("redis set too long,key = " + key);
            }
            if (back.equals(OK)) {
                return true;
            }
            return false;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (redis != null) {
                redis.close();
            }
        }
        return false;
    }

    public static Set<String> keys( String key) {
        BinaryJedis redis = JedisFactory.getJedisCommon();
        Set<String> result = new HashSet<String>();
        try {
            redis.select(JedisCommonConfig.DB_ID);
            Set<byte[]> tmp = redis.keys(encode(key));
            for (byte[] bs : tmp) {
                result.add(toStr(bs));
            }
            if (result.size() == 0) return null;
            return result;
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        } finally {
            redis.close();
        }
    }


    public static boolean exists(String key) {
        BinaryJedis redis = JedisFactory.getJedisCommon();
        try {
            redis.select(JedisCommonConfig.DB_ID);
            return redis.exists(encode(key));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        } finally {
            if (redis != null) {
                redis.close();
            }
        }
    }

    public static boolean del(String key) {
        BinaryJedis redis = JedisFactory.getJedisCommon();
        try {
            redis.select(JedisCommonConfig.DB_ID);
            long result =  redis.del(encode(key));
            if (result == 1){
                return true;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        } finally {
            if (redis != null) {
                redis.close();
            }
        }
        return false;
    }


    /**
     * 加锁
     * @param key
     * @param expireTime
     * @return
     */
    public static boolean acquireLock(String key, int expireTime){
        Jedis redis = JedisFactory.getJedisCommon();
        try {
            redis.select(JedisCommonConfig.DB_ID);
            String param = null;
            String result = redis.set(key, param == null ? String.valueOf(System.currentTimeMillis()) : param, "NX", "EX", expireTime);

            if ("OK".equalsIgnoreCase(result)) {
                return true;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        } finally {
            if (redis != null) {
                redis.close();
            }
        }
        return false;
    }
}
