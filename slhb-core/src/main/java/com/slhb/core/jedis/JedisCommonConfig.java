package com.slhb.core.jedis;

import com.slhb.core.configuration.Property;
import lombok.ToString;

/**
 * This class holds all configuration of database
 * 
 * @author xujian
 */
@ToString
public class JedisCommonConfig {

	/**
	 * Default database url.
	 */
	@Property(key = "redis.common.ip", defaultValue = "127.0.0.1")
	public static String		REDIS_IP;

	/**
	 * Default database user
	 */
	@Property(key = "redis.common.port", defaultValue = "6379")
	public static int		REDIS_PORT;

	/**
	 * Minimum amount of connections that are always active
	 */
	@Property(key = "redis.common.max_total", defaultValue = "10")
	public static int			REDIS_MAX_TOTAL;

	/**
	 * Maximum amount of connections that are allowed to use
	 */
	@Property(key = "redis.common.max_idle", defaultValue = "5")
	public static int			REDIS_MAX_IDLE;

	@Property(key = "redis.common.password", defaultValue = "undefined")
	public static String		REDIS_PASSWORD;

	@Property(key = "redis.common.db_id", defaultValue = "1")
	public static int		DB_ID;

	@Property(key = "redis.common.open_sentinel", defaultValue = "false")
	public static boolean REDIS_OPEN_SENTINEL;

	@Property(key = "redis.common.master_name", defaultValue = "my_master")
	public static String	REDIS_MASTER_NAME;

	@Property(key = "redis.common.sentinel_address", defaultValue = "127.0.0.1:26379")
	public static String REDIS_SENTINEL_ADDRESS;

}
